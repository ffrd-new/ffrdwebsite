<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    /*Navbar*/
    'home'=> 'الصفحة الرئيسية',
    'news'=> 'الأخبار',
    'events'=> 'الفعاليات',
    'gallery'=> 'الوسائط ',
    'media_office'=> 'المكتب الاعلامي',
    'contact'=> 'اتصل بنا ',
    // 'publications'=> 'المنشورات ',
    'publications' => 'إصداراتنا',
    'about' => 'من نحن',
    'about_media' => 'عن المكتب الإعلامي',
    'about_vision' => 'الرؤية',
    'about_message' => 'الرسالة',
    'about_team' => 'مكتبة الديوان الأميري',
    'services' => 'الخدمات',
    'services_permits' => 'طلب تصريح تصوير داخل الإمارة ',
    'services_training' => 'التدريب',
    'services_request' => 'طلب صور أو مواد فيلمية',
    'services_library' => 'مكتبة الديوان الأميري',
    'gallery_pictures' => 'الصور',
    'gallery_videos' => 'الفيديوهات',

    // Footer
    'important_links' => 'الروابط المهمة',
    'contact_us' => 'اتصل بنا',
    'privacy_policy' => 'سياسة الأمن والخصوصية',
    'disclaimer' => 'تنصل',
    'sitemap' => 'خريطة الموقع',
    'terms-conditions' => 'الشروط والأحكام',

    // Home Page
    'latest_video' => 'الفيديوهات',
    'latest_photos' => 'الصور ',
    'latest_tweets' => 'اخر التغريدات',
    'news_highlights' => 'أبرز الأخبار',
    // 'view_more_video' => 'عرض المزيد من الفيديو',
    'view_more_video' => 'عرض المزيد من الفيديوهات',
    'view_more_photos' => 'عرض المزيد من الصور',
    'media_office'=> 'المكتب الإعلامي',
    'about_us'=> 'من نحن',
    'our_services'=> 'الخدمات',
    // 'our_publications'=> 'الإصدارات ',
    'our_publications' => 'إصداراتنا',
    'view_more'=> 'عرض المزيد',
    'circulars'=> 'التعاميم',
    'vision'=> 'رؤية',
    'director_message'=> 'رسالة مدير',
    'mission_permission'=> 'إذن المهمة',
    'working_&_training'=> 'عامل  تدريب',
    'request_for_photo_or_video'=> 'طلب للصور أو الفيديو',
    'fujairah_events' => 'فعاليات الفجيرة',
    'event_calender' => 'فعاليات التقويم',

    'home_media' => 'عن المكتب الإعلامي ',
    'home_vision' => 'الرؤية',
    'home_message' => 'الرسالة',
    // 'home_team' => 'فريق الإعلام والاتصال الحكومي',
    'home_team' => 'مكتبة الديوان الأميري',
    'events_list' => 'قائمة الأحداث',
    // Events
    'upcoming_events'=> 'الأحداث القادمة',

    // News
    'news-title' => 'أخبار المكتب الإعلامي',
    'highlight_news' => 'أحدث الأخبار',

    // Gallery
    'image_gallery' => 'معرض الصور',
    'video_gallery' => 'معرض الفيديوهات',

    // Contact
    'contact_message' => 'كما يمكنكم التواصل معنا من خلال صفحات المكتب الخاصة على قنوات التواصل الاجتماعي ',
    'location' => 'الديوان الأميري، المبنى المركزي، الطابق الأول',
    'working_hours' => 'من الأحد إلى الخميس  8:00 صباحاً - 2:30 مساءً ',

    'search' => 'إظهار نتائج',
    'read_more' => 'قراءة المزيد',
    'name' => 'الاسم',
    'mobile' => 'الهاتف المتحرك',
    'email' => 'البريد الإلكتروني',
    'message' => 'رسالة',
    'submit' => 'إرسال',


    'about_us' => 'معلومات عنا',
    'fujairah_events' => 'فعاليات الفجيرة',
];
