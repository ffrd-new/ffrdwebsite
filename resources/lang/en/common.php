
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    

    /*Navbar*/
    'home'=> 'Home',
    'news'=> 'News',
    'events'=> 'Events',
    'gallery'=> 'Gallery',
    'media_office'=> 'Media Office',
    'contact'=> 'Call Us',
    'publications'=> 'Publications',
    'about' => 'About Us',
    'about_media' => 'About Media Office',
    'about_vision' => 'Vision',
    'about_message' => 'Message',
    'about_team' => 'Media Team & Government Communication',
    'services' => 'Services',
    'services_permits' => 'Permission',
    'services_training' => 'Training',
    'services_request' => 'Request for Photo or Video',
    'services_library' => 'Library',
    'gallery_pictures' => 'Pictures',
    'gallery_videos' => 'Videos',

    // Footer
    'important_links' => 'Important Link',
    'contact_us' => 'Call Us',
    'privacy_policy' => 'Privacy Policy',
    'disclaimer' => 'Disclaimer',
    'sitemap' => 'Sitemap',
    'terms-conditions' => 'Terms & Conditions',

    // Home Page
    'latest_video' => 'Latest Video',
    'latest_photos' => 'Latest Photos',
    'latest_tweets' => 'Latest Tweets',
    'news_highlights' => 'News Highlights',
    'view_more_video' => 'View More Video',
    'view_more_photos' => 'View More Photos',
    'media_office'=> 'Media Office',
    'about_us'=> 'About Us',
    'our_services'=> 'Our Services',
    'our_publications'=> 'Our Publications',
    'view_more'=> 'View More',
    'circulars'=> 'Circulars',
    'vision'=> 'vision',
    'director_message'=> 'Director Message',
    'mission_permission'=> 'Mission Permission',
    'working_&_training'=> 'Working & Training',
    'request_for_photo_or_video'=> 'Request For photo or video',
    'fujairah_events' => 'Fujairah Events',
    'event_calender' => 'Event Calendar',

    'home_media' => 'About Media Office',
    'home_vision' => 'Vision',
    'home_message' => 'Message',
    'home_team' => 'Gov Communication Team',
    'events_list' => 'Events List',
     // Events
    'upcoming_events'=> 'Upcoming Events',

    // News
    'highlight_news' => 'Highlight News',
    'latest_news' => 'Latest News',

    // Gallery
    'image_gallery' => 'Image Gallery',
    'video_gallery' => 'Video Gallery',

    // Contact
    'contact_message' => 'Get connected with us through social media',
    'location' => 'Emiri Court, Al Ittihad Road - Fujairah',
    'working_hours' => 'Sunday to Thursday 8:00 am - 2:30 pm',

    'search' => 'Showing results of ',
    'read_more' => 'Read More',
    'name' => 'Name',
    'mobile' => 'Mobile',
    'email' => 'Email',
    'message' => 'Message',
    'submit' => 'Submit',

    'about_us' => 'About Us',
    'fujairah_events' => 'Fujairah Events',
];
