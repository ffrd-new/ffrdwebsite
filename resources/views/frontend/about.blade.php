@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','About Us | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','من نحن | المكتب الإعلامي لحكومة الفجيرة ')
@endif

@section('content')
<div class="about-page">
	<div class="container-fluid">
		<div class="row topcategory">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@if ( Config::get('app.locale') == 'en') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
					<li><a href="{{Helper::BaseUrl('/about-us#about-media-office')}}" class="active">About Us</a></li>	
				</ul>
				@elseif ( Config::get('app.locale') == 'ar') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
					<li><a href="{{Helper::BaseUrl('/about-us#about-media-office')}}" class="active">معلومات عنا </a></li>	
				</ul>
				@endif
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
				<div class="heading-section">
					@if ( Config::get('app.locale') == 'en') 
					<h3 class="textcapital">About Us</h3>
					@elseif ( Config::get('app.locale') == 'ar')
					<h3 class="textcapital">معلومات عنا</h3>
					@endif
				</div>

				<div class="about-sectiontab">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#about-media-office" data-tab="#about-media-officesection">
							{{trans('common.home_media')}}
						</a></li>
						<li><a href="#vision" data-tab="#visionsection">{{trans('common.home_vision')}}</a></li>

						<li><a href="#message" data-tab="#messagesection">{{trans('common.home_message')}}</a></li>

						<li class="sample"><a href="#media-team-government-communication" data-tab="#media-team-government-communicationsection" class="target-tab-link">{{trans('common.home_team')}}</a></li>

					</ul>

					<div class="tab-content">
						<div id="about-media-officesection" class="tab-pane fade in active">
							@if ( Config::get('app.locale') == 'en') 
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							@elseif ( Config::get('app.locale') == 'ar')
							<p>تأسس المكتب الإعلامي لحكومة الفجيرة بموجب المرسوم الأميري رقم 10 لسنة 2014 الصادر عن صاحب لسمو الشيخ حمد بن محمد الشرقي عضو المجلس الأعلى حاكم الفجيرة، بتاريخ ٢١ يوليو ٢٠١٤ </p>

							<p>ويهدف المكتب إلى تزويد وسائل الإعلام المختلفة بالأخبار المتعلقة بالحاكم وحكومة الفجيرة والتغطية الإعلامية لأنشطتهما المختلفة، بالإضافة إلى الإعداد والتنظيم للمؤتمرات الصحفية المتعلقة بالحاكم والحكومة والتنسيق مع مندوبي وسائل الإعلام المختلفة لحضور تلك المؤتمرات، وترتيب الحوارات واللقاءات الإعلامية التي يجريها الحاكم وولي العهد ومسؤولي الحكومة مع مختلف وسائل الإعلام ومتابعة إذاعتها ونشرها ورصد تأثيراتها وصداها لدى الرأي العام. </p>

							<p>ويعد المكتب الإعلامي لحكومة الفجيرة المظلة التي تجمع المكاتب والمراكز الإعلامية لدوائر الحكومة، لتضمن أعلى مستويات التنسيق البيني وتدقيق المعلومات وتوحيد رسائلها الإعلامية. </p>

							<p>ويقوم المكتب برصد وتحليل ما تنشره وسائل الإعلام واتجاهات الرأي العام فيما يتعلق بآخر المستجدات العالمية والعربية والوطنية وأخبار إمارة الفجيرة وكل ما يجري فيها، إضافة إلى رصد الأخبار والتحليلات المتعلقة بأنشطة الحكومة ومسؤوليها، وإعداد التقارير التي تلخّص مجمل أعمال الرصد والتحليل بأسلوب علمي ومنهجي دقيق. </p>

							<p>ويتولى المكتب الإعلامي لحكومة الفجيرة حفظ وتوثيق وأرشفة الحوارات والمقابلات الصحفية والتلفزيونية والإذاعية للحاكم ولولي العهد ومسؤولي الحكومة، بالإضافة إلى إصدار المطبوعات الدورية والمناسباتية.  </p>

							<p>وينظم المكتب الإعلامي ورشات العمل والإعداد والتدريب الإعلامي ذات الاختصاص ويقوم بإدارة المشاريع والفعاليات الإعلامية في المناسبات المختلفة، ويتبع له (فريق الإعلام والاتصال الحكومي)، وهو الأول من نوعه في الدولة، كجسم إعلامي حكومي، يتألف من منسقي الإعلام في الدوائر المؤسسات والهيئات الحكومية. </p>
							@endif
						</div>
						<div id="visionsection" class="tab-pane fade">
							@if ( Config::get('app.locale') == 'en') 
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							@elseif ( Config::get('app.locale') == 'ar')
							<p>نعتمد على الابتكار والإبداع والتميز، للارتقاء بدور بالعمل الإعلامي بما يدعم كافة الجهود الحكومية في مختلف القطاعات في إمارة الفجيرة، وعكس الدور الرائد للدولة على كافة المستويات ورسالتها الإنسانية الحضارية، وبناء الصورة الذهنية المتفردة له، التي تؤكد جدارتها بالرقم  واحد عالمياً، من خلال ممارسة يشهد لها الجميع بالجودة والريادة في كافة المجالات على صعيد المنطقة والعالم. </p>
							@endif
						</div>
						<div id="messagesection" class="tab-pane fade">
							@if ( Config::get('app.locale') == 'en') 
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							@elseif ( Config::get('app.locale') == 'ar')
							<p>يسعى المكتب الإعلامي لحكومة الفجيرة إلى إيجاد رافد رسمي فعال لتزويد وسائل الإعلام بالأخبار والمعلومات الدقيقة حول الحاكم والحكومة وكافة دوائرها وهيئاتها ومؤسساتها بما يضمن توصيل الرسالة الإعلامية للحكومة بأسلوب فعال يمتاز بالوضوح والموضوعية والشفافية ووفقاً لأرقى المعايير والممارسات. </p>
							@endif
						</div>
						<div id="media-team-government-communicationsection" class="tab-pane fade">
							@if ( Config::get('app.locale') == 'en') 
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<ul>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </li>
							</ul>
							@elseif ( Config::get('app.locale') == 'ar')
							<p>تطبيقاً لما تتضمنه المرسوم الأميري رقم 10 لعام 2014 الصادر عن صاحب السمو الشيخ حمد بن محمد الشرقي عضو المجلس الأعلى- حاكم الفجيرة، الخاص بإنشاء المكتب الإعلامي لحكومة الفجيرة، تم تشكيل (فريق الإعلام والاتصال الحكومي)، وهو الأول من نوعه في الدولة، كجسم إعلامي يتبع للمكتب الإعلامي ويتألف من منسقي الإعلام في الدوائر المؤسسات والهيئات الحكومية، ويهدف إلى: </p>
							<ul>
								<li>الارتقاء بأداء المكاتب الإعلامية في الدوائر المؤسسات والهيئات الحكومية </li>
								<li>توحيد الجهود الإعلامية وإجراء تشبيك بين المؤسسات الحكومية، وتبادل الخبرات والإمكانيات والدعم المتبادل</li>
								<li>توحيد الرسائل الإعلامية وتوجيهها بما يخدم إستراتيجية العمل الإعلامي العام للحكومة. </li>
							</ul>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
				<div class="about_image">
					<img src="{{url('/images/about.jpg')}}" alt="Training">
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>

</script>
<script type="text/javascript">

	$(".nav-tabs a").click(function(){
		// $(this).tab('show');
		$tabid = $(this).attr("href")+'section';
		$('.nav-tabs li').removeClass('active');
		$(this).parent().addClass('active');
		$('.tab-content .tab-pane').removeClass('active in');
		$($tabid).addClass('active in');

		// $('a[href="#passive_order_categories"]').tab('show');
	});
	// $('.nav-tabs a').on('shown.bs.tab', function(event){
	// 	var x = $(event.target).text();         
	// 	var y = $(event.relatedTarget).text();  
	// 	$(".act span").text(x);
	// 	$(".prev span").text(y);
	// });
</script>
@endsection
