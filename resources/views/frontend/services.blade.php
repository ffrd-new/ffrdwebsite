@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','Services | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title',' الخدمات | المكتب الإعلامي لحكومة الفجيرة ')
@endif

@section('content')
<div class="services-page">
	<div class="services-section1" id="permits">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if ( Config::get('app.locale') == 'en') 
					<h3>Permits</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					@elseif ( Config::get('app.locale') == 'ar')
					<h3>طلب تصريح تصوير داخل الإمارة </h3>
					<p>يمكنكم الحصول على تصاريح مؤقته للتصوير الجوي والأرضي داخل إمارة الفجيرة، وذلك وفق الشروط والأحكام التالية: </p>
					<ul>
						<li>يجب على طالب التصريح، التقدم بطلبه، شخصياً أو عبر الايميل، قبل 5 أيام عمل من تاريخ بدء التصوير. </li>
						<li>توجيه رسالة رسمية (من جهة التصوير) تتضمن تفاصيل عن خطة التصوير وغاياته ومدته، وأسماء العاملين فيه.</li>
						<li>ارفاق الرسالة بالتراخيص التجارية والقانونية للجهة التي تطلب التصوير </li>
						<li>تعبئة استمارة معلومات (يمكن الحصول عليها من المكتب الإعلامي).. حول جميع أفراد طاقم العمل، مرفقة بكافة الوثائق الثبوتية الشخصية والمهنية وتأشيرات الزيارة والإقامة. </li>
						<li>تعبئة تعهد بالالتزام بأخلاقيات العمل الإعلامي وعدم التصوير بما يخالف القوانين والأنظمة المتبعة في الدولة أو يسيء لها. </li>
						<li>تعبئة تعهد بالالتزام بعدم استخدام المادة الفيلمية والصور لغايات غير تلك المذكورة في رسالة طلب التصوير الرسمية. </li>

					</ul>
					@endif
				</div>	
			</div>
		</div>
	</div>
	<div class="services-section2" id="training">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if ( Config::get('app.locale') == 'en') 
					<h3>Training</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					@elseif ( Config::get('app.locale') == 'ar')	
					<h3>التدريب</h3>
					<p>يقدم قسم التطوير والتدريب في المكتب الإعلامي لحكومة الفجيرة العديد من البرامج التدريبية الاحترافية، تستهدف تأهيل مسؤولي المكاتب الإعلامية في المؤسسات والدوائر التابعة لحكومة الفجيرة في مجالات الإعلام كافة، وزيادة القدرات العلمية والعملية استنادا، لما يتطلبه واقع عملتهم وطبيعته...وذلك بإدارة نخبة من أفضل المدربين وكبار الممارسين والخبراء في العديد من التخصصات والمجالات الإعلامية المختلفة. </p>
					<p>ويعتمد المكتب الإعلامي لحكومة الفجيرة برنامجاً سنوياً للتدريب، يتم الإعلان عنه عبر موقعه الالكتروني ومنصاته على صفحات التواصل الاجتماعي، و يستهدف تطوير قدرات موظفي الدوائر الحكومية في مجال الإعلام والعلاقات العام، وإكساب خبرات جديدة تدعم قدراتهم على إنجاز مهامهم وفق أفضل المعايير. </p>
					@endif
				</div>		
			</div>
		</div>
	</div>
	<div class="services-section3" id="request-photovideo">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if ( Config::get('app.locale') == 'en') 
					<h3>Request for Photo or Video</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					@elseif ( Config::get('app.locale') == 'ar')	
					<h3>طلب صور أو مواد فيلمية</h3>
					<p>يمكنكم الحصول على صور ومواد فيلمية للأحداث والفعاليات التي تجري في إمارة الفجيرة ويحضرها صاحب السمو حاكم الفجيرة والشيوخ، وذلك وفق الشروط والأحكام التالية: </p>
					<ul>
						<li>توجيه رسالة رسمية تتضمن معلومات عن الجهة التي تطلب المواد الفيلمية والصور، والمكان والهدف استخدامها، أو تعبئة استمارة معلومات للأفراد (يمكن الحصول عليها من المكتب الإعلامي). </li>
						<li>التعهد بعدم القيام بأي تغيير في الصور أو المادة الفيلمية أو التصرف بها لغير الأهداف المصرح بها في الطلب الرسمي</li>
						<li>التعهد بعدم استخدام الصور او المادة الفيلمية في أي مادة إعلامية تسيء لدولة الإمارات العربية المتحدة بشكل عام وللفجيرة بشكل خاص. </li>
					</ul>
					@endif	
				</div>
			</div>
		</div>
	</div>

	<div class="services-section4" id="library">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if ( Config::get('app.locale') == 'en') 
					<h3>Library</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					@elseif ( Config::get('app.locale') == 'ar')	
					<h3>مكتبة الديوان الأميري  </h3>
					<p>تشتمل مكتبة الديوان الأميري على 10 آلاف عنوان تقريباً تضم كتباً ومجلدات ومراجع ومعاجم ذات موضوعات مختلفة في شتى نواحي المعرفة، مثل العلوم الاجتماعية والفلسفة والديانات والمعارف العامة والتاريخ والجغرافية والعلوم السياسية والقانون والفنون الجميلة واللغة والادب والعلوم والزراعة والتكنولوجيا، فضلاً عن مجموعة كبيرة من الكتب الخاصة بدولة الامارات العربية المتحدة.</p>
					<p>تقوم المكتبة بتقديم خدمة الاعارة فقط لموظفين الديوان الاميري وباقي الدوائر والأقسام التابعة له، أما الاستعارة الخارجية فتكون بناء على موافقة مسبقة من المكتب الإعلامي، وتقوم المكتبة باستقبال رواد وزوار من مختلف الاعمار بغرض الدراسة والمطالعة والبحث موفرة أركان خاصة لهذا الغرض مزودة بكومبيوترات وانترنت وذلك يوميا من الساعة التاسعة صباحاً و حتى الثانية ظهراً </p>
					<p>وتتبع المكتبة نظام مكتبة الكونجرس الامريكي في تقسيم الموضوعات وتنظيمها وفهرستها الذي يعتمد في تنظيم وترتيب وتقسيم موضوعات المعرفة المختلفة وفق الاحرف الهجائية الإنجليزية،  </p>
					@endif	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>

</script>
<script type="text/javascript">

</script>
@endsection
