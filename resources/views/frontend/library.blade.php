@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','Library | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title',' مكتبة الديوان الأميري | المكتب الإعلامي لحكومة الفجيرة')
@endif

@section('content')
<div class="services-page">
	<div class="services-section1" id="permits">
		<div class="container-fluid">
			<div class="row topcategory">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if ( Config::get('app.locale') == 'en') 
					<ul class="breadcrumb">
						<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
						<li><a href="{{Helper::BaseUrl('/library')}}" class="active">Library</a></li>	
					</ul>
					@elseif ( Config::get('app.locale') == 'ar') 
					<ul class="breadcrumb">
						<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
						<li><a href="{{Helper::BaseUrl('/library')}}" class="active">مكتبة الديوان الأميري</a></li>	
					</ul>
					@endif
				</div>
			</div>
			<div class="row flip-section">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 service_content">
					@if ( Config::get('app.locale') == 'en') 
					<h3>Library</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<a href="mailto:info@media.fujairah.ae" class="requestmail-btn"> Add email for requesting permission</a>
					
					@elseif ( Config::get('app.locale') == 'ar')
					<h3>مكتبة الديوان الأميري </h3>
					<p>تشتمل مكتبة الديوان الأميري على 10 آلاف عنوان تقريباً تضم كتباً ومجلدات ومراجع ومعاجم ذات موضوعات مختلفة في شتى نواحي المعرفة، مثل العلوم الاجتماعية والفلسفة والديانات والمعارف العامة والتاريخ والجغرافية والعلوم السياسية والقانون والفنون الجميلة واللغة والادب والعلوم والزراعة والتكنولوجيا، فضلاً عن مجموعة كبيرة من الكتب الخاصة بدولة الامارات العربية المتحدة.</p>
					<p>تقوم المكتبة بتقديم خدمة الاعارة فقط لموظفين الديوان الاميري وباقي الدوائر والأقسام التابعة له، أما الاستعارة الخارجية فتكون بناء على موافقة مسبقة من المكتب الإعلامي، وتقوم المكتبة باستقبال رواد وزوار من مختلف الاعمار بغرض الدراسة والمطالعة والبحث موفرة أركان خاصة لهذا الغرض مزودة بكومبيوترات وانترنت وذلك يوميا من الساعة التاسعة صباحاً و حتى الثانية ظهراً.</p>
					<p class="requestmail-btn">وتتبع المكتبة نظام مكتبة الكونجرس الامريكي في تقسيم الموضوعات وتنظيمها وفهرستها الذي يعتمد في تنظيم وترتيب وتقسيم موضوعات المعرفة المختلفة وفق الاحرف الهجائية الإنجليزية. <a href="mailto:info@media.fujairah.ae"> info@media.fujairah.ae </a></p>
					
	<!-- 				<p class="requestmail-btn">للحصول على أي خدمة من خدمات المكتب الإعلامي يرجى التواصل معنا عن طريق البريد الإلكتروني <a href="mailto:info@media.fujairah.ae"> info@media.fujairah.ae </a>
					</p> -->
					
					@endif
				</div>	
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					<div class="services_listsection">
						<h3>{{trans('common.services')}}</h3>
						<ul>
							<li><a href="{{Helper::BaseUrl('/permission')}}">{{trans('common.services_permits')}}</a></li>
							<li><a href="{{Helper::BaseUrl('/request-for-photo-or-video')}}">{{trans('common.services_request')}}</a></li>
							<li><a href="{{Helper::BaseUrl('/training')}}">{{trans('common.services_training')}}</a></li>
							<li class="active"><a href="{{Helper::BaseUrl('/library')}}">{{trans('common.services_library')}}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
@endsection
@section('js')
<script>

</script>
<script type="text/javascript">

</script>
@endsection
