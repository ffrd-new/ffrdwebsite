@extends('layouts.front')

@section('content')

<?php 
if ( Config::get('app.locale') == 'en')
	{ 
		$seo = Helper::seo($news_detail->en_title,str_limit($news_detail->en_meta_desc,'150'),$news_detail->image,$news_detail->en_meta_title,$news_detail->en_meta_keywords,$type);
	}
	elseif(Config::get('app.locale') == 'ar'){
		$seo = Helper::seo($news_detail->ar_title,str_limit($news_detail->ar_meta_desc,'150'),$news_detail->image,$news_detail->ar_meta_title,$news_detail->ar_meta_keywords,$type);
	}

	$meta = filter_var($seo['meta'], FILTER_SANITIZE_STRING);
	$meta_title = filter_var($seo['meta_title'], FILTER_SANITIZE_STRING);
	$meta_keyword = filter_var($seo['meta_keyword'], FILTER_SANITIZE_STRING);

	?>
	@section('title',$seo['meta_title'])
	@section('keywords',$seo['meta_keyword'])
	@section('meta-title',$seo['meta_title'])
	@section('description',$meta)
	@section('image',$seo['image'])
	<div class="detailPage">
		<div class="container-fluid">
			<div class="row topcategory">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<ul class="breadcrumb">
						@if ( Config::get('app.locale') == 'en')
						<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
						<li><a href="{{Helper::BaseUrl('/news')}}">News</a></li>
						<li><a href="#" class="active">{{str_limit($news_detail->en_title,30)}}</a></li>
						@elseif (Config::get('app.locale') == 'ar' )
						<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
						<li><a href="{{Helper::BaseUrl('/news')}}">الأخبار </a></li>
						<li><a href="#" class="active">{{str_limit($news_detail->ar_title,30)}}</a></li>
						@endif
					</ul>
				</div>
			</div>
			<div class="row">

				<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 news news-detailsection">
					@if ( Config::get('app.locale') == 'en')
					<div class="news-details">
						<h3 class="subheading">{{$news_detail->en_title}}</h3>
						<hr>
						<div class="shareicon"> 
							<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 news-nopadding">
								@if($news_detail->news_date)
								<p> {{date('M j, Y', strtotime($news_detail->news_date))}}</p>
								@else
								<p> {{date('M j, Y', strtotime($news_detail->publish_on))}}</p>
								@endif
							</div>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
								<p>
									<a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(url()->current())}}" target="_blank">
										<i class="mdi mdi-facebook-box mdi-24px"></i>
									</a>

									<a href="https://twitter.com/intent/tweet?url={{urlencode(url()->current())}}" target="_blank" data-lang="ar">
										<i class="mdi mdi-twitter-box mdi-24px"></i>
										
									</a>
								</p>
							</div>
						</div>
						<div class="news_imgsection">
							<img alt="{{$news_detail->en_title}}" src="{{asset('/')}}images/{{$type}}/{{Helper::imageCheck($news_detail->image)}}" class="mediaimg" style="height: auto;">
						</div>
						<div class="news-detail-content">
							<p>
								{!!$news_detail->en_description!!}
							</p>
						</div>
					</div>
					@elseif ( Config::get('app.locale') == 'ar')
					<div class="news-details">
						<h3 class="subheading">{{$news_detail->ar_title}}</h3>
						<hr>
						<div class="shareicon"> 
							<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 news-nopadding">
								@if($news_detail->news_date)
								<p> {{ Helper::ArabicDateMnth(date('M j, Y', strtotime($news_detail->news_date)))}}</p>
								@else
								<p>  {{ Helper::ArabicDateMnth(date('M j, Y', strtotime($news_detail->publish_on)))}}</p>
								@endif
							</div>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
								<p>
									<a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(url()->current())}}" target="_blank">
										<i class="mdi mdi-facebook-box mdi-24px"></i>
									</a>

<?php /*

$str = urlencode(url()->current());
$conv = iconv('utf-8', 'windows-1256', $str);
*/
$p =  Request::segment(2);
?>
									<a href="https://twitter.com/intent/tweet?text={{$news_detail->ar_title}}&url={{urlencode(url('/news/'.$p.'/news-fcmc'))}}" target="_blank" data-lang="ar">
										<i class="mdi mdi-twitter-box mdi-24px"></i>
										
									</a>
								
								</p>
							</div>
						</div>
						<div class="news_imgsection">
							<img alt="{{$news_detail->ar_title}}" src="{{asset('/')}}images/{{$type}}/{{Helper::imageCheck($news_detail->image)}}" class="mediaimg" style="height: auto;">
						</div>
						<div class="news-detail-content">
							<p>
								{!!$news_detail->ar_description!!}
							</p>
						</div>
					</div>
					@endif
				</div>
				<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 relatednews news-detail">
					@if($recentnews)
					<h3 class="subheading">{{trans('common.highlight_news')}}</h3>
					<hr>
					@foreach($recentnews as $recentzz)
					@if( Config::get('app.locale') == 'en')
					<div class="list">
						<a href="{{url('/')}}/{{$type}}/{{$recentzz->id}}/{{Helper::generateslug($recentzz->id)}}" class="taghighlight">
							<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"> -->
								<div class="content">	
									<img alt="{{$recentzz->en_title}}" src="{{asset('/')}}images/{{$type}}/{{Helper::imageCheck($recentzz->image)}}" class="mediaimg">
								</div>
								<div class="content">
									<p title="{{$recentzz->en_title}}">{{str_limit($recentzz->en_title,30)}}</p>
									@if($recentzz->news_date)
									<small> {{date('M j, Y', strtotime($recentzz->news_date))}}</small>
									@else
									<small> {{date('M j, Y', strtotime($recentzz->publish_on))}}</small>
									@endif
								</div>
								<!-- </div> -->

							</a>
						</div>
						@elseif( Config::get('app.locale') == 'ar')
						<div class="list">
							<a href="{{url('/')}}/{{$type}}/{{$recentzz->id}}/{{Helper::generateslug($recentzz->id)}}" class="taghighlight">
								<div class="content">	
									<img alt="{{$recentzz->ar_title}}" src="{{asset('/')}}images/{{$type}}/{{Helper::imageCheck($recentzz->image)}}" class="mediaimg">
								</div>
								<div class="content">
									<p title="{{$recentzz->ar_title}}">{{str_limit($recentzz->ar_title,30)}}</p>

									@if($recentzz->news_date)
									<small>  {{ Helper::ArabicDate(date('M j, Y', strtotime($recentzz->news_date)))}}</small>
									@else
									<small>   {{ Helper::ArabicDate(date('M j, Y', strtotime($recentzz->publish_on)))}}</small>
									@endif

								</div>
							</a>
						</div>
						@endif
						@endforeach
						@endif

					</div>

					<!-- Gallery news -->
					<div class="news-albumsection">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							@if(count($newsgal_data)>0)
							@if( Config::get('app.locale') == 'en')
							<h3 class="subheading">Pictures</h3>
							<hr>
							@else
							<h3 class="subheading">الصور</h3>
							<hr>
							@endif
							@foreach ($newsgal_data as $imgkey => $imgallist)
							@php
							$x = json_decode($imgallist->gallery);
							@endphp
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 homegallery-list">
								<a class="galimg" data-kkk="{{$imgkey}}">
									<img src="{{url('/')}}/images/gallery/{{$x[0]}}" class="img-responsive gallery-img zoomeffect">
								</a>
								<div class="tree hide">
									@foreach($x as $xy)
									<a class="{{$imgkey}}" rel="group" data-fancybox='gallery{{$imgkey}}' data-galery="{{$imgkey}}" href="{{url('/')}}/images/gallery/{{$xy}}">
										<img src="{{url('/')}}/images/gallery/{{$xy}}" class="img-responsive gallery-img">
									</a>
									@endforeach  
								</div>

								@if ( Config::get('app.locale') == 'en')
								<div class="details">
									<p title="{{$imgallist->en_title}}">
										{{str_limit($imgallist->en_title,35)}}
									</p>
									<p> 
										<!-- {{date('M j, Y', strtotime($imgallist->publish_on))}}  -->
										@if($imgallist->image_date)
										{{date('M j, Y', strtotime($imgallist->image_date))}}
										@else
										{{ date('M j, Y', strtotime($imgallist->publish_on))}}
										@endif
									</p>
								</div>
								@elseif ( Config::get('app.locale') == 'ar')
								<div class="details">
									<p title="{{$imgallist->ar_title}}">
										{{str_limit($imgallist->ar_title,35)}}
									</p>
									<p>
										<!-- {{ Helper::ArabicDate(date('M j, Y', strtotime($imgallist->publish_on)))}}  -->
										@if($imgallist->image_date)
										{{ Helper::ArabicDate(date('M j, Y', strtotime($imgallist->image_date)))}}
										@else
										{{ Helper::ArabicDate(date('M j, Y', strtotime($imgallist->publish_on)))}}
										@endif
									</p>
								</div>
								@endif

							</div>
							@endforeach
							@endif
						</div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 news-videosection">
						@if(count($newsvideo_data) > 0)
						@if( Config::get('app.locale') == 'en')
						<h3 class="subheading">Videos</h3>
						<hr>
						@else
						<h3 class="subheading">الفيديوهات</h3>
						<hr>
						@endif


						@foreach ($newsvideo_data as $videogallists)
						<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3" style="padding: 0">
							<a class="bla-1" href="{{$videogallists->video_url}}" 
								@if ( Config::get('app.locale') == 'en')
								title="{{$videogallists->en_title}}"
								@elseif ( Config::get('app.locale') == 'ar')
								title="{{$videogallists->ar_title}}"
								@endif
								>
								<div class="content">
									<img src="{{url('/')}}/images/videogallery/{{$videogallists->image}}" class="mediaimg zoomeffect">

								</div>
								@if ( Config::get('app.locale') == 'en')
								<p title="{{$videogallists->en_title}}">{{str_limit($videogallists->en_title,35)}}</p>
								<p> 
									@if($videogallists->video_date)
									{{date('M j, Y', strtotime($videogallists->video_date))}}
									@else
									{{ date('M j, Y', strtotime($videogallists->publish_on))}}
									@endif
								</p>
								@elseif ( Config::get('app.locale') == 'ar')
								<p title="{{$videogallists->ar_title}}">{{str_limit($videogallists->ar_title,35)}}</p>
								<p>@if($videogallists->video_date)
									{{ Helper::ArabicDate(date('M j, Y', strtotime($videogallists->video_date)))}}
									@else
									{{ Helper::ArabicDate(date('M j, Y', strtotime($videogallists->publish_on)))}}
									@endif
								</p>
								@endif
								<img src="{{url('/')}}/images/video-icon.png" class="playbtn">
							</a>
						</div>
						@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
		@endsection

		@section('js')
		<script>
			$(document).ready(function() {	
				$(".galimg").on("click", function(){
					var boolKey = $(this).data('kkk');
					$('a.'+boolKey)[0].click();
				});
			});
		</script>

		<script type="text/javascript">
			$(function(){
				$("a.bla-1").YouTubePopUp();
			$("a.bla-2").YouTubePopUp( { autoplay: 0 } ); // Disable autoplay
		});
	</script>
	@endsection