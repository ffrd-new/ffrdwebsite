@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','Search | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','بحث | المكتب الإعلامي لحكومة الفجيرة')
@endif

@section('content')
<div class="searchresults_section">
	<div class="container-fluid">
		<div class="results">
			<div class="row">
				<div class="col-md-12">
					<h3 class="textcapital search-title">{{trans('common.search')}} "{{$keyw}}"</h3>
					<hr>
				</div>
				@if(count($lists) >0)
				<div class="search_lists">
					@foreach($lists as $newg)
					@if(Config::get('app.locale') == 'en')

					@if($newg->name == 'event')
					<h2 style="display: none;">Events</h2>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 events padding-bottom2">
						<a href="{{url('/')}}/events/{{$newg->id}}/{{Helper::generateslug($newg->id)}}" class="taghighlight">
							<div class="boxed-layout zoomeffect">
								<img alt="{{$newg->en_title}}"src="{{asset('/')}}images/events/thumb/{{Helper::imageCheck($newg->image)}}" class="mediaimg">
								<div class="date">
									<h4>{{date('d', strtotime($newg->event_date))}}</h4>
									<p>{{date('M', strtotime($newg->event_date))}}</p>
								</div>
							</div>
							<div><p>{{str_limit($newg->en_title,25)}}</p></div>
						</a>
					</div>
					@endif
					@endif
					@endforeach	
				</div>
				<div class="search_lists">

					@foreach($lists as $ne)
					@if(Config::get('app.locale') == 'en')
					@if($ne->name == 'news')
					<h2 style="display: none;"><span >News</span></h2>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 newss padding-bottom">
						<a href="{{url('/')}}/{{$ne->name}}/{{$ne->id}}/{{Helper::generateslug($ne->id)}}" class="blist-img">
							<div class="zoomeffect">
								<img alt="{{$ne->en_title}}"src="{{asset('/')}}images/news/thumb/{{Helper::imageCheck($ne->image)}}" class="mediaimg">
							</div>
							<p>{{str_limit($ne->en_title,25)}}</p>
							@if($ne->news_date)
							<small>Posted on {{date('M j, Y', strtotime($ne->news_date))}}</small>
							@else
							<small>Posted on {{date('M j, Y', strtotime($ne->publish_on))}}</small>
							@endif
						</a>
					</div>
					@endif
					@endif
					@endforeach
				</div>
				<div class="search_lists">
					@foreach($lists as $newzzz)
					@if(Config::get('app.locale') == 'en')

					@if($newzzz->name == 'publications')
					<h2 style="display: none;"><span>Publications</span></h2>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-bottom publicat nopadding">
						
							<div class="col-lg-5">
								<a href="{{url('/')}}/publications/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" class="blist-img">
								<img alt="{{$newzzz->en_title}}"src="{{asset('/')}}images/publications/thumb/{{Helper::imageCheck($newzzz->image)}}">
							</a>
							</div>
							<div class="col-lg-7 nopadding">
								<a href="{{url('/')}}/publications/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" class="blist-img">
								<h3>{{str_limit(Helper::table_head_lower($newzzz->en_title),25)}}</h3>
								<p>{!!str_limit($newzzz->en_description,150)!!}</p>
								<small>Posted on {{date('M j, Y', strtotime($newzzz->publish_on))}}</small>
							</a>
								<div class="col-lg-12 nopadding publicadetail">
									<a class="btn btn-default more-infobtn" href="{{url('/')}}/publications/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" role="button">More Info</a>
									@php

									$x = json_decode($newzzz->pdf);
									@endphp

									@if($newzzz->pubcurl != null)
									<a class="btn btn-default downloadbtn" href="{{$newzzz->pubcurl}}" class="btn btn-info">
									Downloads</a>
									@else
									<a class="btn btn-default downloadbtn" href="{{url('/')}}/download/{{$newzzz->id}}" class="btn btn-info">
									Downloads</a>
									@endif

								</div>
							</div>
					
					</div>
					@endif
					@endif
					@endforeach
				</div>
				<div class="search_lists">
					@foreach($lists as $newzzz)
					@if ( Config::get('app.locale') == 'ar')
					@if($newzzz->name == 'event')
					<h2 style="display: none;">{{trans('common.events')}}</h2>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 events padding-bottom2">
						<a href="{{url('/')}}/events/{{$newzzz->id}}/{{Helper::generateslug($newzzz->id)}}" class="taghighlight">
							<div class="boxed-layout zoomeffect">
							<img alt="{{$newzzz->ar_title}}" src="{{asset('/')}}images/events/thumb/{{Helper::imageCheck($newzzz->image)}}" class="mediaimg">
						</div>
							<div><p>{{str_limit($newzzz->ar_title,25)}}</p></div>
							<div class="date">
								<h4>{{date('d', strtotime($newzzz->event_date))}}</h4>
								<p>{{ Helper::ArabicMonth(date('M', strtotime($newzzz->event_date)))}}</p>
							</div>
						</a>
					</div>
					@endif
					@endif
					@endforeach
				</div>
				<div class="search_lists">
					@foreach($lists as $newzzz)
					@if ( Config::get('app.locale') == 'ar')
					@if($newzzz->name == 'news')
					<h2 style="display: none;">{{trans('common.news-title')}}</h2>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 newss padding-bottom">
						<a href="{{url('/')}}/news/{{$newzzz->id}}/{{Helper::generateslug($newzzz->id)}}" class="blist-img">
							<div class="zoomeffect">
							<img alt="{{$newzzz->ar_title}}" src="{{asset('/')}}images/news/thumb/{{Helper::imageCheck($newzzz->image)}}" class="mediaimg">
						</div>
							<p>{{str_limit($newzzz->ar_title,25)}}</p>
							@if($newzzz->news_date)
							<small> شر على     {{ Helper::ArabicDate(date('M j, Y', strtotime($newzzz->news_date)))}}</small>
							@else
							<small> شر على    {{ Helper::ArabicDate(date('M j, Y', strtotime($newzzz->publish_on)))}}</small>
							@endif
						</a>
					</div>
					@endif
					@endif
					@endforeach
				</div>

				<div class="search_lists">
					@foreach($lists as $newzzz)
					@if ( Config::get('app.locale') == 'ar')
					@if($newzzz->name == 'publications')
					<h2 style="display: none;">{{trans('common.publications')}}</h2>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-bottom publicat nopadding">
						
							<div class="col-lg-5">
								<a href="{{url('/')}}/publications/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" class="blist-img">
								<img alt="{{$newzzz->ar_title}}"src="{{asset('/')}}images/publications/thumb/{{Helper::imageCheck($newzzz->image)}}">
							</a>
							</div>
							<div class="col-lg-7 nopadding">
								<a href="{{url('/')}}/publications/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" class="blist-img">
								<h3>{{str_limit( $newzzz->ar_title,25) }}</h3>
								<p>{!!str_limit($newzzz->ar_description,150)!!}</p>
								<small> شر على     {{ Helper::ArabicDate(date('M j, Y', strtotime($newzzz->publish_on)))}}</small>
							</a>
								<div class="col-lg-12 nopadding publicadetail">
									<a class="btn btn-default more-infobtn" href="{{url('/')}}/publications/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" role="button"> مزيد من المعلومات    </a>

									@if($newzzz->pubcurl != null)
									<a class="btn btn-default" href="{{$newzzz->pubcurl}}" class="btn btn-info downloadbtn"> التنزيلات
									</a>
									@else
									<a class="btn btn-default downloadbtn" href="{{url('/')}}/download/{{$newzzz->id}}" class="btn btn-info"> التنزيلات
									</a>
									@endif
								</div>
							</div>
						
					</div>
					@endif
					@endif
					@endforeach
				</div>

				@else
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="event-inner-content serachres">
						@if ( Config::get('app.locale') == 'en')
						<h4>No Results Found</h4>
						@elseif ( Config::get('app.locale') == 'ar')
						<h4> لا توجد سجلات
						</h4>
						@endif
					</div>
				</div>
				@endif	
			</div>
		</div>
	</div>
</div>
@endsection

