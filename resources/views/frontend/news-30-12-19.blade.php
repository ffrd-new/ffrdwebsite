@extends('layouts.front')

@if ( Config::get('app.locale') == 'en') 
@section('title','News | The Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','الأخبار  | مكتب الفجيرة الاعلامي')
@endif

@section('content')
<div class="newslist">
	
	<div class="container-fluid">
		<div class="row topcategory">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<!-- <div class="form-group">
					<form action="" method="GET">
						<div class="form-group">
							<select name="catlist" id="catlist" class="form-control category">
								@if ( Config::get('app.locale') == 'en')
								<option value=""> Select Category </option>
								@elseif ( Config::get('app.locale') == 'ar')
								<option value="">  الاقسام   </option>
								@endif
								@foreach ($listdrop as $list)
								@if ( Config::get('app.locale') == 'en')
								<option value="{{$list->id}}" @if($list->id == Request::segment(1) ) selected @endif >{{$list->en_name}} - ({{$list->count}})</option>
								@elseif ( Config::get('app.locale') == 'ar')
								<option value="{{$list->id}}" @if($list->id == Request::segment(1) ) selected @endif > {{$list->ar_name}}  -  ({{$list->count}})</option>
								@endif
								@endforeach 
							</select>
						</div>
					</form>
				</div> -->
			</div>
			<div class="hiden-xs hidden-sm col-md-4 col-lg-4"></div>
			<div class="col-xs-12 col-sm-8 col-md-4 col-lg-4">
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}" class="textcapital">{{trans('common.home')}}</a></li>
					<li><a href="{{Helper::BaseUrl('/news')}}" class="active textcapital">{{trans('common.news')}}</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid news">
		<div class="row">

			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
				<h3 class="textcapital">{{trans('common.news-title')}}</h3>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
				<div class="form-group searchfilter">
					@if( Config::get('app.locale') == 'en')
					<label>Search BY</label>
					@else
					<label>البحث عن الأخبار عن طريق تحديد التاريخ</label>
					@endif

					<input type="text" class="form-control" name="news_dates" id="news_dates">
				</div>
			</div>

			<div class="returnresult">        
				@if(count($news) >0)
				@foreach($news as $newzzz)
				@if ( Config::get('app.locale') == 'en')
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 padding-bottom news_list">
					<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::generateslug($newzzz->id)}}" class="blist-img">
						<img alt="{{$newzzz->en_title}}"src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($newzzz->image)}}" class="mediaimg">
						<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::generateslug($newzzz->id)}}"><p class="title">
							{{str_limit($newzzz->en_title,70)}}</p></a>
						@if($newzzz->news_date)
						<small> {{date('M j, Y', strtotime($newzzz->news_date))}}</small>
						@else
						<small> {{date('M j, Y', strtotime($newzzz->publish_on))}}</small>
						@endif
					</a>
				</div>

				@elseif ( Config::get('app.locale') == 'ar')
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 padding-bottom news_list">
					<div class="news_imagesection">
						<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::generateslug($newzzz->id)}}" class="blist-img">
							<img alt="{{$newzzz->ar_title}}" src="{{asset('/')}}images/{{$type}}/{{Helper::imageCheck($newzzz->image)}}" class="mediaimg zoomeffect">
						</a>
					</div>
					<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::generateslug($newzzz->id)}}">
						<!-- <p class="title">{{str_limit($newzzz->ar_title,60)}}</p> -->
						<p title="{{$newzzz->ar_title}}" class="title"> {{str_limit($newzzz->ar_title,95)}}</p>
					</a>
					@if($newzzz->news_date)
					<small>  {{ Helper::ArabicDate(date('M j, Y', strtotime($newzzz->news_date)))}}</small>
					@else
					<small>  {{ Helper::ArabicDate(date('M j, Y', strtotime($newzzz->publish_on)))}}</small>
					@endif
				</div>
				@endif
				@endforeach

				@else
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="event-inner-content">
						@if ( Config::get('app.locale') == 'en')
						<h4>No Records Found</h4>
						@elseif ( Config::get('app.locale') == 'ar')
						<h4> لا توجد سجلات
						</h4>
						@endif
					</div>
				</div>
				@endif   	
			</div> 	

			<div class="dateresponse">
			</div>

			<!-- <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 relatednews">
				<h3>{{trans('common.highlight_news')}}</h3>
				<hr>
				@if($recentevent)
				@foreach($recentevent as $res)

				@if ( Config::get('app.locale') == 'en')
				<div class="list">
					<a href="{{url('/')}}/{{$viceversa}}/{{$res->id}}/{{Helper::generateslug($res->id)}}" class="taghighlight">
						<div class="content">
							<i class="mdi mdi-newspaper md-24px"></i>
						</div>
						<div class="content">
							<p>{{str_limit($res->en_title,30)}}</p>
							@if($res->news_date)
							<small><i>Posted on {{date('M j, Y', strtotime($res->news_date))}}</i></small>
							@else
							<small><i>Posted on {{date('M j, Y', strtotime($res->publish_on))}}</i></small>
							@endif
						</div>
					</a>
				</div>
				@elseif ( Config::get('app.locale') == 'ar')
				<div class="list">
					<a href="{{url('/')}}/{{$viceversa}}/{{$res->id}}/{{Helper::generateslug($res->id)}}" class="taghighlight">
						<div class="content">
							<i class="mdi mdi-newspaper md-24px"></i>
						</div>
						<div class="content">
							<p>{{str_limit($res->ar_title,30)}}</p>
							@if($res->news_date)
							<small><i> نشر   {{date('M j, Y', strtotime($res->news_date))}}</i></small>
							@else
							<small><i> نشر {{date('M j, Y', strtotime($res->publish_on))}}</i></small>
							@endif
						</div>
					</a>
				</div>
				@endif
				@endforeach
				@endif	
			</div> -->
		</div>
		<div>
			{{ $news->links() }}
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$(document).on("change", '#catlist', function(event) { 

		var APP_URL = {!! json_encode(url('/')) !!}
		var categoryid=$(this).val();
		$.ajax({
			url: "{{url('/')}}/getcategorytitle/"+categoryid,
			dataType:'json',
			delay: 250,
			initSelection: true, 
			success:function(data){
				<?php if( Config::get('app.locale') == 'en' ) {  ?>
					window.location.href = APP_URL +'/'+data.id+'/news/'+convertToSlug(data.category_name); 
					<?php }elseif( Config::get('app.locale') == 'ar' ) { ?>
						window.location.href = APP_URL +'/'+data.id+'/news/'+data.category_name; 
						<?php } ?>
					}
				});
	});

	function convertToSlug(Text)
	{
		return Text
		.toLowerCase()
		.replace(/[^\w ]+/g,'')
		.replace(/ +/g,'-')
		;
	}

	$(document).ready(function(){
		$("#news_dates").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true
		})


		$('#news_dates').change(function(){
     //Change code!
     var newsDate = $(this).val();
     var APP_URL = {!! json_encode(url('/')) !!}
     $(".dateresponse").html('');
     $.ajax({
     	url: "{{url('/')}}/getNewsByDate/"+newsDate,
     	dataType:'json',
     	success:function(data){
     		if(data.length > 0){
     			$('.returnresult').hide();
     			$.each( data, function( key, value ) {
     				<?php if( Config::get('app.locale') == 'en' ) {  ?>

     					$(".dateresponse").append('<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 padding-bottom news_list"><div class="news_imagesection">'
     						+'<a href="'+APP_URL+'/news/'+value.id+'/'
     						+convertIDtoSlug(value.id)+'"' 
     						+' class="blist-img">'
     						+'<img alt="'+value.en_title+'" src="'+APP_URL+'/images/news/thumb/'+imageHelper(value.id)+'" class="mediaimg"></a></div>'
     						+'<a href="'+APP_URL+'/news/'+value.id+'/'
     						+convertIDtoSlug(value.id)+'">' 
     						+'<p class="title">'+value.en_title+'</p></a>'
     						+'<small>'+gatdate(value.news_date, value.publish_on)+'</small>'
     						+'</div>');

     					<?php }elseif( Config::get('app.locale') == 'ar' ) { ?>
     						$(".dateresponse").append('<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 padding-bottom news_list"><div class="news_imagesection">'
     							+'<a href="'+APP_URL+'/news/'+value.id+'/'
     							+convertIDtoSlug(value.id)+'"' 
     							+' class="blist-img">'
     							+'<img alt="'+value.ar_title+'" src="'+APP_URL+'/images/news/thumb/'+imageHelper(value.id)+'" class="mediaimg"></a></div>'
     							+'<a href="'+APP_URL+'/news/'+value.id+'/'
     							+convertIDtoSlug(value.id)+'">' 
     							+'<p class="title">'+value.ar_title+'</p></a>'
     							+'<small>'+gatdate(value.news_date,value.publish_on)+'</small>'
     							+'</div>');
     						<?php } ?>
     					});
     		}else
     		{
     			$('.returnresult').hide();
     			<?php if( Config::get('app.locale') == 'en' ) {  ?>
     				$(".dateresponse").append('<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">No records found</div>');	
     				<?php }elseif( Config::get('app.locale') == 'ar' ) { ?>
     					$(".dateresponse").append(' <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> لا توجد سجلات  </div> ');	
     					<?php } ?>	
     				}
     			}
     		});
 });
	});


	function convertIDtoSlug(id){
		var result1 = '';
		$.ajax({
			url: "{{url('/')}}/getslugname/"+id,
			dataType:'json',
			async: false,
			success:function(data){
				result1 = data;
			}
		});
		return result1;
	}

	function imageHelper(image){
		var result2 = '';
		$.ajax({
			url: "{{url('/')}}/getImageNames/"+image,
			dataType:'json',
			async: false,
			success:function(data){
				if(data.image != null){

					result2 = data.image;
				}else{
					result2 = 'noimage.jpg';
				}
			}
		});
		return result2;
	}

	function gatdate(date,pubdate){
		var result3 = '';
		console.log(date);
		// console.log(pubdate);
		if(date){

			$.ajax({
				url: "{{url('/')}}/getDate/"+date,
				dataType:'json',
				async: false,
				success:function(data){
					result3 = data;
				}

			});
			return result3;

		}else{
			$.ajax({
				url: "{{url('/')}}/getDate/"+pubdate,
				dataType:'json',
				async: false,
				success:function(data){
					result3 = data;
					console.log(result3);
				}
			});
			return result3;
		}

	}

</script>
@endsection