@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />

@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','Home')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title',' مكتب الفجيرة الاعلامي ')
@endif

@section('content')
<div class="body-inner home-page">
	<div class="scroll">
		<div class="row">
			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 padding0 left">
				<p>{{trans('common.news_highlights')}}</p>
			</div>
			<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 padding0 right">
				<!-- scrollamount="10" scrolldelay="85" -->
				<marquee behavior="scroll" loop="" @if ( Config::get('app.locale') == 'en') scrollamount="10" direction="left" @elseif ( Config::get('app.locale') == 'ar') direction="right" scrollamount="10" scrolldelay="85" @endif  onmouseover="this.stop();" onmouseout="this.start();">
					@php $typ = 'news'; @endphp
					@if(count($lnews)>0)
					<div class="marquee-content">
						@foreach ($lnews as $newslist)
						<a href="{{$newslist->news_url}}" target="_blank">
							@if ( Config::get('app.locale') == 'en')
							<span>
								{{str_limit($newslist->en_title,80)}}
							</span> 
							@elseif ( Config::get('app.locale') == 'ar')
							<span>
								{{str_limit($newslist->ar_title,80)}}
							</span> 
							@endif
						</a>
						@endforeach
					</div>
					@endif
				</marquee>
			</div>
		</div>
	</div>

	<div class="homepage_banner">
		<div class="container-fluid">
			<div class="row homepage-banner">
				<div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 pad-left0">
					@foreach($rulerdetails as $key => $ruler)
					<h2 class="subheading">
						@if( config::get('app.locale') == 'en')
						{{$ruler->en_title}}
						@else
						{{$ruler->ar_title}}
						@endif
					</h2>

					<div class="kingpic">
						<a href="{{url('/')}}/publications/390/كتاب-صاحب-السمو-الشيخ-حمد-بن-محمد-الشرقي-2018"> 
							<img src="{{url('/')}}/images/ruler/{{Helper::imageCheck($ruler->image)}}" class="img-responsive">
						</a>
					</div>

					<div class="king-content">
						@if ( Config::get('app.locale') == 'en') 
						<p>
							{!!str_limit($ruler->en_description,160)!!}
							<a href="#" class="read_more" data-toggle="modal" data-target="#ruler">{{trans('common.read_more')}} &#8594;</a>
						</p>
						@elseif ( Config::get('app.locale') == 'ar')
						<p>
							<!-- {!!str_limit($ruler->ar_description,100)!!} -->
							{!! Helper::text_cut($ruler->ar_description,200) !!}

							<a href="#" class="read_more" data-toggle="modal" data-target="#ruler">{{trans('common.read_more')}} &#8592;</a>
						</p>
						@endif
					</div>
					@endforeach
				</div>
				<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
					<h2 class="subheading">
						@if( config::get('app.locale') == 'en')
						News
						@else
						أخبار المكتب الإعلامي
						@endif
					</h2>
					<!--Slider Start-->
					<div id="home-slider">
						<div id="bootstrap-touch-slider" class="carousel bs-slider slide  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="5000" >
							<ol class="carousel-indicators">
								<li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
								<li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
								<li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner" role="listbox">
								@if(count($newsHome))
								@php $type = 'news'; @endphp
								@foreach($newsHome as $key => $eventslist)
								<div class="zoomeffect item {{ ($key == 0) ? 'active' : '' }} " id="{{$key}}">
									<a href="{{url('/')}}/{{$typ}}/{{$eventslist->id}}/{{Helper::generateslug($eventslist->id)}}">
										<img alt="{{$eventslist->en_title}}" src="{{asset('/')}}images/news/{{Helper::imageCheck($eventslist->image)}}" class="img-responsive slider-img">
										<div class="overlay">
											<div class="slide-text slide_style_left">
												@if ( Config::get('app.locale') == 'en') 
												<div class="slide-inner-text">
													<h4 data-animation="animated fadeInLeft">{{str_limit($eventslist->en_title,80)}}</h4>
													<p><span class="labelbox">News </span> {{date('M j, Y', strtotime($eventslist->news_date))}} </p>
												</div>
												@elseif ( Config::get('app.locale') == 'ar')
												<div class="slide-inner-text">
													<h4 data-animation="animated fadeInLeft">
														{{str_limit($eventslist->ar_title,100)}}
													</h4>
													<p><span class="labelbox">أخبار </span> 
														{{ Helper::ArabicDate(date('M j, Y', strtotime($eventslist->news_date))) }}</p>
													</div>
													@endif
												</div>
											</div>
										</a>
									</div>
									@endforeach
									@endif
								</div>
							</div>
						</div>
						<!--Slider End-->
						<div class="eventshome">
							<div class="main-content">
								<div class="owl-carousel owl-theme">
									@if(count($newsHomeCaro)>0)
									@php $typ = 'news'; @endphp
									@foreach ($newsHomeCaro as $newslist)
									<div class="item">
										<a href="{{url('/')}}/{{$typ}}/{{$newslist->id}}/{{Helper::generateslug($newslist->id)}}"
											@if ( Config::get('app.locale') == 'en') 
											title="{{$newslist->en_title}}"
											@else
											title="{{$newslist->ar_title}}"
											@endif
											>
											<div class="row">

												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padleft0">
													<img alt="{{$newslist->en_title}}" src="{{asset('/')}}images/news/{{Helper::imageCheck($newslist->image)}}" class="img-responsive zoomeffect">
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 padleft0 event-slide-cnt">
													@if ( Config::get('app.locale') == 'en') 
													<p>
														{{str_limit($newslist->en_title,50)}}
													</p>
													@if($newslist->news_date)
													<p><span class="labelbox">News </span> {{date('M j, Y', strtotime($newslist->news_date))}}</p>
													@else
													<p><span class="labelbox">News </span> {{date('M j, Y', strtotime($newslist->publish_on))}}</p>
													@endif
													@elseif ( Config::get('app.locale') == 'ar')
													<p>
														{{str_limit($newslist->ar_title,65)}}
													</p>
													@if($newslist->news_date)
													<p><span class="labelbox">أخبار </span> 

														{{ Helper::ArabicDate(date('M j, Y', strtotime($newslist->news_date))) }}</p>
														@else
														<p><span class="labelbox">أخبار </span> {{ Helper::ArabicDate(date('M j, Y', strtotime($newslist->publish_on)))}}</p>
														@endif
														@endif
													</div>
												</div>
											</a>
										</div>

										@endforeach
										@endif
									</div>
									<div class="owl-theme">
										<div class="owl-controls">
											<div class="custom-nav owl-nav"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="homemedia">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-5 homemedia_section1">
							<h4 class="subheading">{{trans('common.latest_video')}}</h4>
							<div class="videoslider">
								<div class="owl-carousel">
									@if(count($lvideogallery)>0)
									@foreach ($lvideogallery as $imgkey => $videogallist)
									<div> 
										<a class="bla-1" href="{{$videogallist->video_url}}">
											<div class="image">
												<img src="{{url('/')}}/images/videogallery/{{$videogallist->image}}" class="img-responsive videoimg zoomeffect">
												<p><span class="labelbox">{{trans('common.gallery_videos')}}</p>
												</div>
												<img src="{{url('/')}}/images/video-playicon.png" class="img-responsive video-icon">
											</a>
											@if ( Config::get('app.locale') == 'en') 
											<div class="content">
												<p title="{{$videogallist->en_title}}">
													{{str_limit($videogallist->en_title,30)}}
												</p>
												<p>
													@if($videogallist->video_date)
													{{date('M j, Y', strtotime($videogallist->video_date))}}
													@else
													{{date('M j, Y', strtotime($videogallist->publish_on))}}
													@endif
												</p>
											</div>
											@elseif ( Config::get('app.locale') == 'ar') 
											<div class="content">
												<p title="{{$videogallist->ar_title}}">
													{{str_limit($videogallist->ar_title,92)}}
												</p>
												<p>
													@if($videogallist->video_date)
													{{ Helper::ArabicDate(date('M j, Y', strtotime($videogallist->video_date)))}}
													@else
													{{ Helper::ArabicDate(date('M j, Y', strtotime($videogallist->publish_on)))}}
													@endif
												</p>
											</div>
											@endif
										</div>
										@endforeach
										@endif

									</div>
								</div>
								@if ( Config::get('app.locale') == 'en')
								<div class="line"><a href="{{Helper::BaseUrl('/gallery#videos')}}"><span class="hvr-icon-back">{{trans('common.view_more_video')}} <i class="fa fa-angle-right fa-lg hvr-icon" aria-hidden="true"></i></span></a></div>
								@elseif ( Config::get('app.locale') == 'ar')
								<div class="line"><a href="{{Helper::BaseUrl('/gallery#videos')}}"><span class="hvr-icon-back">{{trans('common.view_more_video')}} <i class="fa fa-angle-left fa-lg hvr-icon" aria-hidden="true"></i></span></a></div>
								@endif
							</div>
							<div class="col-xs-12 col-sm-7 col-md-5 col-lg-4 homemedia_section2">
								<h4 class="subheading">{{trans('common.latest_photos')}}</h4>
								<div>
									<div class="homegallery">

										@if(count($limagegallery)>0)
										@foreach ($limagegallery as $imgkey => $imgallist)

										@php
										$x = json_decode($imgallist->gallery);
										@endphp
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 homegallery-list">
											<a class="galimg" data-kkk="{{$imgkey}}">
												<img src="{{url('/')}}/images/gallery/{{$x[0]}}" class="img-responsive gallery-img zoomeffect">
												<p class="labelbox">{{trans('common.latest_photos')}}</p>
											</a>
											<div class="tree hide">
												@foreach($x as $xy)
												<a class="{{$imgkey}}" rel="group" data-fancybox='gallery{{$imgkey}}' data-galery="{{$imgkey}}" href="{{url('/')}}/images/gallery/{{$xy}}">
													<img src="{{url('/')}}/images/gallery/{{$xy}}" class="img-responsive gallery-img">
												</a>
												@endforeach  
											</div>

											<!-- <img src="{{url('/')}}/images/homeimgicon.png" class="img-responsive imgicon"> -->
											@if ( Config::get('app.locale') == 'en')
											<div class="details">
												<p title="{{$imgallist->en_title}}">
													{{str_limit($imgallist->en_title,25)}}
												</p>
												<p> 
													@if($imgallist->image_date)
													{{date('M j, Y', strtotime($imgallist->image_date))}}
													@else
													{{date('M j, Y', strtotime($imgallist->publish_on))}}
													@endif
												</p>
											</div>
											@elseif ( Config::get('app.locale') == 'ar')
											<div class="details">
												<p title="{{$imgallist->ar_title}}">
													{{str_limit($imgallist->ar_title,25)}}
													<!-- {{$imgallist->ar_title}} -->
												</p>
												<p>
													@if($imgallist->image_date)
													{{ Helper::ArabicDate(date('M j, Y', strtotime($imgallist->image_date)))}}
													@else
													{{ Helper::ArabicDate(date('M j, Y', strtotime($imgallist->publish_on)))}}
													@endif
												</p>
											</div>
											@endif

										</div>
										@endforeach
										@endif

									</div>
								</div>
								@if ( Config::get('app.locale') == 'en')
								<div class="line"><a href="{{Helper::BaseUrl('/gallery#pictures')}}"><span class="hvr-icon-back">{{trans('common.view_more_photos')}} <i class="fa fa-angle-right fa-lg hvr-icon" aria-hidden="true"></i></span></a></div>
								@elseif ( Config::get('app.locale') == 'ar')
								<div class="line"><a href="{{Helper::BaseUrl('/gallery#pictures')}}"><span class="hvr-icon-back">{{trans('common.view_more_photos')}} <i class="fa fa-angle-left fa-lg hvr-icon" aria-hidden="true"></i></span></a></div>
								@endif
							</div>
							<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 twitter">
								<div class="header">
									<h4 class="subheading">{{trans('common.fujairah_events')}}</h4>
								</div>

								<div class="latest_events">
									@if(count($levents))
									@php $type = 'events'; @endphp
									@foreach($levents as $key => $eventslist)
									@if ( Config::get('app.locale') == 'en') 
									<p><a href="{{url('/')}}/{{$type}}/{{$eventslist->id}}/{{Helper::generateslug($eventslist->id)}}">{{str_limit($eventslist->en_title,60)}}</a></p>
									@elseif ( Config::get('app.locale') == 'ar')
									<p><a href="{{url('/')}}/{{$type}}/{{$eventslist->id}}/{{Helper::generateslug($eventslist->id)}}">{{str_limit($eventslist->ar_title,80)}}</a></p>
									@endif
									@endforeach
									@endif
								</div>

								<div class="event-countdown">
									<?php 
									$types = 'events';
									if(count($tes) != 0){  ?>
									@foreach($tes as $te)

									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 eventimg-section hidden-md hidden-sm hidden-xs">
										<img alt="{{$te->en_title}}" src="{{asset('/')}}images/{{$types}}/thumb/{{Helper::imageCheck($te->image)}}" class="mediaimg">
										<input type="hidden" name="dat" id="dat" value="<?php echo date('M j,Y G:i:s', strtotime($te->event_date)); ?>">

									</div>

									<div class="col-lg-8 col-md-12 col-sm-8 col-xs-12">
										<div class="event-counters"> 
											<div id="demo">
												<ul>

												</ul>
											</div>
											@if ( Config::get('app.locale') == 'en')
											<p>{{str_limit($te->en_title,50)}}</p>

											@else 
											<p title="{{$te->ar_title}}">{{str_limit($te->ar_title,50)}}</p>

											@endif
											@php $typ = 'events'; @endphp
											<p class="view_more"><a href="{{ Helper::BaseUrl('/upcoming-events')}}"><span class="hvr-icon-back">{{trans('common.view_more')}} <i class="fa fa-angle-left fa-lg hvr-icon" aria-hidden="true"></i></span></a></p>
										</div>
									</div>
									<div class="counter-eventdateSection">
										@if ( Config::get('app.locale') == 'en')
										<p class="counter-eventdate">{{$te->event_date}}</p>
										@else
										<p class="counter-eventdate">{{ Helper::ArabicDate(date('M j, Y', strtotime($te->event_date)))}} </p>
										@endif
									</div>
									@endforeach
									<?php }else{ ?>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
										<input type="hidden" name="dat" id="dat" value="0">
										<p id="demos"></p>
									</div>
									<?php } ?>
								</div>

								<!-- <div class="header">
									<h4 class="subheading">{{trans('common.event_calender')}}</h4>
								</div> -->

								<div class="customCalendar">
									<div id="dncalendar-container">
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>


				<div class="mediaoffice">
					<h4 class="heading">{{trans('common.media_office')}}</h4>
					<!-- <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div> -->
	<!-- <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		@if ( Config::get('app.locale') == 'en')
		<p class="text-center">
			Maecenas at dui eget orci ornare eleifend. Ut vitae lorem placerat, sagittis sem quis, vehicula elit. Curabitur maximus eu risus sed dictum. Proin sit amet tempus felis, sit amet efficitur dui. 
		</p>
		@elseif ( Config::get('app.locale') == 'ar')
		<p class="text-center">
			ز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد 
		</p>
		@endif
	</div> -->
	<!-- <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div> -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="inner">
					<h4>{{trans('common.about_us')}}</h4>
					<!-- @if ( Config::get('app.locale') == 'en')
					<p>
						Maecenas at dui eget orci ornare eleifend. Ut vitae lorem placerat, 
					</p>
					@elseif ( Config::get('app.locale') == 'ar')
					<p>
						على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام 
					</p>
					@endif -->
					<ul class="list about-list">
						<li>
							<span>
								<img src="{{url('/')}}/images/home-about.png" alt="{{trans('common.home_media')}}">
							</span>
							<span><a href="{{ Helper::BaseUrl('/about-mediaoffice') }}" data-tab="about-media-officesection">{{trans('common.home_media')}}</a></span>
						</li>
						<li> 
							<span>
								<img src="{{url('/')}}/images/home-vision.png" alt="{{trans('common.home_vision')}}">
							</span>
							<span><a href="{{ Helper::BaseUrl('/about-vision') }}" data-tab="visionsection">{{trans('common.home_vision')}}</a></span>
						</li>
						<li>
							<span>
								<img src="{{url('/')}}/images/home-message.png" alt="{{trans('common.home_message')}}">
							</span>
							<span><a href="{{ Helper::BaseUrl('/about-message') }}" data-tab="messagesection">{{trans('common.home_message')}}</a></span>
						</li>
						<li>
							<span>
								<img src="{{url('/')}}/images/home-team.png" alt="{{trans('common.home_team')}}">
							</span>
							<span><a href="{{ Helper::BaseUrl('/about-gov-communication-team') }}" data-tab="media-team-government-communicationsection">{{trans('common.home_team')}}</a></span>
						</li>
					</ul>

				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="inner">
					<h4>{{trans('common.our_services')}}</h4>

					<ul class="list service-list">
						<li>
							<span>
								<img src="{{url('/')}}/images/home-permission.png" alt="{{trans('common.services_permits')}}">
							</span>
							<span><a href="{{ Helper::BaseUrl('/')}}/permission">{{trans('common.services_permits')}}</a></span>
						</li>
						<li>
							<span>
								<img src="{{url('/')}}/images/home-training.png" alt="{{trans('common.services_training')}}">
							</span>
							<span><a href="{{ Helper::BaseUrl('/')}}/training">{{trans('common.services_training')}}</a></span>
						</li>
						<li>
							<span>
								<img src="{{url('/')}}/images/home-request.png" alt="{{trans('common.services_request')}}">
							</span>
							<span><a href="{{ Helper::BaseUrl('/')}}/request-for-photo-or-video">{{trans('common.services_request')}}</a></span>
						</li>
						<li>
							<span>
								<img src="{{url('/')}}/images/home-library.png" alt="{{trans('common.services_library')}}">
							</span>
							<span><a href="{{ Helper::BaseUrl('/')}}/library">{{trans('common.services_library')}}</a></span>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 publications">
				<div class="inner">
					<h4>{{trans('common.our_publications')}}</h4>
					<div class="thslider-container">
						<section id="dg-container" class="dg-container">
							<div class="dg-wrapper">
								@foreach($publicationli as $pubvalue)
								<a href="{{url('/')}}/publications/{{$pubvalue->id}}/{{Helper::make_slug($pubvalue->ar_title)}}">
									<img src="{{url('/')}}/images/publications/thumb/{{$pubvalue->image}}" alt="image01">
										<!-- @if ( Config::get('app.locale') == 'en')
										<p>{{str_limit($pubvalue->en_title,15)}}</p>
										@elseif ( Config::get('app.locale') == 'ar')
										<p>{{str_limit($pubvalue->ar_title,15)}}</p>
										@endif -->
									</a>
									@endforeach
								</div>
								<nav>	
									<span class="dg-prev"><i class="fa fa-angle-left fa-2x" aria-hidden="true"></i></span>
									<span class="dg-next"><i class="fa fa-angle-right fa-2x" aria-hidden="true"></i></span>
								</nav>
							</section>
						</div>
						<div class="pub-slider hidden-lg hidden-md">
							<div class="owl-carousel">
								@foreach($publicationli as $pubvalue)
								<div>
									<a href="{{url('/')}}/publications/{{$pubvalue->id}}/{{Helper::generateslugforpub($pubvalue->id)}}">
										<img src="{{url('/')}}/images/publications/thumb/{{$pubvalue->image}}" alt="image01">
										<!-- @if ( Config::get('app.locale') == 'en')
										<p>{{str_limit($pubvalue->en_title,15)}}</p>
										@elseif ( Config::get('app.locale') == 'ar')
										<p>{{str_limit($pubvalue->ar_title,15)}}</p>
										@endif -->
									</a>
								</div>
								@endforeach
							</div>
						</div>
						@if ( Config::get('app.locale') == 'en')
						<a href="{{Helper::BaseUrl('/publications')}}" class="view_more hvr-icon-back">{{trans('common.view_more')}} <i class="fa fa-angle-right fa-lg hvr-icon" aria-hidden="true"></i></a>
						@elseif ( Config::get('app.locale') == 'ar')
						<a href="{{Helper::BaseUrl('/publications')}}" class="view_more hvr-icon-back">{{trans('common.view_more')}} <i class="fa fa-angle-left fa-lg hvr-icon" aria-hidden="true"></i></a>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade event-modal" id="myModal" role="dialog">
	<div class="modal-dialog" role="document">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title subheading">{{trans('common.events_list')}}</h4>
			</div>
			<div class="modal-body">
				<div id="myDiv"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default event-btn" data-dismiss="modal">
					@if ( Config::get('app.locale') == 'en')
					Close
					@elseif ( Config::get('app.locale') == 'ar')
					أغلق
					@endif
				</button>
			</div>
		</div>

	</div>
</div>


<!-- Modal -->
<div id="ruler" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content ruler_detailsection">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title subheading">@if ( Config::get('app.locale') == 'en') 
					{{$ruler->en_title}}
					@elseif ( Config::get('app.locale') == 'ar')
					{{$ruler->ar_title}}
					@endif
				</h3>
			</div>
			
			<div class="modal-body">
				<div class="ruler_cntsection">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<img src="{{url('/')}}/images/ruler/{{Helper::imageCheck($ruler->image)}}" class="img-responsive">
						</div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							
							@if ( Config::get('app.locale') == 'en') 

							<h4>Lorem Ipsum</h4>
							<p>
								{!!($ruler->en_description)!!}	
							</p>
							@elseif ( Config::get('app.locale') == 'ar')

							<h4>عضو المجلس الأعلى حاكم الفجيرة</h4>
							<p>
								{!!($ruler->ar_description)!!}
							</p>
							@endif
						</div>
						
					</div>
				</div>
			</div>

		</div>

	</div>
</div>

@endsection

@section('js')
<!-- <script src="{{asset('/public')}}/js/jquery.marquee.min.js"></script>
<script src="{{asset('/public')}}/js/jquery.pause.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<script>
	$(function(){
		$('.marquee').marquee({
			allowCss3Support: true,
			css3easing: 'linear',
			easing: 'linear',
			delayBeforeStart: 1000,
			direction: 'right',
			duplicated: false,
			duration: 7000,
			gap: 20,
			pauseOnCycle: false,
			pauseOnHover: true,
			startVisible: false

		});
	});
</script> -->

<script>

	$(document).ready(function(){	
		$(".galimg").on("click", function(){
			var boolKey = $(this).data('kkk');
			$('a.'+boolKey)[0].click();
		});
	});

	$(document).ready(function(){
		$('.main-content .owl-carousel').owlCarousel({
			// stagePadding: 50,
			autoplay: true,
			dots:false,
			@if ( Config::get('app.locale') == 'ar')
			rtl: true,
			@endif
			loop: true,
			margin: 10,
			nav: true,
			navText: [
			'<i class="fa fa-angle-left" aria-hidden="true"></i>',
			'<i class="fa fa-angle-right" aria-hidden="true"></i>'
			],
			navContainer: '.main-content .custom-nav',
			responsive:{
				0:{
					items: 1
				},
				600:{
					items: 1
				},
				1000:{
					items: 2
				}
			}
		});


		$('.videoslider .owl-carousel').owlCarousel({
			loop: true,
			autoplay: true,
			margin: 10,
			dots: false,
			@if ( Config::get('app.locale') == 'ar')
			rtl: true,
			@endif
			responsive:{
				0:{
					items:1,
				},
				600:{
					items:1,
				},
				1000:{
					items:1,
				}
			}
		})
		$('.pub-slider .owl-carousel').owlCarousel({
			loop:true,
			margin:10,
			autoplay: true,
			nav:true,
			dots: true,
			@if ( Config::get('app.locale') == 'ar')
			rtl: true,
			@endif
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:false,
					dots:true,
				},
				600:{
					items:4,
					nav:true,
					dots:true,
				},
				1000:{
					items:3,
					nav:true,
					loop:false,
					dots:true,
				}
			}
		})
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://www.jqueryscript.net/demo/Super-Simple-Event-Calendar-Plugin-For-jQuery-dnCalendar/js/dncalendar.min.js"></script>
<script type="text/javascript">
	$(function(){
		$("a.bla-1").YouTubePopUp();
		$("a.bla-2").YouTubePopUp( { autoplay: 0 } ); // Disable autoplay
	});

	$(document).ready(function() {
		var bn = [];
		var tle;
		var dte;
		var eveid;
		var slugs;
		var APP_URL = "{{url('/')}}";
		var my_var = <?php echo $levents_cal; ?>;

		$.each(my_var, function (i) {
			$.each(my_var[i], function (key, val) { 
				if(key == 'id'){
					eveid = val;
				}

				<?php if( Config::get('app.locale') == 'en' ) {  ?>
					if(key == 'en_slug'){
						slugs = val;
					}
					<?php } elseif( Config::get('app.locale') == 'ar' ) { ?>
						if(key == 'ar_slug'){
							slugs = val;
						}
						<?php } ?>

						<?php if( Config::get('app.locale') == 'en' ) {  ?>
							if(key == 'event_date_on' || key == 'en_title') {

								tle = val;
								if(key == 'en_title') {
									dte = val;
								}

								bn.push({"date":tle, "note": [dte],"ids":eveid,"slg":slugs})
							}
							<?php } elseif( Config::get('app.locale') == 'ar' ) { ?>
								if(key == 'event_date_on' || key == 'ar_title') {

									tle = val;
									if(key == 'ar_title') {
										dte = val;
									}

									bn.push({"date":tle, "note": [dte],"ids":eveid,"slg":slugs})
								}
								<?php } ?>

							});
		});

		var fd = JSON.stringify(bn);
		var pars = JSON.parse(fd);

		var my_calendar = $("#dncalendar-container").dnCalendar({

			minDate: "2019-08-01",
			maxDate: "2020-12-02",
			defaultDates: "2019-08-28",
			monthNames: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
			monthNamesShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec' ],
			dayNames: [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			dayNamesShort: [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ],
			dataTitles: { defaultDate: 'Now', today : 'Today' },
			notes: pars,
			showNotes: true,
			startWeek: 'monday',
			dayClick: function(date, view) {
                	//console.log(pars);

                	var gf = date.getMonth()+1;
                	var dt = date.getDate();
                	var hg = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
                	var hmonth = ""+gf+"";
                	var hday = ""+dt+"";
                	var newdat;
                	var newnote;
                	var newnoteid;
                	var newnoteslg;


                	if(hday.length == 1){
                		dday = "0"+hday;
                	}else{
                		dday = dt;
                	}

                	if(hmonth.length == 1){
                		dmonth = "0"+hmonth;
                	}else{
                		dmonth = gf;
                	}

                	fin = date.getFullYear() + "-" + dmonth + "-" + dday;

                	$.each(pars, function(j){
                		$.each(pars[j], function(key,val){

                			if(key == 'date' || key == 'note' || key == 'ids' || key == 'slg'){
                				newdat = val;
                				if(key == 'note'){
                					newnote = val;
                				}
                				if(key == 'ids'){
                					newnoteid = val;
                				}
                				if(key == 'slg'){
                					newnoteslg = val;
                				}

                				if(newdat == fin){
                					$('#mySecondDiv').remove();
                					var mySecondDiv = $('<div id="mySecondDiv"> <ul> <li> <a href='+APP_URL+'/events/'+newnoteid+'/'+newnoteslg+'>'+ newnote +'</a></li></ul></div>');
                					$("#myModal").modal("show");
                					$('#myDiv').append(mySecondDiv);
                				}
                			}
                		});
                	});
                }
            });

			// init calendar
			my_calendar.build();

			// update calendar
			/*my_calendar.update({
				minDate: "2019-08-01",
			 	defaultDate: "2019-08-28"
			 });*/
			});

		</script>
		<!-- Event Countdown Timer -->
		<script>
  // Set the date we're counting down to
  var fd = $("#dat").val();
  console.log(fd);
  
  var countDownDate = new Date(fd).getTime();
  // var countDownDate = new Date("Dec 23 2019").getTime();

  // Update the count down every 1 second
  $('.latest_events').hide();
  $('.event-countdown').show();
  var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  // document.getElementById("demo").innerHTML = '<ul><li><span>'+days+'</span>' + "<p>days</p></li>" + '<li><span>'+hours+'</span>' + "<p>hours</p></li>"+ '<li><span>'+minutes+'</span>' + "<p>minutes</p></li> " +'<li><span>'+seconds+'</span>' + "<p>seconds</p></li></ul>";
  // $("#demo").html('tttt');
  <?php if( Config::get('app.locale') == 'en' ) {  ?>
  	$("#demo ul").html('<li><span>'+days+'</span>' + "<p>days</p></li>" + '<li><span>'+hours+'</span>' + "<p>hours</p></li>"+ '<li><span>'+minutes+'</span>' + "<p>minutes</p></li> " +'<li><span>'+seconds+'</span>' + "<p>seconds</p></li>");
  	<?php } else { ?>
  		$("#demo ul").html('<li><span>'+days+'</span>' + "<p>أيام</p></li>" + '<li><span>'+hours+'</span>' + "<p>ساعات</p></li>"+ '<li><span>'+minutes+'</span>' + "<p>الدقائق</p></li> " +'<li><span>'+seconds+'</span>' + "<p>ثواني</p></li>");
  		<?php } ?>

  // If the count down is finished, write some text
  if(distance < 0 ) {
  	document.getElementById("demo").innerHTML = "اليوم";
//   window.location.reload();
}
}, 1000);

  if(fd == 0){
  	$('.latest_events').show()
  	$('.event-countdown').hide();
    //document.getElementById("demos").innerHTML = "There is no events for this month";
}

</script>
<!-- End Event Countdown Timer -->
@endsection