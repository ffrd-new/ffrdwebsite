@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','About Us | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','من نحن | المكتب الإعلامي لحكومة الفجيرة ')
@endif

@section('content')
<div class="about-page">
	<div class="container-fluid">
		<div class="row topcategory">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@if ( Config::get('app.locale') == 'en') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
					<li><a href="{{Helper::BaseUrl('/about-message')}}" class="active">Message</a></li>	
				</ul>
				@elseif ( Config::get('app.locale') == 'ar') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
					<li><a href="{{Helper::BaseUrl('/about-message')}}" class="active">الرسالة </a></li>	
				</ul>
				@endif
			</div>
		</div>


	<div class="row flip-section">
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 service_content">
			<div class="heading-section">
				@if ( Config::get('app.locale') == 'en') 
				<h3 class="textcapital">Message</h3>
				@elseif ( Config::get('app.locale') == 'ar')
				<h3 class="textcapital">الرسالة</h3>
				@endif
			</div>

			@if ( Config::get('app.locale') == 'en') 
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							@elseif ( Config::get('app.locale') == 'ar')
							<p>يسعى المكتب الإعلامي لحكومة الفجيرة إلى إيجاد رافد رسمي فعال لتزويد وسائل الإعلام بالأخبار والمعلومات الدقيقة حول الحاكم والحكومة وكافة دوائرها وهيئاتها ومؤسساتها بما يضمن توصيل الرسالة الإعلامية للحكومة بأسلوب فعال يمتاز بالوضوح والموضوعية والشفافية ووفقاً لأرقى المعايير والممارسات. </p>
							@endif
		</div>	
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<div class="services_listsection">
				<h3 class="textcapital">{{trans('common.about_us')}}</h3>
				<ul>
					<li><a href="{{Helper::BaseUrl('/about-mediaoffice')}}">{{trans('common.home_media')}}</a></li>
					<li><a href="{{Helper::BaseUrl('/about-vision')}}">{{trans('common.home_vision')}}</a></li>
					<li class="active"><a href="{{Helper::BaseUrl('/about-message')}}">{{trans('common.home_message')}}</a></li>
					<li><a href="{{Helper::BaseUrl('/about-gov-communication-team')}}">{{trans('common.home_team')}}</a></li>
				</ul>
			</div>
		</div>
	</div>

</div>
</div>
@endsection
@section('js')
<script type="text/javascript">

</script>
@endsection
