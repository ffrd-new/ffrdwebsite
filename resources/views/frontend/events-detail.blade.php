@extends('layouts.front')
@section('content')

<?php 
if ( Config::get('app.locale') == 'en')
	{ 
		$seo = Helper::seo($news_detail->en_title,str_limit($news_detail->en_meta_desc,'150'),$news_detail->image,$news_detail->en_meta_title,$news_detail->en_meta_keywords,$type);
	}
	elseif(Config::get('app.locale') == 'ar'){
		$seo = Helper::seo($news_detail->ar_title,str_limit($news_detail->ar_meta_desc,'150'),$news_detail->image,$news_detail->ar_meta_title,$news_detail->ar_meta_keywords,$type);
	}

	$meta = filter_var($seo['meta'], FILTER_SANITIZE_STRING);
	$meta_title = filter_var($seo['meta_title'], FILTER_SANITIZE_STRING);
	$meta_keyword = filter_var($seo['meta_keyword'], FILTER_SANITIZE_STRING);

	?>
	@section('title',$seo['meta_title'])
	@section('keywords',$seo['meta_keyword'])
	@section('meta-title',$seo['meta_title'])
	@section('description',$meta)
	@section('image',$seo['image'])
	<div class="detailPage">
		<div class="container-fluid">
			<div class="topcategory">
				@if ( Config::get('app.locale') == 'en') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
					<li><a href="{{Helper::BaseUrl('/upcoming-events')}}">Events</a></li>
					<li><a href="#" class="active">{{str_limit($news_detail->en_title,30)}}</a></li>
				</ul>
				@elseif ( Config::get('app.locale') == 'ar') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
					<li><a href="{{Helper::BaseUrl('/upcoming-events')}}">فعاليات </a></li>
					<li><a href="#" class="active">{{str_limit($news_detail->ar_title,30)}}</a></li>
				</ul>
				@endif
			</div>

		</div>
		<div class="row">
			<div class="container-fluid">

				<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 events">
					<div class="events-details">
						@if ( Config::get('app.locale') == 'en')
						<h3>{{$news_detail->en_title}}</h3>
						<hr>
						<div class="shareicon"> 
							<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
								<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
									<h5><i class="fa fa-calendar" aria-hidden="true"></i> {{date('M j - Y', strtotime($news_detail->event_date))}}</h5>
								</div>
								<div class="col-md-8 col-lg-8 col-sm-6 col-xs-12 event-location">
									<i class="fa fa-map-marker evntloc" aria-hidden="true"></i>
									<p>{{str_limit($news_detail->en_location,100)}}</p>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
								<p>
									<a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(url()->current())}}" target="_blank">
										<i class="mdi mdi-facebook-box mdi-24px"></i></a>

										<a href="https://twitter.com/intent/tweet?url={{urlencode(url()->current())}}" target="_blank">
											<i class="mdi mdi-twitter-box mdi-24px"></i>
										</a>
									</p>
								</div>
							</div>

							<div class="events-detail-content">
								<p>
									{!!$news_detail->en_description!!}
								</p>
							</div>
							<div class="events-detail-img">
								<img alt="{{$news_detail->en_title}}" src="{{asset('/')}}images/{{$type}}/{{Helper::imageCheck($news_detail->image)}}" class="mediaimg">
							</div>
							@elseif( Config::get('app.locale') == 'ar')
							<h3 class="subheading">{{$news_detail->ar_title}}</h3>
							<hr>
							<div class="shareicon"> 
								<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
									<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
										<!-- <h5><i class="fa fa-calendar" aria-hidden="true"></i> {{date('M j, Y', strtotime($news_detail->event_date))}} -->
											<h5><i class="fa fa-calendar" aria-hidden="true"></i> {{ Helper::ArabicDateMnth(date('M j, Y', strtotime($news_detail->event_date)))}}
											</h5>
										</div>
										<div class="col-md-8 col-lg-8 col-sm-6 col-xs-12 event-location">
											<i class="fa fa-map-marker evntloc" aria-hidden="true"></i> 
											<p>{{str_limit($news_detail->ar_location,100)}}</p>
										</div>	
									</div>
									<div class="col-md-3 col-lg-3">
										<p>
											<a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(url()->current())}}" target="_blank">
												<i class="mdi mdi-facebook-box mdi-24px"></i></a>

												<!-- <a href="https://twitter.com/intent/tweet?url={{urlencode(url()->current())}}" target="_blank">
													<i class="mdi mdi-twitter-box mdi-24px"></i>
												</a> -->
									<?php 
									$p =  Request::segment(2);
									?>
									<a href="https://twitter.com/intent/tweet?text={{$news_detail->ar_title}}&url={{urlencode(url('/events/'.$p.'/events-fcmc'))}}" target="_blank" data-lang="ar">
										<i class="mdi mdi-twitter-box mdi-24px"></i>	
									</a>
													<!-- <a href="https://twitter.com/intent/tweet?url={{urlencode(url()->current())}}" target="_blank" data-lang="ar">
										<i class="mdi mdi-twitter-box mdi-24px"></i>
										
									</a> -->
											</p>
										</div>
									</div>

									<div class="events-detail-content">
										<p>
											{!!$news_detail->ar_description!!}
										</p>
									</div>
									<div class="events-detail-img">
										<img alt="{{$news_detail->ar_title}}" src="{{asset('/')}}images/{{$type}}/{{Helper::imageCheck($news_detail->image)}}" class="mediaimg">
									</div>
									@endif
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 relatedevents">
								<h3>{{trans('common.upcoming_events')}}</h3>
								<hr>
								@foreach($recentnews as $recentzz)
								@if ( Config::get('app.locale') == 'en')
								<div class="list">

									<a href="{{url('/')}}/events/{{$recentzz->id}}/{{Helper::generateslug($recentzz->id)}}" class="taghighlight">
										<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"> -->
											<div class="content">
												<img alt="{{$recentzz->en_title}}" src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($recentzz->image)}}" class="mediaimg">
											</div>
											<div class="content">
												<p title="{{$recentzz->en_title}}">
													{{str_limit($recentzz->en_title,30)}}</p>

													@if($recentzz->event_date)
														<small>  {{date('M j - Y', strtotime($recentzz->event_date))}} </small>
														@else
														<small> {{date('M j - Y', strtotime($recentzz->publish_on))}}</small>
														@endif
													
												</div>
												<!-- </div> -->

											</a>
										</div>
										@elseif ( Config::get('app.locale') == 'ar')
										<div class="list">
											<a href="{{url('/')}}/events/{{$recentzz->id}}/{{Helper::generateslug($recentzz->id)}}" class="taghighlight" title="{{$recentzz->ar_title}}">
												<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"> -->
													<div class="content">
														<img alt="{{$recentzz->ar_title}}" src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($recentzz->image)}}" class="mediaimg">
													</div>
													<div class="content">
														<p title="{{$recentzz->ar_title}}">
															{{str_limit($recentzz->ar_title,30)}}
														</p>
														<!-- <small>  {{ Helper::ArabicDate(date('M j, Y', strtotime($recentzz->publish_on)))}}</small> -->

														@if($recentzz->event_date)
														<small>  {{ Helper::ArabicDate(date('M j, Y', strtotime($recentzz->event_date)))}}</small>
														@else
														<small>   {{ Helper::ArabicDate(date('M j, Y', strtotime($recentzz->publish_on)))}}</small>
														@endif

													</div>
													<!-- </div> -->

												</a>
											</div>
											@endif
											@endforeach
										</div>
									</div>
								</div>
							</div>
							@endsection