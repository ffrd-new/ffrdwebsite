@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','Privacy Policy | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','سياسة الأمن والخصوصية | المكتب الإعلامي لحكومة الفجيرة')
@endif

@section('content')
<div class="privacy-page">
	<div class="container-fluid">
		<div class="topcategory">
			@if ( Config::get('app.locale') == 'en') 
			<ul class="breadcrumb">
				<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
				<li><a href="{{Helper::BaseUrl('/privacy-policy')}}" class="active">Privacy Policy</a></li>	
			</ul>
			@elseif ( Config::get('app.locale') == 'ar') 
			<ul class="breadcrumb">
				<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
				<li><a href="{{Helper::BaseUrl('/privacy-policy')}}" class="active">سياسة الأمن والخصوصية </a></li>	
			</ul>
			@endif
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@if ( Config::get('app.locale') == 'en') 
				<h3>Privacy Policy</h3>
				<div class="content-section">
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
				</div>
				@elseif ( Config::get('app.locale') == 'ar')
				<h3>سياسة الأمن والخصوصية</h3>

				<div class="content-section">
					<h4>بيان بخصوصية الموقع الإلكتروني</h4>
					<p>تم إعداد بيان الخصوصية هذا من أجل عرض التزام المكتب الإعلامي – حكومة الفجيرة بالخصوصية. وبسبب الطبيعة الحساسة للمعلومات التي نجمعها من مستخدمينا، بما في ذلك العناوين وأرقام الهواتف نعتقد انه من المهم تلبية متطلبات الخصوصية الشديدة. فيما يلي توضيح لأساليبنا في جمع المعلومات ونشرها على هذا الموقع الإلكتروني:-</p>
					<h4>جمع المعلومات:</h4>
					<p>يصف هذا البند المعلومات التي نجمعها منكم وتشير إلى الأهداف الرئيسية لأسباب جمع كل نوع من المعلومات منكم. وإذا استخدمنا معلوماتكم أو كشفنا أو وزعنا معلوماتكم للغير لهدف ليس له أي علاقة بالأهداف التالية، نقوم بإبلاغكم بواسطة البريد الإلكتروني عن حقكم في اختيار عدم المشاركة في ذلك الاستخدام، الكشف أو التوزيع المسبق. ويتم الحكم على حقك باختيار عدم المشاركة من خلال بند الخيار/اختيار عدم المشاركة أدناه</p>
					<p>كما تم الوصف أكثر تفصيلاً أدناه ، نستخدم عنوانك IP للمساعدة في تشخيص المشاكل مع مقدم خدماتنا لإدارة موقعنا الإلكتروني. ويستخدم أو قد يستخدم عنوانك IP  للمساعدة على تعريفك لمتابعة النشاط ضمن موقعنا ولجمع معلومات سكانية كثيرة. ويقدم مقدم الخدمة بجمع المعلومات بجمع IP . ويستخدم موقعنا رموزا للحفاظ على هويتك. وقد نستخدم رموزا لتوصيل محتويات تهمك أنت بالتحديد.تحتاج استمارة التسجيل والطلب في موقعنا من المستخدم تزويدنا بمعلومات عن الاتصال به (مثل الاسم وعنوان البريد الإلكتروني)، المعلومات المالية (مثل، رقم الحساب أو رقم بطاقة الائتمان)، والمعلوماتالسكانية (مثل، العمر أو مستوى الدخل). ونستخدم معلومات الاتصال بالمستخدم للاتصال بالمستخدم عند الضرورة. وقد يختار المستخدمون عن المشاركة في استلام البريد في المستقبل كما ذكر سابقا في بندالاختيار/عدم الاختيار أدناه. والمعلومات المالية التي يتم جمعها تستخدم للكشف عن مؤهلات المستخدم للتسجيل ولمحاسبة المستخدم على المنتجات والخدمات.</p>
					<p>يتم جمع البيانات السكانية والسيرة الذاتية في موقعنا الإلكتروني. ونستخدم هذه البيانات لتحديد خبرة المستخدمين في موقعنا الإلكتروني لنبين لهم المحتويات التي قد تهمهم وعرض المحتويات طبقا لا فضلياتهم.</p>
					<p>وقد تم تقاسم المعلومات السكانية مع المعلنين على أساس التجميع. أسس التجميع تعني أن معلوماتكم سيتم تعميمها مع البيانات التي نجمعها من جميع مستخدمينا. فمثلا، قد نتقاسم معدل العمر أو مستوى الدخل عند مستخدمينا مع المعلنين. ولن نتقاسم المعلومات المعروفة شخصيا، مثل معلومات الاتصالات، المعلومات المالية والمعلومات السكانية، ومع المعلنين حول أي شخص معين يستخدم مواقعنا الإلكترونية. </p>
					<h4>الاتصال بالموقع الإلكتروني</h4>
					<p>إذا كان لديك أي سؤال حول بيان الخصوصية هذا، إجراءات هذا الموقع أو تعاملاتك مع هذا الموقع الإلكتروني، بامكانك الاتصال بنا على( <a href="mailto:	info@media.fujairah.ae">اضغط هنا</a> ).</p>
				</div>
				@endif
				
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>

</script>
<script type="text/javascript">

</script>
@endsection
