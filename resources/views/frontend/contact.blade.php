@extends('layouts.front')

@if ( Config::get('app.locale') == 'en') 
@section('title','Call Us | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title',' اتصل بنا | المكتب الإعلامي لحكومة الفجيرة ')
@endif

@section('content')
<div class="contactpage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="topcategory">
					@if ( Config::get('app.locale') == 'en') 
					<ul class="breadcrumb">
						<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
						<li><a href="{{Helper::BaseUrl('/contact')}}" class="active">Call Us</a></li>	
					</ul>
					@elseif ( Config::get('app.locale') == 'ar') 
					<ul class="breadcrumb">
						<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
						<li><a href="{{Helper::BaseUrl('/contact')}}" class="active">اتصل بنا</a></li>	
					</ul>
					@endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3612.330488988927!2d56.331243015008084!3d25.124515283930034!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ef4f88be83b2343%3A0xc55a35ba01e1e073!2sEmiri%20Court!5e0!3m2!1sen!2sin!4v1567582467031!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h3 class="">{{trans('common.contact')}}</h3>
				@if ( Config::get('app.locale') == 'en')
				<p class="">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc commodo a neque et convallis. Nunc cursus at nisi vitae faucibus. Nunc sit amet tristique elit. Phasellus porta in 
				</p>
				@elseif ( Config::get('app.locale') == 'ar')
				<p class="">
					يسر المكتب الاعلامي لحكومة الفجيرة التواصل معكم، وتلبية كل متطلبات شركائها من الجهات الإعلامية والثقافية، والاستجابة لكافة استفساراتهم بما يخدم الرسالة الإعلامية والحضارية للإمارات العربية المتحدة وإمارة الفجيرة ومصداقيتها…ويبرز الإنجازات فيهما.  
				</p>
				@endif
				<div class="row contactdet contact-details">

					<div class="contact-item">
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<i class="mdi mdi-map-marker-outline mdi-24px"></i>
						</div>
						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">
							<p><a href="{{ Helper::BaseUrl('/contact')}}">{{trans('common.location')}}</a></p>
						</div>
					</div>

					<div class="contact-item">
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<i class="mdi mdi-clock-outline mdi-24px"></i>
						</div>
						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">
							<p>{{trans('common.working_hours')}} </p>
						</div>
					</div>

					<!-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="">
							<h4><i class="mdi mdi-map-marker-outline mdi-24px"></i></h4>
							<h4>{{trans('common.location')}}</h4>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="">
							<h4><i class="mdi mdi-clock-outline mdi-24px"></i></h4>
							<h4>{{trans('common.working_hours')}} </h4>
						</div>
					</div> -->

					<div class="contact-item">
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<i class="mdi mdi-email-outline mdi-24px"></i>
						</div>
						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">
							<p><a href="mailto:info@media.fujairah.ae">info@media.fujairah.ae</a> </p>
						</div>
					</div>

					<div class="contact-item">
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<i class="fa fa-phone fa-lg" aria-hidden="true"></i>
						</div>
						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">
							<p><a href="tel:09 205 0851" class="contact_no" dir="ltr">09 205 0851</a> </p>
						</div>
					</div>
					<div class="contact-item">
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
							<i class="fa fa-fax"></i>
						</div>
						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">
							<p><a href="tel:09 222 3399" class="contact_no" dir="ltr">09 222 3399</a></p>
						</div>
					</div>

					<div class="clearfix"></div>
					
				</div>				
			</div>
		</div>
	</div>
</div>
@endsection