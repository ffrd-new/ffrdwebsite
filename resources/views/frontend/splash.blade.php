<!DOCTYPE html>
<html lang="en" ng-app="mymodule">
<head>
  <title>@yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="Robots" CONTENT="NOINDEX, NOFOLLOW" />
  <meta name="description" content="@yield('description')">
  <meta name="keywords" content="@yield('keywords')">
  <meta name="meta-title" content="@yield('meta-title')">
  <!-- <link rel="canonical" href="Site url" /> -->
  <link rel="shortcut icon" href="{{url('/')}}/images/favicon.png">
  <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css">


  <link rel="stylesheet" href="{{url('/')}}/css/style.css">
  <link rel="stylesheet" href="{{url('/')}}/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{url('/')}}/css/jquery.fancybox.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/testimonial.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/materialdesignicons.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/owl.carousel.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/owl.theme.default.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/3dslider/demo.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/3dslider/style.css" />
  <link rel="stylesheet" type="text/css" href="{{asset('/')}}css/YouTubePopUp.css">
  <link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/demo/Super-Simple-Event-Calendar-Plugin-For-jQuery-dnCalendar/css/dncalendar-skin.min.css" />
  <link rel="stylesheet" href="{{ asset('css/admin/datepicker/bootstrap-datepicker.css') }}">
  @if ( Config::get('app.locale') == 'ar')
  <link rel="stylesheet" href="{{url('/')}}/css/bootstrap-flipped.css">
  <link rel="stylesheet" href="{{url('/')}}/css/style_ar.css">
  <link rel="stylesheet" href="{{url('/')}}/css/responsive_ar.css">
  <!-- <link rel="stylesheet" href="{{url('/')}}/css/responsive.css"> -->
  @elseif ( Config::get('app.locale') == 'en')
  <link rel="stylesheet" href="{{url('/')}}/css/style_en.css">
  <link rel="stylesheet" href="{{url('/')}}/css/responsive.css">
  @endif
  @stack('styles')
  

  <meta property="og:url"                content="{{url()->current()}}" />
  <meta property="og:type"               content="article" />
  <meta property="og:title"              content="@yield('title')" />
  <meta property="og:description"        content="@yield('description')" />
  <meta property="og:image"              content="@yield('image')" /> 

</head>
<body>
<div class="splash-content">
	<div class="logosection hidden-lg hidden-md hidden-sm">
		<a href="{{Helper::BaseUrl('/')}}">
			<img src="{{url('/')}}/images/logo.png" class="img-responsive logo">
		</a>
	</div>
     <video width="100%" height="100%" autoplay loop muted>
		  <source src="{{url('/')}}/images/bg.mp4" type="video/mp4" >
		  <source src="{{url('/')}}/images/bg.mp4" type="video/mov" >
	</video>
</div>
</body>

<script src="{{url('/')}}/js/jquery-3.3.1.min.js"></script>
<script src="{{url('/')}}/js/3dslider/modernizr.custom.53451.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/js/owl.carousel.min.js"></script>
<script src="{{url('/')}}/js/jquery.fancybox.min.js"></script>
<script src="{{url('/')}}/js/3dslider/jquery.gallery.js"></script>
<script src="{{asset('/')}}js/YouTubePopUp.jquery.js"></script>
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
<script src="{{ asset('js/admin/datepicker/bootstrap-datepicker.js') }}"></script>

 
<script type="text/javascript">
  // $(window).on('load',function() {
  //   $("#loading").delay(10000).hide('slow');
  // });
  var url = "{{Helper::BaseUrl('/')}}";
  setTimeout(function(){window.location= url+'/main'}, 5000); 
</script>
@yield('js')
</html>

