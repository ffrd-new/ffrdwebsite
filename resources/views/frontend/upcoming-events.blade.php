@extends('layouts.front')

@if ( Config::get('app.locale') == 'en') 
@section('title','Upcoming Events | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title',' أحداث | المكتب الإعلامي لحكومة الفجيرة')
@endif

@section('content')
<div class="eventslist">
	<div class="container-fluid">
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 class="textcapital">{{trans('common.fujairah_events')}}</h3>
				<hr>
			</div>

			@if ( Config::get('app.locale') == 'en')
			<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12 events padding-bottom2">
				<a href="#" class="taghighlight">
					<div class="boxed-layout zoomeffect">
						<img alt="#" src="{{url('/')}}" class="mediaimg">
						<div class="date">
							<h4>event_date</h4>
							<p>event_date</p>
						</div>
					</div>
					<div>
						
					</div>
				</a>
			</div>
			@elseif( Config::get('app.locale') == 'ar')
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-bottom2">

				@if(count($evntlis) > 0)
				<input type="hidden" name="cnt" id="cnt" value="<?php echo count($evntlis); ?>">
				@foreach($evntlis as $key => $ev)
				<div class="upcoming-events">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<a class="taghighlight">
							<div class="boxed-layout">
								<a href="{{url('/')}}/events/{{$ev->id}}/{{Helper::generateslug($ev->id)}}">
									<img alt="{{$ev->ar_title}}" src="{{asset('/')}}images/events/thumb/{{Helper::imageCheck($ev->image)}}" class="img-responsive">
								</a>
							</div>
						</a>
					</div>
					<div class="col-lg-5 col-md-5 hidden-sm col-xs-12">
						<div class="up_events_content">
							<a href="{{url('/')}}/events/{{$ev->id}}/{{Helper::generateslug($ev->id)}}">
								<h4>{{$ev->ar_title}}</h4>
							</a>
							<div class="event-item">
								<i class="fa fa-calendar fa-fw" aria-hidden="true"></i>
								<p>
									<input type="hidden"  name="dat" id="dat{{$key}}" value="<?php echo date('M j,Y G:i:s', strtotime($ev->event_date)); ?>">
									@if(isset($ev->event_date))
									<?php 
									$da = strtotime($ev->event_date);
									$dat = date('d-m-Y', $da);
									?>
									{{$dat}}
									@endif
								</p>
							</div>
							<div class="event-item">
								<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
								<p>
									@if(isset($ev->ar_location))
									{{$ev->ar_location}}
									@endif
								</p>
							</div>
							<div class="event-item">
								<p>
									@if(isset($ev->ar_description))
									{!! str_limit(strip_tags($ev->ar_description), $limit = 180, $end = '...') !!}

									@endif
									<a href="{{url('/')}}/events/{{$ev->id}}/{{Helper::generateslug($ev->id)}}" class="read_more">{{trans('common.read_more')}} &#8592;</a>
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
						<div class="upcomingevent-countdown">
							<p>العد التنازلي </p>
							<!-- <p class="countdown-label">أيام, ساعات , دقائق , ثواني </p> -->
							<p>ثواني, دقائق, ساعات, أيام</p>
							<div class="upcomingevent-counter">
								<p id="example{{$key}}" data-countdown="<?php echo date('m/d/Y H:i:s', strtotime($ev->event_date)); ?>">
									
								</p>
							</div>
						</div>
					</div>

					<div class="hidden-lg hidden-md hidden-xs col-sm-12">
						<div class="up_events_content">
							<h4>{{$ev->ar_title}}</h4>
							<div class="event-item">
								<i class="fa fa-calendar fa-fw" aria-hidden="true"></i>
								<p>
									<input type="hidden"  name="dat" id="dat{{$key}}" value="<?php echo date('M j,Y G:i:s', strtotime($ev->event_date)); ?>">
									@if(isset($ev->event_date))
									<?php 
									$da = strtotime($ev->event_date);
									$dat = date('d-m-Y', $da);
									?>
									{{$dat}}
									@endif
								</p>
							</div>
							<div class="event-item">
								<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
								<p>
									@if(isset($ev->ar_location))
									{{$ev->ar_location}}
									@endif
								</p>
							</div>
							<div class="event-item">
								<p>
									@if(isset($ev->ar_description))
									{!! str_limit(strip_tags($ev->ar_description), $limit = 180, $end = '...') !!}
									@endif
									<a href="{{url('/')}}/events/{{$ev->id}}/{{Helper::generateslug($ev->id)}}" class="read_more">{{trans('common.read_more')}} &#8592;</a>
								</p>
							</div>
						</div>
					</div>

				</div>
				@endforeach
				@endif
			</div>
			@endif
		</div>
	</div>
</div>
@endsection


@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://www.jqueryscript.net/demo/Super-Simple-Event-Calendar-Plugin-For-jQuery-dnCalendar/js/dncalendar.min.js"></script>
<script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.0.4/dist/jquery.countdown.min.js"></script>

<script type="text/javascript">
	$(document).on("change", '#catlist', function(event) { 
		var APP_URL = {!! json_encode(url('/')) !!}
		var categoryid=$(this).val();
		$.ajax({
			url: "{{url('/')}}/getcategorytitle/"+categoryid,
			dataType:'json',
			delay: 250,
			initSelection: true, 
			success:function(data){
				<?php if( Config::get('app.locale') == 'en' ) {  ?>
					window.location.href = APP_URL +'/'+data.id+'/event/'+convertToSlug(data.category_name);
					<?php }elseif( Config::get('app.locale') == 'ar' ) { ?>
						window.location.href = APP_URL +'/'+data.id+'/event/'+data.category_name;
						<?php } ?>                	 
					}
				});
	});


	function convertToSlug(Text)
	{
		return Text
		.toLowerCase()
		.replace(/[^\w ]+/g,'')
		.replace(/ +/g,'-')
		;
	}



	$(function(){
		$('[data-countdown]').each(function() {
			var $this = $(this), finalDate = $(this).data('countdown');
			$this.countdown(finalDate, function(event) {
				if(event.strftime('%D:%H:%M:%S') == '00:00:00:00'){
					$this.html('اليوم');   
				}else{
					$this.html(event.strftime('%D:%H:%M:%S'));
					// $this.html(event.strftime('%S:%M:%H:%D'));
					// $this.html('<span>'event.strftime('%D')'</span>' '<span>'event.strftime(' %H')'</span>' '<span>'event.strftime('%M')'</span>' '<span>'event.strftime('%D')'</span>' );
				}

			});
		});
	});


</script>
@endsection