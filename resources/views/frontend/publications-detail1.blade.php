@extends('layouts.front')

@section('content')

<?php 

if ( Config::get('app.locale') == 'en')
	{ 
		$seo = Helper::seo($news_detail->en_title,str_limit($news_detail->en_meta_desc,'150'),$news_detail->image,$news_detail->en_meta_title,$news_detail->en_meta_keywords,$type);
	}
	elseif(Config::get('app.locale') == 'ar'){
		$seo = Helper::seo($news_detail->ar_title,str_limit($news_detail->ar_meta_desc,'150'),$news_detail->image,$news_detail->ar_meta_title,$news_detail->ar_meta_keywords,$type);
	}

	$meta = filter_var($seo['meta'], FILTER_SANITIZE_STRING);
	$meta_title = filter_var($seo['meta_title'], FILTER_SANITIZE_STRING);
	$meta_keyword = filter_var($seo['meta_keyword'], FILTER_SANITIZE_STRING);

	?>
	@section('title',$seo['meta_title'])
	@section('keywords',$seo['meta_keyword'])
	@section('meta-title',$seo['meta_title'])
	@section('description',$meta)
	@section('image',$seo['image'])
	<div class="detailPage">
		<div class="container-fluid publication-detailPage">
			<div class="row">
				<div class="topcategory">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<ul class="breadcrumb">
							@if ( Config::get('app.locale') == 'en')
							<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
							<li><a href="{{Helper::BaseUrl('/publications')}}">News</a></li>
							<li><a href="#" class="active">{{str_limit($news_detail->en_title,30)}}</a></li>
							@elseif (Config::get('app.locale') == 'ar' )
							<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
							<li><a href="{{Helper::BaseUrl('/publications')}}">فعاليات </a></li>
							<li><a href="#" class="active">{{str_limit($news_detail->ar_title,30)}}</a></li>
							@endif
						</ul>
					</div>
				</div>


				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news publicat nopadding">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news publication-content">
						@if ( Config::get('app.locale') == 'en')
						<div class="news-details">
							<h3 class="subheading">{{$news_detail->en_title}}</h3>
							<hr>
							<div class="shareicon"> 
								<div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
									<p>
										<!-- {{date('M j, Y', strtotime($news_detail->publish_on))}} -->
										@if($news_detail->publication_date)
										{{date('M j, Y', strtotime($news_detail->publication_date))}}
										@else
										{{ date('M j, Y', strtotime($news_detail->publish_on))}}
										@endif
										
									</p>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
									<p>
										<a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(url()->current())}}" target="_blank">
											<i class="mdi mdi-facebook-box mdi-24px"></i>
										</a>

										<a href="https://twitter.com/intent/tweet?url={{urlencode(url()->current())}}" target="_blank">
											<i class="mdi mdi-twitter-box mdi-24px"></i>
										</a>
									</p>
								</div>
							</div>
							<!--<img alt="{{$news_detail->en_title}}"src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($news_detail->image)}}" >-->
							<div class="news-detail-content">
								<p>
									{!!$news_detail->en_description!!}
								</p>	

								<div class="publicdetail-btns">
									<span class="downs col-lg-2">
										@php
										$xs = json_decode($news_detail->pdf);
										@endphp
										@if($news_detail->pubcurl != null)
										<a class="btn btn-default" href="{{$news_detail->pubcurl}}" class="btn btn-info">
										Downloads</a>
										@else
										<a class="btn btn-default" href="{{url('/')}}/pdfdownload/{{$news_detail->id}}" >
										Download</a>
										@endif
									</span>

									@if(isset($news_detail->flipname))
									<span class="downs "> 
										<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">View Book</button>
									</span>
									@endif
								</div>
							</div>
						</div>
						@elseif ( Config::get('app.locale') == 'ar')
						<div class="news-details">
							<h3 class="subheading">{{$news_detail->ar_title}}</h3>
							<div class="shareicon text-center"> 
								<!-- <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"> -->
									<p>   
										@if($news_detail->publication_date)
										{{ Helper::ArabicDateMnth(date('M j, Y', strtotime($news_detail->publication_date)))}}
										@else
										{{ Helper::ArabicDateMnth(date('M j, Y', strtotime($news_detail->publish_on)))}}
										@endif
									</p>
								<!-- </div> -->
							<!-- 	<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
									<p>
										<a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(url()->current())}}" target="_blank">
											<i class="mdi mdi-facebook-box mdi-24px"></i>
										</a>
										<a href="https://twitter.com/intent/tweet?url={{urlencode(url()->current())}}" target="_blank">
											<i class="mdi mdi-twitter-box mdi-24px"></i>
										</a>
									</p>
								</div> -->
							</div>

							<div class="publicdetail-btns">
								<span class="downs">
									@php
									$xs = json_decode($news_detail->pdf);
									@endphp
									@if($news_detail->pubcurl != null)
									<a class="btn btn-default" href="{{$news_detail->pubcurl}}" class="btn btn-info"> التنزيلات
									</a>
									@else
									<a class="btn btn-default" href="{{url('/')}}/pdfdownload/{{$news_detail->id}}" class="btn btn-info"> التنزيلات
									</a>
									@endif
								</span>
							</div>

							<div class="news-detail-content">
								<p>
									{!!$news_detail->ar_description!!}
								</p>	
							</div>

							<div class="publicdetail-btns">
								@if(isset($news_detail->flipname))
								<span class="downs"> 
									<button type="button" class="btn btn-default downloadbtn" data-toggle="modal" data-target="#myModal"> عرض كتاب       </button>
								</span>
								@endif
							</div>
						</div>
						@endif
					</div>
				<!-- 	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 relatednews">
						@if($recentnews)
						<h3>{{trans('common.highlight_news')}}</h3>
						<hr>
						@foreach($recentnews as $recentzz)
						@if( Config::get('app.locale') == 'en')
						<div class="list">
							<a href="{{url('/')}}/{{$type}}/{{$recentzz->id}}/{{Helper::generateslug($recentzz->id)}}" class="taghighlight">
								<div class="content">	
									<img alt="{{$recentzz->en_title}}" src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($recentzz->image)}}" class="mediaimg pub">
								</div>
								<div class="content">
									<p>{{str_limit($recentzz->en_title,30)}}</p>
									<small>Posted on {{date('M j, Y', strtotime($recentzz->publish_on))}}</small>
								</div>
							</a>
						</div>
						@elseif( Config::get('app.locale') == 'ar')
						<div class="list">
							<a href="{{url('/')}}/{{$type}}/{{$recentzz->id}}/{{Helper::generateslug($recentzz->id)}}" class="taghighlight">
								<div class="content">	
									<img alt="{{$recentzz->ar_title}}" src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($recentzz->image)}}" class="mediaimg pub">
								</div>
								<div class="content">
									<p>{{str_limit($recentzz->ar_title,30)}}</p>
									<small> نشر على  {{ Helper::ArabicDate(date('M j, Y', strtotime($recentzz->publish_on)))}}</small>
								</div>
							</a>
						</div>
						@endif
						@endforeach
						@endif
					</div> -->
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog" >
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<div style="padding-bottom:10px">
						@if( Config::get('app.locale') == 'en')
						<button id="prev" class="btn">Previous</button>
						<button id="next" class="btn">Next</button>
						@elseif( Config::get('app.locale') == 'ar')
						<button id="prev" class="btn">  سابق   </button>
						<button id="next" class="btn">  التالى  </button>
						@endif



						&nbsp; &nbsp;
						<br/>

					</div>
					<div class="msg loader-section"></div>
					<canvas id="the-canvas" style="width: 100%;"></canvas>
					<div class="page-view">
						<span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						@if ( Config::get('app.locale') == 'en')
						Close
						@elseif ( Config::get('app.locale') == 'ar')
						أغلق
						@endif
					</button>
				</div>
			</div>

		</div>
	</div>


	@endsection
	@section('js')
	<script type="text/javascript">

		var APP_URL = {!! json_encode(url('/')) !!}

	// If absolute URL from the remote server is provided, configure the CORS
// header on that server.

var filename = '{{$news_detail->flipname}}';
var url = APP_URL + '/public/images/pdf-files/'+filename;
var urls = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/web/compressed.tracemonkey-pldi-09.pdf';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];
$(".msg").show();
var baseurl = '{{url("/images")}}/';
// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

var pdfDoc = null,
pageNum = 1,
pageRendering = false,
pageNumPending = null,
scale = 0.8,
canvas = document.getElementById('the-canvas'),
ctx = canvas.getContext('2d');

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
 function renderPage(num) {
 	$(".msg").show();
 	$(".msg").html('<img class="img-responsive succ center-block publication-loader" src='+baseurl+'loading.gif>'); 
 	pageRendering = true;
  // Using promise to fetch the page
  pdfDoc.getPage(num).then(function(page) {
  	var viewport = page.getViewport({scale: scale});
  	canvas.height = viewport.height;
  	canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
    	canvasContext: ctx,
    	viewport: viewport
    };
    var renderTask = page.render(renderContext);
    // Wait for rendering to finish
    renderTask.promise.then(function() {
    	pageRendering = false;
    	$(".msg").show();
    	if (pageNumPending !== null) {
    		$('.img').remove();
        // New page rendering is pending
        renderPage(pageNumPending);
        pageNumPending = null;

    }else{
    	$(".msg").hide();
    	$('.img').remove();
    }
});
});

  // Update page counters
  document.getElementById('page_num').textContent = num;
}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
 function queueRenderPage(num) {
 	if (pageRendering) {
 		pageNumPending = num;
 		$('.img').remove();
 	} else {
 		renderPage(num);
 		$(".msg").show();
 		$(".msg").html('<img class="img-responsive succ center-block publication-loader" src='+baseurl+'loading.gif>'); 
 	}
 }

/**
 * Displays previous page.
 */
 function onPrevPage() {
 	$('.img').remove();
 	if (pageNum <= 1) {
 		return;
 	}
 	pageNum--;
 	queueRenderPage(pageNum);
 }
 document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
 function onNextPage() {
 	$('.img').remove();
 	if (pageNum >= pdfDoc.numPages) {
 		return;
 	}
 	pageNum++;
 	queueRenderPage(pageNum);
 }
 document.getElementById('next').addEventListener('click', onNextPage);

/**
 * Asynchronously downloads PDF.
 */

 myPromise = pdfjsLib.getDocument(url).promise;
 pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
 	pdfDoc = pdfDoc_;
 	document.getElementById('page_count').textContent = pdfDoc.numPages;

  // Initial/first page rendering
  renderPage(pageNum);
});





</script>
@endsection