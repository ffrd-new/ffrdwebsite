@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','Permits | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','طلب تصريح تصوير داخل الإمارة  | المكتب الإعلامي لحكومة الفجيرة')
@endif

@section('content')
<div class="services-page">
	<div class="services-section1" id="permits">
		<div class="container-fluid">
			<div class="row topcategory">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if ( Config::get('app.locale') == 'en') 
					<ul class="breadcrumb">
						<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
						<li><a href="{{Helper::BaseUrl('/permission')}}" class="active">Permission</a></li>	
					</ul>
					@elseif ( Config::get('app.locale') == 'ar') 
					<ul class="breadcrumb">
						<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
						<li><a href="{{Helper::BaseUrl('/permission')}}" class="active">طلب تصريح تصوير داخل الإمارة</a></li>	
					</ul>
					@endif
				</div>
			</div>
			<div class="row flip-section">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 service_content">
					@if ( Config::get('app.locale') == 'en') 
					<h3 class="subheading">Permission</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					
					@elseif ( Config::get('app.locale') == 'ar')
					<h3 class="subheading">طلب تصريح تصوير داخل الإمارة </h3>
					<p>يمكنكم الحصول على تصاريح مؤقته للتصوير الجوي والأرضي داخل إمارة الفجيرة، وذلك وفق الشروط والأحكام التالية: </p>
					<ul>
						<li>يجب على طالب التصريح، التقدم بطلبه، شخصياً أو عبر الايميل، قبل 5 أيام عمل من تاريخ بدء التصوير. </li>
						<li>توجيه رسالة رسمية (من جهة التصوير) تتضمن تفاصيل عن خطة التصوير وغاياته ومدته، وأسماء العاملين فيه.</li>
						<li>ارفاق الرسالة بالتراخيص التجارية والقانونية للجهة التي تطلب التصوير </li>
						<li>تعبئة استمارة معلومات (يمكن الحصول عليها من المكتب الإعلامي).. حول جميع أفراد طاقم العمل، مرفقة بكافة الوثائق الثبوتية الشخصية والمهنية وتأشيرات الزيارة والإقامة. </li>
						<li>تعبئة تعهد بالالتزام بأخلاقيات العمل الإعلامي وعدم التصوير بما يخالف القوانين والأنظمة المتبعة في الدولة أو يسيء لها. </li>
						<li>تعبئة تعهد بالالتزام بعدم استخدام المادة الفيلمية والصور لغايات غير تلك المذكورة في رسالة طلب التصوير الرسمية. </li>

					</ul>
					@endif

					<form action="{{url('/requestphoto')}}" enctype="multipart/form-data" method="post" name="requestform" class="requestform">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<input type="text" name="cname" class="form-control" placeholder="{{trans('common.name')}}*" value="{{ old('cname') }}" required="">
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<input type="email" name="cemail" class="form-control" placeholder="{{trans('common.email')}}*" value="{{ old('cemail') }}" required="">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<input type="tel" name="cphone" class="form-control" placeholder="{{trans('common.mobile')}}*" value="{{ old('cphone') }}"  required="">
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<textarea class="form-control" name="message" placeholder="{{trans('common.message')}}" value="{{ old('message') }}" rows="6"></textarea>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<button type="submit" name="submit" class="submit-btn btn">{{trans('common.submit')}}</button>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								@include ('layouts.error')
							</div>
						</div>		
					</form>
				</div>	
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					<div class="services_listsection">
						<h3>{{trans('common.services')}}</h3>
						<ul>
							<li class="active"><a href="{{Helper::BaseUrl('/permission')}}">{{trans('common.services_permits')}}</a></li>
							<li><a href="{{Helper::BaseUrl('/request-for-photo-or-video')}}">{{trans('common.services_request')}}</a></li>
							<li><a href="{{Helper::BaseUrl('/training')}}">{{trans('common.services_training')}}</a></li>
							<li><a href="{{Helper::BaseUrl('/library')}}">{{trans('common.services_library')}}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
@endsection
@section('js')
<script>

</script>
<script type="text/javascript">

</script>
@endsection
