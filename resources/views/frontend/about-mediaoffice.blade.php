@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','About Us | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','من نحن | المكتب الإعلامي لحكومة الفجيرة ')
@endif

@section('content')
<div class="about-page">
	<div class="container-fluid">
		<div class="row topcategory">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@if ( Config::get('app.locale') == 'en') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
					<li><a href="{{Helper::BaseUrl('/about-media-office')}}" class="active">About Media Office</a></li>	
				</ul>
				@elseif ( Config::get('app.locale') == 'ar') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
					<li><a href="{{Helper::BaseUrl('/about-media-office')}}" class="active">عن المكتب الإعلامي </a></li>	
				</ul>
				@endif
			</div>
		</div>


		<div class="row flip-section">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 service_content">
				<div class="heading-section">
					@if ( Config::get('app.locale') == 'en') 
					<h3 class="textcapital">About Media Office</h3>
					@elseif ( Config::get('app.locale') == 'ar')
					<h3 class="textcapital">عن المكتب الإعلامي </h3>
					@endif
				</div>

				@if ( Config::get('app.locale') == 'en') 
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				@elseif ( Config::get('app.locale') == 'ar')
				<p>تأسس المكتب الإعلامي لحكومة الفجيرة بموجب المرسوم الأميري رقم 10 لسنة 2014 الصادر عن صاحب لسمو الشيخ حمد بن محمد الشرقي عضو المجلس الأعلى حاكم الفجيرة، بتاريخ ٢١ يوليو ٢٠١٤ </p>

				<p>ويهدف المكتب إلى تزويد وسائل الإعلام المختلفة بالأخبار المتعلقة بالحاكم وحكومة الفجيرة والتغطية الإعلامية لأنشطتهما المختلفة، بالإضافة إلى الإعداد والتنظيم للمؤتمرات الصحفية المتعلقة بالحاكم والحكومة والتنسيق مع مندوبي وسائل الإعلام المختلفة لحضور تلك المؤتمرات، وترتيب الحوارات واللقاءات الإعلامية التي يجريها الحاكم وولي العهد ومسؤولي الحكومة مع مختلف وسائل الإعلام ومتابعة إذاعتها ونشرها ورصد تأثيراتها وصداها لدى الرأي العام. </p>

				<p>ويعد المكتب الإعلامي لحكومة الفجيرة المظلة التي تجمع المكاتب والمراكز الإعلامية لدوائر الحكومة، لتضمن أعلى مستويات التنسيق البيني وتدقيق المعلومات وتوحيد رسائلها الإعلامية. </p>

				<p>ويقوم المكتب برصد وتحليل ما تنشره وسائل الإعلام واتجاهات الرأي العام فيما يتعلق بآخر المستجدات العالمية والعربية والوطنية وأخبار إمارة الفجيرة وكل ما يجري فيها، إضافة إلى رصد الأخبار والتحليلات المتعلقة بأنشطة الحكومة ومسؤوليها، وإعداد التقارير التي تلخّص مجمل أعمال الرصد والتحليل بأسلوب علمي ومنهجي دقيق. </p>

				<p>ويتولى المكتب الإعلامي لحكومة الفجيرة حفظ وتوثيق وأرشفة الحوارات والمقابلات الصحفية والتلفزيونية والإذاعية للحاكم ولولي العهد ومسؤولي الحكومة، بالإضافة إلى إصدار المطبوعات الدورية والمناسباتية.  </p>

				<p>وينظم المكتب الإعلامي ورشات العمل والإعداد والتدريب الإعلامي ذات الاختصاص ويقوم بإدارة المشاريع والفعاليات الإعلامية في المناسبات المختلفة، ويتبع له (فريق الإعلام والاتصال الحكومي)، وهو الأول من نوعه في الدولة، كجسم إعلامي حكومي، يتألف من منسقي الإعلام في الدوائر المؤسسات والهيئات الحكومية. </p>
				@endif
			</div>	
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<div class="services_listsection">
					<h3 class="textcapital">{{trans('common.about_us')}}</h3>
					<ul>
						<li class="active"><a href="{{Helper::BaseUrl('/about-mediaoffice')}}">{{trans('common.home_media')}}</a></li>
						<li><a href="{{Helper::BaseUrl('/about-vision')}}">{{trans('common.home_vision')}}</a></li>
						<li><a href="{{Helper::BaseUrl('/about-message')}}">{{trans('common.home_message')}}</a></li>
						<li><a href="{{Helper::BaseUrl('/about-gov-communication-team')}}">{{trans('common.home_team')}}</a></li>
					</ul>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection
@section('js')
<script>

</script>
<script type="text/javascript">

	$(".nav-tabs a").click(function(){
		// $(this).tab('show');
		$tabid = $(this).attr("href")+'section';
		$('.nav-tabs li').removeClass('active');
		$(this).parent().addClass('active');
		$('.tab-content .tab-pane').removeClass('active in');
		$($tabid).addClass('active in');

		// $('a[href="#passive_order_categories"]').tab('show');
	});
	// $('.nav-tabs a').on('shown.bs.tab', function(event){
	// 	var x = $(event.target).text();         
	// 	var y = $(event.relatedTarget).text();  
	// 	$(".act span").text(x);
	// 	$(".prev span").text(y);
	// });
</script>
@endsection
