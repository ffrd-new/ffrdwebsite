@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','About Us | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','من نحن | المكتب الإعلامي لحكومة الفجيرة ')
@endif

@section('content')
<div class="about-page">
	<div class="container-fluid">
		<div class="row topcategory">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@if ( Config::get('app.locale') == 'en') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
					<li><a href="{{Helper::BaseUrl('/about-gov-communication-team')}}" class="active">Gov Communication Team</a></li>	
				</ul>
				@elseif ( Config::get('app.locale') == 'ar') 
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
					<li><a href="{{Helper::BaseUrl('/about-gov-communication-team')}}" class="active">مكتبة الديوان الأميري </a></li>	
				</ul>
				@endif
			</div>
		</div>

	<div class="row flip-section">
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 service_content">
			<div class="heading-section">
				@if ( Config::get('app.locale') == 'en') 
				<h3 class="textcapital">Team</h3>
				@elseif ( Config::get('app.locale') == 'ar')
				<h3 class="textcapital">مكتبة الديوان الأميري</h3>
				@endif
			</div>

			@if ( Config::get('app.locale') == 'en') 
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<ul>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed </li>
							</ul>
							@elseif ( Config::get('app.locale') == 'ar')
							<!-- <p>تطبيقاً لما تتضمنه المرسوم الأميري رقم 10 لعام 2014 الصادر عن صاحب السمو الشيخ حمد بن محمد الشرقي عضو المجلس الأعلى- حاكم الفجيرة، الخاص بإنشاء المكتب الإعلامي لحكومة الفجيرة، تم تشكيل (فريق الإعلام والاتصال الحكومي)، وهو الأول من نوعه في الدولة، كجسم إعلامي يتبع للمكتب الإعلامي ويتألف من منسقي الإعلام في الدوائر المؤسسات والهيئات الحكومية، ويهدف إلى: </p>
							<ul>
								<li>الارتقاء بأداء المكاتب الإعلامية في الدوائر المؤسسات والهيئات الحكومية </li>
								<li>توحيد الجهود الإعلامية وإجراء تشبيك بين المؤسسات الحكومية، وتبادل الخبرات والإمكانيات والدعم المتبادل</li>
								<li>توحيد الرسائل الإعلامية وتوجيهها بما يخدم إستراتيجية العمل الإعلامي العام للحكومة. </li>
							</ul> -->

					<p>تشتمل مكتبة الديوان الأميري على 10 آلاف عنوان تقريباً تضم كتباً ومجلدات ومراجع ومعاجم ذات موضوعات مختلفة في شتى نواحي المعرفة، مثل العلوم الاجتماعية والفلسفة والديانات والمعارف العامة والتاريخ والجغرافية والعلوم السياسية والقانون والفنون الجميلة واللغة والادب والعلوم والزراعة والتكنولوجيا، فضلاً عن مجموعة كبيرة من الكتب الخاصة بدولة الامارات العربية المتحدة.</p>

					<p>تقوم المكتبة بتقديم خدمة الاعارة فقط لموظفين الديوان الاميري وباقي الدوائر والأقسام التابعة له، أما الاستعارة الخارجية فتكون بناء على موافقة مسبقة من المكتب الإعلامي، وتقوم المكتبة باستقبال رواد وزوار من مختلف الاعمار بغرض الدراسة والمطالعة والبحث موفرة أركان خاصة لهذا الغرض مزودة بكومبيوترات وانترنت وذلك يوميا من الساعة التاسعة صباحاً و حتى الثانية ظهراً.</p>

					<p>وتتبع المكتبة نظام مكتبة الكونجرس الامريكي في تقسيم الموضوعات وتنظيمها وفهرستها الذي يعتمد في تنظيم وترتيب وتقسيم موضوعات المعرفة المختلفة وفق الاحرف الهجائية الإنجليزية.</p>
							@endif
		</div>	
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<div class="services_listsection">
				<h3 class="textcapital">{{trans('common.about_us')}}</h3>
				<ul>
					<li><a href="{{Helper::BaseUrl('/about-mediaoffice')}}">{{trans('common.home_media')}}</a></li>
					<li><a href="{{Helper::BaseUrl('/about-vision')}}">{{trans('common.home_vision')}}</a></li>
					<li><a href="{{Helper::BaseUrl('/about-message')}}">{{trans('common.home_message')}}</a></li>
					<li class="active"><a href="{{Helper::BaseUrl('/about-gov-communication-team')}}">{{trans('common.home_team')}}</a></li>
				</ul>
			</div>
		</div>
	</div>

</div>
</div>
@endsection
@section('js')
<script type="text/javascript">

</script>
@endsection
