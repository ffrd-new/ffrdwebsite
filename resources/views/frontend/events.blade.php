@extends('layouts.front')

@if ( Config::get('app.locale') == 'en') 
@section('title','Events | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title',' أحداث | المكتب الإعلامي لحكومة الفجيرة ')
@endif

@section('content')
<div class="eventslist">
	<div class="container-fluid">
		<div class="row topcategory">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="form-group">
					<select name="catlist" id="catlist" class="form-control category">
						@if ( Config::get('app.locale') == 'en')
						<option value=""> Select Category </option>
						@elseif ( Config::get('app.locale') == 'ar')
						<option value="">  الاقسام   </option>
						@endif

						@foreach ($listdrop as $list)
						@if ( Config::get('app.locale') == 'en')
						<option value="{{$list->id}}" @if($list->id == Request::segment(1) ) selected @endif >{{$list->en_name}} - ({{$list->count}})</option>
						@elseif ( Config::get('app.locale') == 'ar')
						<option value="{{$list->id}}" @if($list->id == Request::segment(1) ) selected @endif >{{$list->ar_name}} - ({{$list->count}})</option>
						@endif
						@endforeach 
					</select>
				</div>
			</div>
			<div class="hiden-xs hidden-sm col-md-4 col-lg-4"></div>
			<div class="col-xs-12 col-sm-8 col-md-4 col-lg-4">
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}" class="textcapital">{{trans('common.home')}}</a></li>
					<li><a href="{{Helper::BaseUrl('/events')}}" class="active textcapital">{{trans('common.events')}}</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<!-- <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9"> -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="textcapital">{{trans('common.events')}}</h3>
					<hr>
				</div>
				@if(count($events) >0)
				@foreach($events as $newzzz)
				@if ( Config::get('app.locale') == 'en')
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 events padding-bottom2">
					<a href="{{url('/')}}/events/{{$newzzz->id}}/{{Helper::generateslug($newzzz->id)}}" class="taghighlight">
						<div class="boxed-layout zoomeffect">
							<img alt="{{$newzzz->en_title}}"src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($newzzz->image)}}" class="mediaimg">


							<div class="date">
								<h4>{{date('d', strtotime($newzzz->event_date))}}</h4>
								<p>{{date('M', strtotime($newzzz->event_date))}}</p>
							</div>
						</div>
						<div><p title="{{$newzzz->ar_title}}">{{str_limit($newzzz->en_title,35)}}</p></div>
					</a>
				</div>
				@elseif( Config::get('app.locale') == 'ar')
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 events padding-bottom2">
					<a href="{{url('/')}}/events/{{$newzzz->id}}/{{Helper::generateslug($newzzz->id)}}" class="taghighlight">
						<div class="boxed-layout zoomeffect">
							<img alt="{{$newzzz->ar_title}}"src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($newzzz->image)}}" class="mediaimg">
							<div class="date">
								<h4>{{date('d', strtotime($newzzz->event_date))}}</h4>
								<p>{{ Helper::ArabicMonth(date('M', strtotime($newzzz->event_date)))}}</p>
							</div>
						</div>
						<div><p title="{{$newzzz->ar_title}}">{{str_limit($newzzz->ar_title,35)}}</p></div>
						
					</a>
				</div>
				@endif
				@endforeach
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pagination-section">
					{{ $events->links() }}
				</div> 
				@else
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="event-inner-content">
						@if ( Config::get('app.locale') == 'en')
						<h4>No Records Found</h4>
						@elseif ( Config::get('app.locale') == 'ar')
						<h4> لا توجد سجلات
						</h4>
						@endif
					</div>
				</div>
				@endif	
			</div>
		</div>
	</div>
	@endsection


	@section('js')
	<script type="text/javascript">
		$(document).on("change", '#catlist', function(event) { 
			var APP_URL = {!! json_encode(url('/')) !!}
			var categoryid=$(this).val();
			$.ajax({
				url: "{{url('/')}}/getcategorytitle/"+categoryid,
				dataType:'json',
				delay: 250,
				initSelection: true, 
				success:function(data){
					<?php if( Config::get('app.locale') == 'en' ) {  ?>
						window.location.href = APP_URL +'/'+data.id+'/event/'+convertToSlug(data.category_name);
						<?php }elseif( Config::get('app.locale') == 'ar' ) { ?>
							window.location.href = APP_URL +'/'+data.id+'/event/'+data.category_name;
							<?php } ?>                	 
						}
					});
		});


		function convertToSlug(Text)
		{
			return Text
			.toLowerCase()
			.replace(/[^\w ]+/g,'')
			.replace(/ +/g,'-')
			;
		}
	</script>
	@endsection