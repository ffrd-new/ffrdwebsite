@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','Request for Photo or Video | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title',' طلب للصور أو الفيديو | المكتب الإعلامي لحكومة الفجيرة')
@endif

@section('content')
<div class="services-page">

	<div class="services-section1" id="request-photovideo">
		<div class="container-fluid">
			<div class="row topcategory">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if ( Config::get('app.locale') == 'en') 
					<ul class="breadcrumb">
						<li><a href="{{Helper::BaseUrl('/main')}}">Home</a></li>
						<li><a href="{{Helper::BaseUrl('/request-for-photo-or-video')}}" class="active">Request for Photo or Video</a></li>	
					</ul>
					@elseif ( Config::get('app.locale') == 'ar') 
					<ul class="breadcrumb">
						<li><a href="{{Helper::BaseUrl('/main')}}">الصفحة الرئيسية </a></li>
						<li><a href="{{Helper::BaseUrl('/request-for-photo-or-video')}}" class="active">طلب صور أو مواد فيلمية</a></li>	
					</ul>
					@endif
				</div>
			</div>
			<div class="row flip-section">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 service_content">
					@if ( Config::get('app.locale') == 'en') 
					<h3>Request for Photo or Video</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<a href="mailto:info@media.fujairah.ae" class="requestmail-btn"> Add email for requesting permission</a>
					@elseif ( Config::get('app.locale') == 'ar')	
					<h3>طلب صور أو مواد فيلمية</h3>
					<p>يمكنكم الحصول على صور ومواد فيلمية للأحداث والفعاليات التي تجري في إمارة الفجيرة ويحضرها صاحب السمو حاكم الفجيرة والشيوخ، وذلك وفق الشروط والأحكام التالية: </p>
					<ul>
						<li>توجيه رسالة رسمية تتضمن معلومات عن الجهة التي تطلب المواد الفيلمية والصور، والمكان والهدف استخدامها، أو تعبئة استمارة معلومات للأفراد (يمكن الحصول عليها من المكتب الإعلامي). </li>
						<li>التعهد بعدم القيام بأي تغيير في الصور أو المادة الفيلمية أو التصرف بها لغير الأهداف المصرح بها في الطلب الرسمي</li>
						<li>التعهد بعدم استخدام الصور او المادة الفيلمية في أي مادة إعلامية تسيء لدولة الإمارات العربية المتحدة بشكل عام وللفجيرة بشكل خاص. </li>
					</ul>
					<p class="requestmail-btn">للحصول على أي خدمة من خدمات المكتب الإعلامي يرجى التواصل معنا عن طريق البريد الإلكتروني <a href="mailto:info@media.fujairah.ae"> info@media.fujairah.ae </a>
					</p>
					@endif	

				</div>	

				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					<div class="services_listsection">
						<h3>{{trans('common.services')}}</h3>
						<ul>
							<li><a href="{{Helper::BaseUrl('/permission')}}">{{trans('common.services_permits')}}</a></li>
							<li class="active"><a href="{{Helper::BaseUrl('/request-for-photo-or-video')}}">{{trans('common.services_request')}}</a></li>
							<li><a href="{{Helper::BaseUrl('/training')}}">{{trans('common.services_training')}}</a></li>
							<li><a href="{{Helper::BaseUrl('/library')}}">{{trans('common.services_library')}}</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>
@endsection
@section('js')
<script type="text/javascript">

</script>
@endsection
