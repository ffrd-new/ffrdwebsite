@extends('layouts.front')

@section('css')
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-demo.css" />
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/3dslider-style.css" />
@endsection

@if ( Config::get('app.locale') == 'en') 
@section('title','Privacy Policy | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','سياسة الأمن والخصوصية | المكتب الإعلامي لحكومة الفجيرة')
@endif

@section('content')
<div class="privacy-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@if ( Config::get('app.locale') == 'en') 
				<h3>Sitemap</h3>
					<ul class="sitemap-list">
					<li ><a class="" href="{{Helper::BaseUrl('/main')}}">{{trans('common.home')}}</a></li>
					<li class="nav-item">

						<a class="nav-link dropdown-toggle" href="#" id="navbardrop" >
							{{trans('common.about')}}
						</a>
						<ul>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/about-us#about-media-office') }}">{{trans('common.about_media')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/about-us#vision') }}">{{trans('common.about_vision')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/about-us#message') }}">{{trans('common.about_message')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/about-us#media-team-government-communication') }}">{{trans('common.about_team')}}</a></li>
						</ul>

					</li>
						
					</li>
					<li><a href="{{Helper::BaseUrl('/news')}}" class="">{{trans('common.news')}}</a></li>
					<li><a class="" href=" {{Helper::BaseUrl('/events')}}">{{trans('common.events')}}</a></li>
					<li class="nav-item">
						<!-- data-toggle="dropdown" -->
						<a class="nav-link dropdown-toggle" href="#" id="navbardrop">
							{{trans('common.services')}}
						</a>
						<ul>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/permission') }}">{{trans('common.services_permits')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/training') }}">{{trans('common.services_training')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/request-for-photo-or-video') }}">{{trans('common.services_request')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/library') }}">{{trans('common.services_library')}}</a></li>
						</ul>

					</li>
						

					<li><a href="{{Helper::BaseUrl('/publications')}}" class="">{{trans('common.publications')}}</a></li>
					<li><a href="{{ Helper::BaseUrl('/gallery') }} " class="">{{trans('common.gallery')}}</a></li>

					<li><a href=" {{Helper::BaseUrl('/contact')}} " class="">{{trans('common.contact')}}</a></li>
				</ul>

				@elseif ( Config::get('app.locale') == 'ar')
				<h3>خريطة الموقع</h3>
				<ul class="sitemap-list">
					<li ><a class="" href="{{Helper::BaseUrl('/main')}}">{{trans('common.home')}}</a></li>
					<li class="nav-item">

						<a class="nav-link dropdown-toggle" href="#" id="navbardrop" >
							{{trans('common.about')}}
						</a>
						<ul>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/about-us#about-media-office') }}">{{trans('common.about_media')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/about-us#vision') }}">{{trans('common.about_vision')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/about-us#message') }}">{{trans('common.about_message')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/about-us#media-team-government-communication') }}">{{trans('common.about_team')}}</a></li>
						</ul>

					</li>
						
					</li>
					<li><a href="{{Helper::BaseUrl('/news')}}" class="">{{trans('common.news')}}</a></li>
					<li><a class="" href=" {{Helper::BaseUrl('/events')}}">{{trans('common.events')}}</a></li>
					<li class="nav-item">
						<!-- data-toggle="dropdown" -->
						<a class="nav-link dropdown-toggle" href="#" id="navbardrop">
							{{trans('common.services')}}
						</a>
						<ul>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/permission') }}">{{trans('common.services_permits')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/training') }}">{{trans('common.services_training')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/request-for-photo-or-video') }}">{{trans('common.services_request')}}</a></li>
							<li><a class="dropdown-item" href="{{ Helper::BaseUrl('/library') }}">{{trans('common.services_library')}}</a></li>
						</ul>

					</li>
						

					<li><a href="{{Helper::BaseUrl('/publications')}}" class="">{{trans('common.publications')}}</a></li>
					<li><a href="{{ Helper::BaseUrl('/gallery') }} " class="">{{trans('common.gallery')}}</a></li>

					<li><a href=" {{Helper::BaseUrl('/contact')}} " class="">{{trans('common.contact')}}</a></li>
				</ul>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>

</script>
<script type="text/javascript">

</script>
@endsection
