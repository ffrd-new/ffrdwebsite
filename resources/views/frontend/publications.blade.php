@extends('layouts.front')

@if ( Config::get('app.locale') == 'en') 
@section('title','Publications | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','إصداراتنا | المكتب الإعلامي لحكومة الفجيرة')
@endif

@section('content')
<div class="newslist">
	<div class="container-fluid">
		<div class="row topcategory">
			<div class="hiden-xs hidden-sm col-md-4 col-lg-4"></div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}" class="textcapital">{{trans('common.home')}}</a></li>
					<li><a href="{{Helper::BaseUrl('/publications')}}" class="active textcapital">{{trans('common.publications')}}</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid news">
		<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="textcapital">{{trans('common.publications')}}</h3>
					<hr>
				</div>
				@if(count($publist) >0)
				@foreach($publist as $newzzz)
				@if ( Config::get('app.locale') == 'en')
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-bottom publicat nopadding">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
						<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" class="blist-img">
							<img src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($newzzz->image)}}" alt="{{$newzzz->en_title}}" class="img-responsive">
						</a>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 publication-content">
						<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" class="blist-img">
							<h3 title="{{$newzzz->en_title}}">{{Helper::text_cut(Helper::table_head_lower($newzzz->en_title),28)}}</h3>
							<p>{!!Helper::text_cut($newzzz->en_description,180)!!}</p>
					<!-- 		<p><small>Posted on {{date('M j, Y', strtotime($newzzz->publish_on))}}</small></p> -->
							<p>
								<small> @if($newzzz->publication_date)
								{{date('M j, Y', strtotime($newzzz->publication_date))}}
								@else
								{{ date('M j, Y', strtotime($newzzz->publish_on))}}
								@endif
								</small>
							</p>
						</a>
						<div class="nopadding publicadetail">
							<a class="btn btn-default more-infobtn" href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::generateslugforpub($newzzz->id)}}" role="button">More Info</a>
							@php

							$x = json_decode($newzzz->pdf);
							@endphp

							@if($newzzz->pubcurl != null)
							<a class="btn btn-default downloadbtn" href="{{$newzzz->pubcurl}}" class="btn btn-info">
								Downloads
							</a>
							@else
							<a class="btn btn-default downloadbtn" href="{{url('/')}}/download/{{$newzzz->id}}" class="btn btn-info">
								Downloads
							</a>
							@endif

						</div>
					</div>
					
				</div>
				@elseif ( Config::get('app.locale') == 'ar')
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-bottom publicat nopadding">
					
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
						<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::make_slug($newzzz->ar_title)}}" class="blist-img">
							<img src="{{asset('/')}}images/{{$type}}/thumb/{{Helper::imageCheck($newzzz->image)}}" alt="{{$newzzz->ar_title}}" class="img-responsive">
						</a>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 publication-content">
						<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::make_slug($newzzz->ar_title)}}" class="blist-img">
							<h3>{{$newzzz->ar_title}}</h3>
						</a>
							<p>{!!Helper::text_cut($newzzz->ar_description,160)!!} 
								<a href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::make_slug($newzzz->ar_title)}}" class="read_more">{{trans('common.read_more')}} &#8592;</a>
							</p>
							<!-- <p>{!!$newzzz->ar_description!!}</p> -->
							<p>
								<small>@if($newzzz->publication_date)
								{{ Helper::ArabicDate(date('M j, Y', strtotime($newzzz->publication_date)))}}
								@else
								{{ Helper::ArabicDate(date('M j, Y', strtotime($newzzz->publish_on)))}}
								@endif
								</small>
							</p>
						<!-- </a> -->
						<div class="nopadding publicadetail">
							<a class="btn btn-default more-infobtn" href="{{url('/')}}/{{$type}}/{{$newzzz->id}}/{{Helper::make_slug($newzzz->ar_title)}}" role="button"> مزيد من المعلومات    </a>

							@if($newzzz->pubcurl != null)
							<a class="btn btn-default downloadbtn" href="{{$newzzz->pubcurl}}" class="btn btn-info"> التنزيلات
							</a>
							@else
							<a class="btn btn-default downloadbtn" href="{{url('/')}}/download/{{$newzzz->id}}" class="btn btn-info"> التنزيلات
							</a>
							@endif
						</div>
					</div>
				</div>
				@endif
				@endforeach

				@else
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="event-inner-content">
						@if ( Config::get('app.locale') == 'en')
						<h4>No Records Found</h4>
						@elseif ( Config::get('app.locale') == 'ar')
						<h4> لا توجد سجلات
						</h4>
						@endif
					</div>
				</div>
				@endif   	
		</div>
		<div>
			{{ $publist->links() }}
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">

	$(document).on("change", '#catlist', function(event) { 

		var APP_URL = {!! json_encode(url('/')) !!}
		var categoryid=$(this).val();
		$.ajax({
			url: "{{url('/')}}/getcategorytitle/"+categoryid,
			dataType:'json',
			delay: 250,
			initSelection: true, 
			success:function(data){
				<?php if( Config::get('app.locale') == 'en' ) {  ?>
					window.location.href = APP_URL +'/'+data.id+'/publications/'+convertToSlug(data.category_name); 
					<?php }elseif( Config::get('app.locale') == 'ar' ) { ?>
						window.location.href = APP_URL +'/'+data.id+'/publications/'+data.category_name; 
						<?php } ?>
					}
				});
	});

	function convertToSlug(Text)
	{
		return Text
		.toLowerCase()
		.replace(/[^\w ]+/g,'')
		.replace(/ +/g,'-')
		;
	}
</script>
@endsection