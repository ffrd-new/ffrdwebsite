@extends('layouts.front')

@if ( Config::get('app.locale') == 'en') 
@section('title','Gallery | Fujairah Government Media Office')
@elseif ( Config::get('app.locale') == 'ar') 
@section('title','الوسائط | المكتب الإعلامي لحكومة الفجيرة ')
@endif

@section('content')


<div class="gallerylist">
	<div class="container-fluid">
		<div class="row topcategory flip-section">
			<div class="col-xs-12 col-sm-8 col-md-4 col-lg-4">
				<div class="form-group galleryselect">
					<a href="{{Helper::BaseUrl('/gallery#pictures')}}" class="active" id="pictures" data-attr="pictures">{{trans('common.image_gallery')}}</a>
					<a href="{{Helper::BaseUrl('/gallery#videos')}}" id="videos" data-attr="videos">{{trans('common.video_gallery')}}</a>
				</div>
			</div>
			<div class="hiden-xs hidden-sm col-md-4 col-lg-4"></div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<ul class="breadcrumb">
					<li><a href="{{Helper::BaseUrl('/main')}}">{{trans('common.home')}}</a></li>
					<li><a href="{{Helper::BaseUrl('/gallery')}}" class="active">{{trans('common.gallery')}}</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="imagegallery" class="">
		<div class="container-fluid">
			<div class="album-images">
				<div class="row">
					<div class="col-xs-10 col-sm-8 col-md-8 col-lg-8">
						<h4 class="imgtitle">Lorem ipsum dolor sit amet</h4>
					</div>
					<div class="col-xs-2 col-sm-4 col-md-4 col-lg-4">
						<i class="mdi mdi-close mdi-24px close-btn"></i>
					</div>
				</div>
				<div class="row masgallery-grid invisible-scrollbar">
				</div>
				<hr>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="subheading">{{trans('common.image_gallery')}}</h3>
				</div>
				<!-- <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<select name="catlist" id="catlist" class="form-control category">
						@if ( Config::get('app.locale') == 'en')
						<option value=""> Select Category </option>
						@elseif ( Config::get('app.locale') == 'ar')
						<option value="">  الاقسام   </option>
						@endif

						@foreach ($listdrop as $list)
						@if ( Config::get('app.locale') == 'en')
						<option value="{{$list->id}}" @if($list->id == Request::segment(1) ) selected @endif >{{$list->en_name}} - ({{$list->count}})</option>
						@elseif ( Config::get('app.locale') == 'ar')
						<option value="{{$list->id}}" @if($list->id == Request::segment(1) ) selected @endif >{{$list->ar_name}} - ({{$list->count}})</option>
						@endif
						@endforeach 
					</select>
				</div> -->
			</div>
			<hr>
		</div>

		<div class="container-fluid">
			<div class="albumlist">
				<div class="row">
					@if (count($imagegallery) > 0)
					@foreach($imagegallery as $g)
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						@if ( Config::get('app.locale') == 'en')
						<div class="content" data-gallery="{{$g->gallery}}" data-title="{{$g->en_title}}">
							@php
							$x = json_decode($g->gallery);
							@endphp
							<img src="{{url('/')}}/images/gallery/{{$x[0]}}" class="mediaimg zoomeffect">	
						</div>
						<div class="content-title">
							<p title="{{$g->en_title}}" class="title">{{str_limit($g->en_title,25)}}
								<!-- <span class="tooltiptext">{{$g->en_title}}</span> -->
							</p>
							<!-- <p>{{date('M j, Y', strtotime($g->publish_on))}}</p> -->
							<p>
								@if($g->image_date)
								{{date('M j, Y', strtotime($g->image_date))}}
								@else
								{{ date('M j, Y', strtotime($g->publish_on))}}
								@endif
							</p>
						</div>
						@elseif ( Config::get('app.locale') == 'ar')
						<div class="content" data-gallery="{{$g->gallery}}" data-title="{{$g->ar_title}}">
							@php
							$x = json_decode($g->gallery);
							@endphp
							<img src="{{url('/')}}/images/gallery/{{$x[0]}}" class="mediaimg zoomeffect">		
						</div>
						<div class="content-title">
							<p title="{{$g->ar_title}}" class="title">{{str_limit($g->ar_title,95)}}</p>
							



							<!-- <p>{{ Helper::ArabicDate(date('M j, Y', strtotime($g->publish_on)))}}</p> -->
							<p>
								@if($g->image_date)
								{{ Helper::ArabicDate(date('M j, Y', strtotime($g->image_date)))}}
								@else
								{{ Helper::ArabicDate(date('M j, Y', strtotime($g->publish_on)))}}
								@endif
							</p>
						</div>
						@endif
					</div>
					@endforeach
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pagination-section">
						{{ $imagegallery->fragment('pictures')->links() }}
						<!-- {{ $imagegallery->links() }} -->
					</div> 
					@else
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="event-inner-content">
							@if ( Config::get('app.locale') == 'en')
							<h4>No Records Found</h4>
							@elseif ( Config::get('app.locale') == 'ar')
							<h4> لا توجد سجلات
							</h4>
							@endif
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div id="videogallery" class="">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3>{{trans('common.video_gallery')}}</h3>
				</div>
				<!-- <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="form-group">
						<select name="catvideolist" id="catvideolist" class="form-control category">
							@if ( Config::get('app.locale') == 'en')
							<option value=""> Select Category </option>
							@elseif ( Config::get('app.locale') == 'ar')
							<option value="">  الاقسام   </option>
							@endif
							@foreach ($videolistdrop as $videolist)
							@if ( Config::get('app.locale') == 'en')
							<option value="{{$videolist->id}}" @if($videolist->id == Request::segment(2) ) selected @endif >{{$videolist->en_name}} - ({{$videolist->count}})</option>
							@elseif ( Config::get('app.locale') == 'ar')
							<option value="{{$videolist->id}}" @if($videolist->id == Request::segment(2) ) selected @endif >{{$videolist->ar_name}} - ({{$videolist->count}})</option>
							@endif
							@endforeach 
						</select>
					</div>
				</div>	 -->
			</div>
			<hr>
		</div>
		<div class="container-fluid">
			<div class="row">
				@if(count($vidoegallery) > 0)
				@foreach ($vidoegallery as $videogallist)
				<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
					<a class="bla-1" href="{{$videogallist->video_url}}">
						<div class="content">
							<img src="{{url('/')}}/images/videogallery/{{$videogallist->image}}" class="mediaimg zoomeffect">
							
						</div>
						@if ( Config::get('app.locale') == 'en')
						<p title="{{$videogallist->en_title}}" class="title">{{str_limit($videogallist->en_title,80)}}</p>
						<!-- <p>{{ date('M j, Y', strtotime($videogallist->publish_on))}}
						</p> -->
						<p>@if($videogallist->video_date)
							{{date('M j, Y', strtotime($videogallist->video_date))}}
							@else
							{{ date('M j, Y', strtotime($videogallist->publish_on))}}
							@endif
						</p>

						@elseif ( Config::get('app.locale') == 'ar')
						<p class="title" title="{{$videogallist->ar_title}}">{{str_limit($videogallist->ar_title,95)}}</p>

						<p>@if($videogallist->video_date)
							{{ Helper::ArabicDate(date('M j, Y', strtotime($videogallist->video_date)))}}
							@else
							{{ Helper::ArabicDate(date('M j, Y', strtotime($videogallist->publish_on)))}}
							@endif
						</p>

						<!-- <p>{{ Helper::ArabicDate(date('M j, Y', strtotime($videogallist->publish_on)))}}</p> -->
						@endif
						<img src="{{url('/')}}/images/video-icon.png" class="playbtn">
					</a>
				</div>
				@endforeach
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pagination-section">
					{{ $vidoegallery->fragment('videos')->links() }}
					<!-- {{ $vidoegallery->links() }} -->
				</div> 
				@else
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="event-inner-content">
						@if ( Config::get('app.locale') == 'en')
						<h4>No Records Found</h4>
						@elseif ( Config::get('app.locale') == 'ar')
						<h4> لا توجد سجلات
						</h4>
						@endif
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
	
</div>
@endsection
@section('js')
<script>

	$(document).ready(function(){

		$('[data-toggle="tooltip"]').tooltip();

		var type = window.location.hash.substr(1);
		var ser = window.location.search;
            /*if(window.location.search != ''){
               $('.galleryselect button:last-child').addClass('disable');
           }*/ 

           if(type == "video")
           {
           	$('#videogallery').show();
           	$('#imagegallery').hide();
           	$('.galleryselect button:last-child').addClass('active');
           	$('.galleryselect button:last-child').siblings().removeClass('active');
           }
           else if(type == "image"){
           	$('#videogallery').hide();
           	$('#imagegallery').show();
           	$('.galleryselect button:first-child').addClass('active');
           	$('.galleryselect button:first-child').siblings().removeClass('active');
           }
           else{
           	$('#videogallery').hide();
           	$('#imagegallery').show();
           }
           $('.album-images').hide();

           $('.galleryselect a:first-child').click(function(){
           	$('#imagegallery').show();
           	$('#videogallery').hide();
           	$(this).addClass('active');
           	$(this).siblings().removeClass('active');
           });

           $('.galleryselect a:last-child').click(function(){
           	$attr = $(this).data('attr');

           	$('#videogallery').show();
           	$('#imagegallery').hide();
           	$(this).addClass('active');
           	$(this).siblings().removeClass('active');
           });

           $('.albumlist').on('click', '.content', function(){
           	$('.album-images').slideDown("slow");
           	$('.albumlist').addClass("gallery-overlay");
           	$(this).addClass('active-album');
           	var imggal = $(this).data('gallery');
           	var imgtitle = $(this).data('title');  
           	$('h4.imgtitle').text(imgtitle);
           	var APP_URL = "{{(url('/'))}}";
           	var obje = imggal;
           	$('.row.masgallery-grid').html('');
           	$.each(obje, function(idx, obj) {
           		var imgs =  APP_URL +'/images/gallery/'+obj;
           		$( ".row.masgallery-grid" ).append( "<div class='col-xs-6 col-sm-4 col-md-3 col-lg-2 ptag'><a data-fancybox='gallery' href='"+imgs+"'><img src='"+imgs+"' class='mediaimg zoomeffect'></a></div>" );
           	});
           	$(this).parent('div').siblings('div').find('.active-album').removeClass('active-album');
           });
           $('.album-images').on('click', '.close-btn', function(){
           	$('.album-images').slideUp("slow");
           	$('.albumlist').removeClass("gallery-overlay");
           	$('.albumlist').removeClass('active-album');
           	$('.row.masgallery-grid').empty();
           });
       });
   </script>
   <script type="text/javascript">
   	$(function(){
   		$("a.bla-1").YouTubePopUp();
			$("a.bla-2").YouTubePopUp( { autoplay: 0 } ); // Disable autoplay
		});
	</script>

	<script type="text/javascript">


		$(document).on("change", '#catlist', function(event) { 
			var APP_URL = {!! json_encode(url('/')) !!}
			var categoryid=$(this).val();
			$.ajax({
				url: "{{url('/')}}/getcategorytitle/"+categoryid,
				dataType:'json',
				delay: 250,
				initSelection: true, 
				success:function(data){


					<?php if( Config::get('app.locale') == 'en' ) {  ?>

						window.location.href = APP_URL +'/'+data.id+'/gallery/'+convertToSlug(data.category_name)+'#image'; 
						<?php }elseif( Config::get('app.locale') == 'ar' ) { ?>
							window.location.href = APP_URL +'/'+data.id+'/gallery/'+data.category_name+'#image'; 
							<?php } ?>



						}
					});
		});

		$(document).on("change", '#catvideolist', function(event) { 
			var APP_URL = {!! json_encode(url('/')) !!}
			var categoryid=$(this).val();
			$.ajax({
				url: "{{url('/')}}/getcategorytitle/"+categoryid,
				dataType:'json',
				delay: 250,
				initSelection: true, 
				success:function(data){

					<?php if( Config::get('app.locale') == 'en' ) {  ?>
						window.location.href = APP_URL +'/gallery/'+data.id+'/'+convertToSlug(data.category_name)+'#video';  
						<?php }elseif( Config::get('app.locale') == 'ar' ) { ?>
							window.location.href = APP_URL +'/gallery/'+data.id+'/'+data.category_name+'#video'; 
							<?php } ?>

							$('.galleryselect button:last-child').addClass('test');
							$('.galleryselect button:last-child').siblings().removeClass('active');

						}
					});
		});


		function convertToSlug(Text)
		{
			return Text
			.toLowerCase()
			.replace(/[^\w ]+/g,'')
			.replace(/ +/g,'-')
			;
		}
	</script>
	@endsection