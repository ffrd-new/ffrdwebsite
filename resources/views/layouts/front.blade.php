<!DOCTYPE html>
<html lang="en" ng-app="mymodule">
<head>
  <title>@yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="Robots" CONTENT="NOINDEX, NOFOLLOW" />
  <meta name="description" content="@yield('description')">
  <meta name="keywords" content="@yield('keywords')">
  <meta name="meta-title" content="@yield('meta-title')">
  <!-- <link rel="canonical" href="Site url" /> -->
  <link rel="shortcut icon" href="{{url('/')}}/images/favicon.png">
  <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css">


  <link rel="stylesheet" href="{{url('/')}}/css/style.css">
  <link rel="stylesheet" href="{{url('/')}}/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{url('/')}}/css/jquery.fancybox.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/testimonial.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/materialdesignicons.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/owl.carousel.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/owl.theme.default.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/3dslider/demo.css" />
  <link rel="stylesheet" href="{{url('/')}}/css/3dslider/style.css" />
  <link rel="stylesheet" type="text/css" href="{{asset('/')}}css/YouTubePopUp.css">
  <link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/demo/Super-Simple-Event-Calendar-Plugin-For-jQuery-dnCalendar/css/dncalendar-skin.min.css" />
  <link rel="stylesheet" href="{{ asset('css/admin/datepicker/bootstrap-datepicker.css') }}">
  @if ( Config::get('app.locale') == 'ar')
  <link rel="stylesheet" href="{{url('/')}}/css/bootstrap-flipped.css">
  <link rel="stylesheet" href="{{url('/')}}/css/style_ar.css">
  <link rel="stylesheet" href="{{url('/')}}/css/responsive_ar.css">
  <!-- <link rel="stylesheet" href="{{url('/')}}/css/responsive.css"> -->
  @elseif ( Config::get('app.locale') == 'en')
  <link rel="stylesheet" href="{{url('/')}}/css/style_en.css">
  <link rel="stylesheet" href="{{url('/')}}/css/responsive.css">
  @endif
  @stack('styles')
  

  <meta property="og:url"                content="{{url()->current()}}" />
  <meta property="og:type"               content="article" />
  <meta property="og:title"              content="@yield('title')" />
  <meta property="og:description"        content="@yield('description')" />
  <meta property="og:image"              content="@yield('image')" /> 

</head>
<body>
  <header>
    <!--Header-->
    <div class="header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <a class="navbar-brand" href="{{Helper::BaseUrl('/main')}}">
              <img src="{{url('/')}}/images/logo.png" class="img-responsive logo">
            </a>
          </div>
          <div class="col-lg-7 col-md-6 col-sm-5 col-xs-12 hidden-xs">
            <div class="header-content hidden-sm">
             <!--  <p>
                @if ( Config::get('app.locale') == 'en')
                <a href="{{Helper::BaseUrl('/set')}}"> عربى</a>
                @elseif ( Config::get('app.locale') == 'ar')
                <a href="#">English</a>
                @endif 
              </p> -->
             <!-- <p class="mail-icon">
                <a href=" https://gmail.com" target="_blank"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
              </p>-->

              @if ( Config::get('app.locale') == 'en')
              <p>  <?php echo date("l F j, Y, g:i a"); ?> </p>
              @elseif ( Config::get('app.locale') == 'ar')
              <?php $date =  date("l F j, Y, g:i a"); ?>
              <p> {{Helper::ArabicDate($date)}}</p>
              @endif    
            </div>
          </div>
          <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">

            <div class="tolerance-section">
              <img src="{{url('/')}}/images/year_of_tolerance.png" alt="Year Of Tolerance">
            </div>
            <p class="social_lists hidden-xs">
              <a href="https://www.facebook.com/people/Fujairah-Fujairah/100008721224790" target="_blank" title="Follow us on Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              <a href="https://twitter.com/fjmediaoffice?lang=en" title="Follow us on Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> </a>

              <a href="https://www.instagram.com/fujairah_media_office/?hl=en" target="_blank" title="Join our Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              <a href="https://www.youtube.com/channel/UCdD63S8pzb8qQhgpB-NXRVg/" target="_blank" title="Subscribe to Youtube "><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
              <!-- <a href="#" target="_blank" title=""><i class="fa fa-linkedin" aria-hidden="true"></i></a> -->
            </p>
            <!-- </div> -->
          </div>

        </div>
      </div>
    </div>
    <!--Header End-->
    <!-- Navbar -->
    <nav class="navbar navbar-default header-menu">
      <div class="container-fluid">
        <ul class="nav navbar-nav">
          <li ><a class="{{ request()->is('/en') ? 'active' : '' }}" href="{{Helper::BaseUrl('/main')}}">{{trans('common.home')}}</a></li>
          <li class="nav-item dropdown dropdown_menu">
            <a class="nav-link dropdown-toggle @if(Request::path() === 'ar/about-us' || Request::path() === 'en/about-us' || Request::path() === 'ar/about-mediaoffice' || Request::path() === 'ar/about-vision' || Request::path() === 'ar/about-message' || Request::path() === 'ar/about-gov-communication-team' || Request::path() ==='en/about-mediaoffice' || Request::path() ==='en/about-vision' || Request::path() ==='en/about-message' || Request::path() ==='en/about-gov-communication-team') active @endif" href="#" id="navbardrop" >
              {{trans('common.about')}}
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/about-mediaoffice') }}">{{trans('common.about_media')}}</a>
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/about-vision') }}">{{trans('common.about_vision')}}</a>
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/about-message') }}">{{trans('common.about_message')}}</a>
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/about-gov-communication-team') }}">{{trans('common.about_team')}}</a>
            </div>
          </li>
          <li><a href="{{Helper::BaseUrl('/news')}}" class="{{ request()->is('news') ? 'active' : '' }}">{{trans('common.news')}}</a></li>
          <!--   <li><a class="{{ request()->is('events') ? 'active' : '' }}" href=" {{Helper::BaseUrl('/events')}}">{{trans('common.events')}}</a></li> -->
          <li class="nav-item dropdown dropdown_menu">
            <!-- data-toggle="dropdown" -->

            <a class="nav-link dropdown-toggle @if(Request::path() === 'ar/gallery' || Request::path() === 'en/gallery') active @endif" href="#" id="navbardrop">
              {{trans('common.gallery')}}
            </a>
            <div class="dropdown-menu gallery-menu">
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/gallery#pictures') }}" data-href="photo">{{trans('common.gallery_pictures')}}</a>
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/gallery#videos') }}" data-href="video">{{trans('common.gallery_videos')}}</a>
            </div>
          </li>

          <li class="nav-item dropdown dropdown_menu">
            <!-- data-toggle="dropdown" -->
            <a class="nav-link dropdown-toggle @if(Request::path() === 'ar/permission' || Request::path() === 'en/permission' || Request::path() === 'ar/training' || Request::path() === 'en/training' || Request::path() === 'ar/request-for-photo-or-video' || Request::path() === 'en/request-for-photo-or-video' || Request::path() === 'ar/library' || Request::path() === 'ar/library') active @endif" href="#" id="navbardrop">
              {{trans('common.services')}}
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/permission') }}">{{trans('common.services_permits')}}</a>
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/request-for-photo-or-video') }}">{{trans('common.services_request')}}</a>
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/training') }}">{{trans('common.services_training')}}</a>
              <a class="dropdown-item" href="{{ Helper::BaseUrl('/library') }}">{{trans('common.services_library')}}</a>
            </div>
          </li>

          <li><a href="{{Helper::BaseUrl('/publications')}}" class="{{ request()->is('publications') ? 'active' : '' }}">{{trans('common.publications')}}</a></li>
          <!--  <li><a href="{{ Helper::BaseUrl('/gallery') }} " class="{{ request()->is('gallery') ? 'active' : '' }}">{{trans('common.gallery')}}</a></li> -->

          <li><a href=" {{Helper::BaseUrl('/contact')}} " class="{{ request()->is('contact') ? 'active' : '' }}">{{trans('common.contact')}}</a></li>
        </ul>

        <div class="pull-right">
         <form method="post" id="demo-2" action="{{ Helper::BaseUrl('/search')}}" accept-charset="UTF-8" autocomplete="off">
          <div id="custom-search-input">
            <div class="input-group col-md-2">
             {{csrf_field()}}
             <input  type="search" class="form-control" name="globsearch" placeholder="" />
           </div>
         </div>
       </form>
     </div>
   </div>
 </nav>

 <nav class="mobile-menu hidden-lg hidden-md hidden-sm">
  <div class="row">
    <div class="container-fluid">
        <div class="col-xs-2 col-sm-2 right">
      <i class="mdi mdi-menu mdi-24px" onclick="openNav()"></i>
    </div>
      <div class="col-xs-10 col-sm-10 left">
        <p class="social_lists">
         <a href="https://www.facebook.com/people/Fujairah-Fujairah/100008721224790" target="_blank" title="Follow us on Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
         <a href="https://twitter.com/fjmediaoffice?lang=en" title="Follow us on Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> </a>

         <a href="https://www.instagram.com/fujairah_media_office/?hl=en" target="_blank" title="Join our Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
         <a href="https://www.youtube.com/channel/UCdD63S8pzb8qQhgpB-NXRVg/" target="_blank" title="Subscribe to Youtube "><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
       </p>
     </div>
   
  </div>
</div>
</nav>
<!-- Navbar End-->
<div id="mySidenav" class="sidenav  @if ( Config::get('app.locale') == 'en')''
@elseif ( Config::get('app.locale') == 'ar')rightmenu @endif">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="{{Helper::BaseUrl('/main')}}">
  <img src="{{url('/')}}/images/logo.png" alt="FCMC" class="moblogo">
</a>
  <a class="{{ request()->is('/') ? 'active' : '' }}" href="{{ Helper::BaseUrl('/')}}">{{trans('common.home')}}</a>

  <a href="#aboutmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">{{trans('common.about')}}</a>
  <ul class="collapse list-unstyled sidemenu-dropdown" id="aboutmenu">
    <li>
     <a class="dropdown-item" href="{{ Helper::BaseUrl('/about-mediaoffice') }}">{{trans('common.about_media')}}</a>
   </li>
   <li>
     <a class="dropdown-item" href="{{ Helper::BaseUrl('/about-vision') }}">{{trans('common.about_vision')}}</a>
   </li>
   <li>
    <a class="dropdown-item" href="{{ Helper::BaseUrl('/about-message') }}">{{trans('common.about_message')}}</a>
  </li>
  <li>
   <a class="dropdown-item" href="{{ Helper::BaseUrl('/about-gov-communication-team') }}">{{trans('common.about_team')}}</a>
 </li>
</ul>

<a class="{{ request()->is('news') ? 'active' : '' }}" href="{{ Helper::BaseUrl('/news')}}">{{trans('common.news')}}</a>
<!-- <a class="{{ request()->is('events') ? 'active' : '' }}" href="{{ Helper::BaseUrl('/events')}}">{{trans('common.events')}}</a> -->

<a class="{{ request()->is('gallery') ? 'active' : '' }} dropdown-toggle" href="#gallerymenu" data-toggle="collapse" aria-expanded="false">{{trans('common.gallery')}}</a>
<ul class="collapse list-unstyled sidemenu-dropdown" id="gallerymenu">
 <li>
  <a class="dropdown-item" href="{{ Helper::BaseUrl('/gallery#pictures') }}" data-href="photo">{{trans('common.gallery_pictures')}}</a>
</li>
<li>  
  <a class="dropdown-item" href="{{ Helper::BaseUrl('/gallery#videos') }}" data-href="video">{{trans('common.gallery_videos')}}</a>
</li>
</ul>

<a href="#servicesmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> {{trans('common.services')}}</a>
<ul class="collapse list-unstyled sidemenu-dropdown" id="servicesmenu">
  <li>
   <a class="dropdown-item" href="{{ Helper::BaseUrl('/permission') }}">{{trans('common.services_permits')}}</a>
 </li>
  <li>
  <a class="dropdown-item" href="{{ Helper::BaseUrl('/request-for-photo-or-video') }}">{{trans('common.services_request')}}</a>
</li>
 <li>
   <a class="dropdown-item" href="{{ Helper::BaseUrl('/training') }}">{{trans('common.services_training')}}</a>
 </li>
<li>
  <a class="dropdown-item" href="{{ Helper::BaseUrl('/library') }}">{{trans('common.services_library')}}</a>
</li>
</ul>

<!--     <a class="{{ request()->is('/') ? 'active' : '' }}" href="#">{{trans('common.media_office')}}</a> -->
<a href="{{Helper::BaseUrl('/publications')}}" class="{{ request()->is('publications') ? 'active' : '' }}">{{trans('common.publications')}}</a>

<a class="{{ request()->is('contact') ? 'active' : '' }}" href="{{ Helper::BaseUrl('/contact')}}">{{trans('common.contact')}}</a>
</div>
</header>


@yield('content')

<footer>
  <div class="primary-footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 one">
          <h4>{{trans('common.important_links')}}</h4>
          @if ( Config::get('app.locale') == 'en')
          <p>
            ultricies dictum.<br>
            ae Cras pulvinar eleifend tristique.<br>
            ae Sed sit amet tempor ex.ae
          </p>
       <!--  <ul class="important_links">
        <li><a href="http://www.fujairah.ae/ar/Pages/default.aspx" target="_blank">lorem ipsum dolor</a></li>
        <li><a href="https://www.fujairahpolice.gov.ae/ar/Pages/default.aspx" target="_blank">
lorem ipsum dolor</a></li>
        <li> <a href="https://www.fujmun.gov.ae/" target="_blank">lorem ipsum dolor </a></li>
        <li><a href="http://www.fujairahport.ae/?page_id=146" target="_blank"> lorem ipsum dolor</a></li>
        <li><a href="https://fnrc.gov.ae/portal/ar/index.aspx" target="_blank">lorem ipsum dolor</a></li>
        <li><a href="https://fcma.gov.ae/" target="_blank">lorem ipsum dolor </a></li>
        <li><a href="http://www.fuj-hr.ae/" target="_blank">lorem ipsum dolor </a></li>
        <li><a href="http://ffinance.ae/Pages/default.aspx" target="_blank">lorem ipsum dolor </a></li>
        <li><a href="https://www.fujairahfreezone.com/ar/" target="_blank">lorem ipsum dolor</a></li>
        <li><a href="http://www.fujairahtourism.ae/index.php?ln=ar" target="_blank">lorem ipsum dolor</a></li>
        <li><a href="http://www.fujcci.ae/" target="_blank">lorem ipsum dolor </a></li>
        <li><a href="https://www.fscfuj.gov.ae/" target="_blank">lorem ipsum dolor</a></li>
        <li><a href="http://www.pwad.gov.ae/ar/home" target="_blank">lorem ipsum dolor</a></li>
        <li><a href="https://www.ffaa.ae/ar/index.php" target="_blank"> lorem ipsum dolor</a></li>
      </ul> -->

      @elseif ( Config::get('app.locale') == 'ar')
      <ul class="important_links">
        <li><a href="http://www.fujairah.ae/ar/Pages/default.aspx" target="_blank">البوابة الرسمية لحكومة الفجيرة </a></li>
        <li><a href="https://www.fujairahpolice.gov.ae/ar/Pages/default.aspx" target="_blank">القيادة العامة لشرطة الفجيرة </a></li>
        <li><a href="https://www.fujmun.gov.ae/" target="_blank">بلدية الفجيرة </a></li>
        <li><a href="http://www.fujairahport.ae/?page_id=146" target="_blank"> ميناء الفجيرة  </a></li>
        <li><a href="https://fnrc.gov.ae/portal/ar/index.aspx" target="_blank">مؤسسة الفجيرة للموارد الطبيعية </a></li>
        <li><a href="https://fcma.gov.ae/" target="_blank">هيئة الفجيرة للثقافة والإعلام </a></li>
        <li><a href="http://www.fuj-hr.ae/" target="_blank">دائرة الموارد البشرية بالفجيرة </a></li>
        <li><a href="http://ffinance.ae/Pages/default.aspx" target="_blank">الدائرة المالية </a></li>
        <li><a href="https://www.fujairahfreezone.com/ar/" target="_blank">هيئة المنطقة الحرة بالفجيرة </a></li>
        <li><a href="http://www.fujairahtourism.ae/index.php?ln=ar" target="_blank">هيئة الفجيرة للسياحة والآثار </a></li>
        <li><a href="http://www.fujcci.ae/" target="_blank">غرفة تجارة وصناعة الفجيرة </a></li>
        <li><a href="https://www.fscfuj.gov.ae/" target="_blank">مركز الفجيرة للإحصاء </a></li>
        <li><a href="http://www.pwad.gov.ae/ar/home" target="_blank">دائرة الأشغال والزراعة بالفجيرة  </a></li>
        <li><a href="https://www.ffaa.ae/ar/index.php" target="_blank">اكاديمية الفجيرة للفنون الجميلة</a></li>
      </ul>
      @endif
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 two">
      <h4>{{trans('common.contact_us')}}</h4>
      @if ( Config::get('app.locale') == 'en')
      <p>
        <span><i class="fa fa-map-marker fa-lg fa-fw" aria-hidden="true"></i></span>
        <span><a href="{{ Helper::BaseUrl('/contact')}}">Emiri Court, Al Ittihad Road - Fujairah</a></span>
      </p>
      @elseif ( Config::get('app.locale') == 'ar')
      <p>
        <span><i class="fa fa-map-marker fa-lg fa-fw" aria-hidden="true"></i></span>
        <span><a href="{{ Helper::BaseUrl('/contact')}}">الديوان الأميري، المبنى المركزي، الطابق الأول </a></span>
      </p>
      @endif
      @if ( Config::get('app.locale') == 'en')

      <p>
        <span><i class="fa fa-phone fa-lg fa-fw" aria-hidden="true"></i></span>
        <span><a href="tel: 09 205 0851"> 09 205 0851</a> </span>
      </p>
      <p>
        <span><i class="fa fa-fax fa-lg fa-fw" aria-hidden="true"></i></span>
        <span><a href="tel: 09 222 3399">09 222 3399 </a> </span>
      </p>
      <p>
        <span><i class="fa fa-envelope fa-lg fa-fw" aria-hidden="true"></i></span>
        <span><a href="mailto:info@media.fujairah.ae">info@media.fujairah.ae</a> </span>
      </p>
      @elseif ( Config::get('app.locale') == 'ar')
      <p>
        <span><i class="fa fa-phone fa-lg fa-fw" aria-hidden="true"></i></span>
        <span><a href="tel: 09 205 0851" dir="ltr">09 205 0851</a> </span>
      </p>
      <p>
        <span><i class="fa fa-fax fa-lg fa-fw" aria-hidden="true"></i></span>
        <span><a href="tel: 09 222 3399" dir="ltr">09 222 3399</a> </span>
      </p>
      <p>
        <span><i class="fa fa-envelope fa-lg fa-fw" aria-hidden="true"></i></span>
        <span><a href="mailto:info@media.fujairah.ae">info@media.fujairah.ae</a> </span>
      </p>
      @endif


    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 three">
      <!-- <ul class="social">
        <li><a href="#" target="_blank" title="Follow us on Facebook"><i class="mdi mdi-facebook mdi-18px"></i></a></li>
        <li><a href="https://twitter.com/fjmediaoffice?lang=en" title="Follow us on Twitter" target="_blank"><i class="mdi mdi-twitter mdi-18px"></i></a></li>

        <li><a href="https://www.instagram.com/fujairah_media_office/?hl=en" title="Join our Instagram" target="_blank"><i class="mdi mdi-instagram mdi-18px"></i></a></li>
        <li><a href="https://www.youtube.com/channel/UCdD63S8pzb8qQhgpB-NXRVg/" target="_blank" title="Subscribe to Youtube"><i class="mdi mdi-youtube mdi-18px"></i></a></li>
      </ul> -->
      <div class="mapouter">
        <div class="gmap_canvas">
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14449.321989534754!2d56.333432!3d25.124515!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc55a35ba01e1e073!2sEmiri%20Court!5e0!3m2!1sen!2sus!4v1573543025877!5m2!1sen!2sus" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- <div class="container-fluid"><hr></div> -->

<div class="footer-under secondary-footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <p>Copyright 2019. <a href="{{url('/')}}">
          @if ( Config::get('app.locale') == 'en')
          Government of Fujairah
          @elseif ( Config::get('app.locale') == 'ar')
          مكتب الفجيرة الاعلامي
          @endif
        </a></p>  
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <ul class="privacy-link">
          <li><a href="{{Helper::BaseUrl('/privacy-policy')}}">{{trans('common.privacy_policy')}} </a></li>
          <li><a href="{{Helper::BaseUrl('/terms-and-conditions')}}"> {{trans('common.terms-conditions')}}
          </a></li>
        </ul> 
      </div>
    </div>
  </div>
</div>

</footer>
</body>

<script src="{{url('/')}}/js/jquery-3.3.1.min.js"></script>
<script src="{{url('/')}}/js/3dslider/modernizr.custom.53451.js"></script>
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/js/owl.carousel.min.js"></script>
<script src="{{url('/')}}/js/jquery.fancybox.min.js"></script>
<script src="{{url('/')}}/js/3dslider/jquery.gallery.js"></script>
<script src="{{asset('/')}}js/YouTubePopUp.jquery.js"></script>
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
<script src="{{ asset('js/admin/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript">
  $(function() {
    $('#dg-container').gallery();
  });
   $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip();
  });
</script>
<script>
  /* Set the width of the side navigation to 250px */
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  /* Set the width of the side navigation to 0 */
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  $('#demo-2 input[type=search]').click(function () {
    $('.search').toggleClass('expanded');
  });

  $(function(){

    var url = window.location.pathname, 
        urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
        // now grab every link from the navigation

        $('.header-menu a').each(function(){
            // and test its normalized href against the url pathname regexp
            if(urlRegExp.test(this.href.replace(/\/$/,''))){
              $(this).addClass('active');
            }
          });


        /* Gallery menu */
        var type = window.location.hash.substr(1);
        $(".galleryselect").each(function(){
          if(type == this.id){
            $(this).trigger('click');
          }
        });
        /* End Gallery menu */

      });

  $(document).ready(function(){
    $('.about-list li a').on('click',function(){
      var tab = $(this).data('tab');
      $('.about-sectiontab ul li a').each(function(i){
        if(tab == $(this).attr("href")){ 
          $(this).trigger('click');
        }
      })
    });

    var hash = window.location.hash.substr(1);
    var hash1 = location.hash;
    $('.about-sectiontab ul li a').each(function(i){
      if(hash1 == $(this).attr("href")) { 
        $(this).trigger('click');   
      }
    });
  });

  $(window).on('hashchange', function(e){
    var hash = window.location.hash.substr(1);
    var hash1 = location.hash;
    $('.about-sectiontab ul li a').each(function(i){
      if(hash1 == $(this).attr("href")){ 
        $(this).trigger('click'); 
      }
    });
  });

</script>
@yield('js')
</html>