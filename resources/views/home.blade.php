@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
	<div class="row">
        <div class="col-lg-3 col-xs-6">
          	<div class="small-box bg-aqua">
            	<div class="inner">
              		<h3>150</h3>
              		<p>New Orders</p>
            	</div>
            	<div class="icon">
              		<i class="ion ion-bag"></i>
            	</div>
            	<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          	</div>
        </div>
        <div class="col-lg-3 col-xs-6">
          	<div class="small-box bg-green">
            	<div class="inner">
              		<h3>53<sup style="font-size: 20px">%</sup></h3>
              		<p>Bounce Rate</p>
            	</div>
            	<div class="icon">
              		<i class="ion ion-stats-bars"></i>
            	</div>
            	<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          	</div>
        </div>
        <div class="col-lg-3 col-xs-6">
          	<div class="small-box bg-yellow">
            	<div class="inner">
              		<h3>44</h3>
              		<p>User Registrations</p>
            	</div>
            	<div class="icon">
              		<i class="ion ion-person-add"></i>
            	</div>
            	<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          	</div>
        </div>
        <div class="col-lg-3 col-xs-6">
          	<div class="small-box bg-red">
            	<div class="inner">
              		<h3>65</h3>
              		<p>Unique Visitors</p>
            	</div>
            	<div class="icon">
              		<i class="ion ion-pie-graph"></i>
            	</div>
            	<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          	</div>
        </div>
    </div>
    
<!--     <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Quick Example</h3>
        </div>
        <form role="form">
        	<div class="box-body">
            	<div class="form-group">
              		<label for="name">Name</label>
              		<input type="text" class="form-control" id="name" placeholder="Enter Name">
            	</div>
            	<div class="form-group">
              		<label for="email">Email</label>
              		<input type="email" class="form-control" id="email" placeholder="Enter Email">
            	</div>
            	<div class="form-group">
              		<label for="password">Password</label>
              		<input type="password" class="form-control" id="password" placeholder="Enter Password">
            	</div>
          	</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-primary">Submit</button>
          	</div>
        </form>
    </div> -->
@stop