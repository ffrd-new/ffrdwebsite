<?php 
$authId = Auth::user()->id;
$rname = Helper::getRolename($authId);
$roleid = '';
foreach ($rname as $key => $value) {
  $roleid = $value->role_id;
}
?>

<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>News Category االفئة لأخبار</h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}" toggle_form="#add_category-{{$tablename}}"> Add إضافة</button>
    </div>
  </div>
</div>
<style>
  .toggleform{ display: block !important; }
</style>
<div id="add_category-{{$tablename}}" class="hide_form-{{$tablename}}">
  <form id="category_form-{{$tablename}}" action="{{Helper::admin_prefix('cms_category_store') }}">
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="title">Category  <span>*</span></label>
            <input type="text" class="form-control" name="en_name" placeholder="Category"  id="title">
            <input type="hidden" name="type" value="{{$page['type']}}">
            <input type="hidden" name="get_url" value="cms_category">
            <input type="hidden" name="id" value="">
            <input type="hidden" name="form_id" value="{{$tablename}}">
          </div>
         
          <div class="form-group">
            <label>Parent Category القسم الرئيسي</label>
            <select class="form-control select2 select2-hidden-accessible" id="category-{{$tablename}}"  style="width: 100%;" tabindex="-1" aria-hidden="true" name="is_parent">
              <option value="">Category</option>
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group arabic_form">
            <label for="title"> الفئة  <span>*</span></label>
            <input type="text" class="form-control" name="ar_name" placeholder=" الفئة  "  id="title">
            <input type="hidden" name="type" value="{{$page['type']}}">
            <input type="hidden" name="get_url" value="cms_category">
            <input type="hidden" name="id" value="">
            <input type="hidden" name="form_id" value="{{$tablename}}">
          </div>
          @if($roleid == 1) 
          <div class="form-group">
            <label for="status">Status الحالة  </label><br>
            <input type="checkbox" class="form-control" checked data-toggle="toggle" id="status-{{$tablename}}" value="true" name="status">
          </div>
          @endif
            {{csrf_field()}}
          
          <div class="form-group">
            <button type="submit" class="btn submit-btn btn-flat pull-right" id="submit_blog_category">Submit  تأكيد            </button>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        </div>
      </div>
    </div>
  </form>

<span id="console-event"></span>
</div>

<script type="text/javascript">

  $(document).on('change','#title',function(){
      var slug=$('#slug').val();
      var title=$('#title').val();
        $('#slug').val(title);
  });

</script>
   <!-- Main content -->
    <section class="content" style="padding: 0px 5px">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="border-top:none;">
            <!-- /.box-header -->
            <div class="box-body">
              <div>
                <table  class="table  table-bordered table-hover" id="table-{{$tablename}}">
                <thead>
                <tr>
                  @if($lang=='ar')
                  <th> عنوان   </th>
                  <th>نوع </th>
                  <th> الحالة  </th>
                  <th> عمل </th>
                  @else
                  <th>Title</th>
                  <th>Type</th>
                  <th>Status</th>
                  <th>Action</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                </tbody>
                 </table>
               </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

        <script type="text/javascript"> 
        var tableid =  "{{$tablename}}";
        var type = "{{$page['type']}}";
        var cc = [{"data":"title","name":"title"},{"data":"type","name":"type"},{"data":"status","name":"status"},{"data":"action","name":"action"}];
         var x =   FormAction({
              formname:"#category_form-"+tableid,
              fid:tableid,
              datatable:true,
              selectjs:[{
                selector:"#category-"+tableid,
                dynamic:true,
                placeholder:"Select Category",
                url:APP_URL+"/admin/cms_category?content="+type+"&category=parent",
              }],
              custjs:true,
              custype:type,
              geturl:'cms_category',
              toggle_name:["status"],
              tableurl:"{{url('/admin')}}/cms_category_table?type="+type+"&lang={{$lang}}",
              tablecol:{!!$table!!},
            });

            $('<div class="lang">' +
    '<select class="form-control" id="egname" name="lang">' +
    '<option value="en">English</option>' +
    '<option value="ar"> عربى  </option>' +
    '</select>' +
    '</div>').appendTo('.dataTables_filter');  

            @if($lang)
                $('select[id="egname"]').val('{{$lang}}')
            @endif
        </script>

<script type="text/javascript">
    $(document).ready(function(){
    $('.dropify-wrapper').css('height','35px!important');
  });
  
$("#egname").on("change", function(event){
var category = $('select[id="egname"]').val();
// var filter_value = $(this).val();
// var liveurl = APP_URL+"/admin/langmember/"+category+"/"+type;
// x.ajax.url(liveurl).load();

// if(category == 'ar'){
// $('.en').hide();
// $('.ar').show();
// }
     $.ajax({
        url: APP_URL+"/admin/cms_view?type="+type+"&lang="+category,
        dataType : 'html',
        success:function(data){
        $('.'+type).html(data);
        }
        })
     
});  
</script>