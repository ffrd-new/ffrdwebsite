@extends('adminlte::page')

@section('title', 'News')

@section('content_header')
    <h1><i class="fa fa-newspaper-o" aria-hidden="true"></i> News الأخبار </h1>
@stop

@section('content')
@php
$lang = 'en';
@endphp
   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#news_category" data-toggle="tab">News Category فئة الأخبار</a></li>
              <li><a href="#news" data-toggle="tab">News الأخبار </a></li>
              <li><a href="#news_highlights" data-toggle="tab">News Highlights أبرز الأخبار </a></li>
              
            </ul>
            <div class="tab-content" style="padding:0px;">
              <div class="tab-pane" id="news">
               <span class="news"></span>
              </div>
    
              <div class="tab-pane active" id="news_category">
                <span class="news_category"></span>
              </div>
                <div class="tab-pane" id="news_highlights">
                <span class="news_highlights"></span>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
@stop
@section('adminlte_js')
  <script>
      _blog_cms_page('{{$lang}}')
  </script>
@stop