<div class="row">
 <div class="box-body">
  <div class="col-md-10">
    <h4>FAQ</h4>
  </div>
  <div class="col-md-2">
    <button type="button" class="btn bg-grey btn-flat pull-right add"  toggle_form="#add_faq">Add FAQ</button>
  </div>
</div>
</div>
<div id="add_faq" class="hide_form">
  <form id="faq_form">
   <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="title">Title <span>*</span></label>
          <input type="text" class="form-control" name="title" placeholder="Title" required id="title">
        </div>
        <div class="form-group">
          <label for="content">Content <span>*</span></label>
          <textarea name="content" placeholder="Content" id="content" required></textarea>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="category">Category <span>*</span></label>
          <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" id="category" required>
            <option selected="selected">Category</option>
          </select>
        </div>
        <div class="form-group">
          <label for="f_image">Featured Image</label>
          <input type="file" class="dropify" data-min-width="400" name="f_image" id="f_image" />
        </div>
        <div class="form-group">
          <label for="meta_decp">Meta Description</label>
          <textarea name="meta_decp" placeholder="Meta Description" class="form-control" id="meta_decp"></textarea>
        </div>
        <div class="form-group">
          <label for="status">Status</label><br>
          <input type="checkbox" checked data-toggle="toggle" id="status" name="status">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <button type="submit" class="btn bg-grey btn-flat pull-right">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form>
<hr>
</div>
<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
 <div class="box-body">
  <table id="Table" class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Category</th>
        <th>Status</th>
        <th>Publish on</th>
        <th><i class="fa fa-bars" aria-hidden="true"></i></th>
      </tr>
    </thead>
    <tbody id="faq_list">
      <tr>
        <td>1</td>
        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
        <td>FAQ</td>
        <td>Published</td>
        <td>11/3/19</td>
        <td><i class="fa fa-pencil edit_faq edit" aria-hidden="true" title="" toggle_form="#add_faq"></i> | <i class="fa fa-trash-o delete_faq" aria-hidden="true" title=""></i></td>
      </tr>
    </tbody>
  </table>
</div>
</div>