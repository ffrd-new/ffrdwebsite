@extends('adminlte::page')

@section('title', 'FAQ')

@section('content_header')
    <h1><i class="fa fa-question-circle" aria-hidden="true"></i> FAQ</h1>
@stop

@section('content')
	 <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#faq" data-toggle="tab">FAQ</a></li>
              <li><a href="#faq_category" data-toggle="tab">FAQ Category</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="faq">
                @include('admin.faq.faq')
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="faq_category">
                @include('admin.faq.faq_category')
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
@stop
@section('adminlte_js')
  <script>
    //
      
    $(document).ready(function(){

      var flag = 0;
      //Toggle
      $('.hide_form').hide();
      $('.add').click(function(){
        var form = $(this).attr('toggle_form');
        $('body').find(form).toggle('slow');
      });

       $('.edit').click(function(){
        var form = $(this).attr('toggle_form');
        $('body').find(form).toggle('slow');
      });

      //CKEditor
      CKEDITOR.replace( 'content' );
     

      //Select2
      $('.select2').select2();

      //Dropify
      $('.dropify').dropify();

      //Datatable
      $('#myTable').DataTable();
    });

  </script>
@stop