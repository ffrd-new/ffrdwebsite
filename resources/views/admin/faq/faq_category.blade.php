<div class="row">
 <div class="box-body">
  <div class="col-md-10">
    <h4>FAQ Category</h4>
  </div>
  <div class="col-md-2">
    <button type="button" class="btn bg-grey btn-flat pull-right add" toggle_form="#add_faq_category">Add FAQ Category</button>
  </div>
</div>
</div>
<div id="add_faq_category" class="hide_form">
  <form id="faq_category_form">
   <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="title">Category <span>*</span></label>
          <input type="text" class="form-control" name="title" placeholder="Title" id="title" required>
        </div>

      </div>
      <div class="col-md-6">
       <div class="form-group">
        <label for="status">Category Status</label><br>
        <input type="checkbox" class="form-control" checked data-toggle="toggle" id="status">
      </div>
      <div class="form-group">
        <button type="submit" class="btn bg-grey btn-flat pull-right">Submit</button>
      </div>
    </div>
  </div>
</div>
</form>
<hr>
</div>


<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
  <div class="box-body">
    <table id="Table" class="table table-bordered">
      <thead>
        <tr>
          <th>Id</th>
          <th>Title</th>
          <th><i class="fa fa-bars" aria-hidden="true"></i></th>
        </tr>
      </thead>
      <tbody id="faq_category_list">
        <tr>
          <td>1</td>
          <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
          <td><i class="fa fa-pencil edit_faq edit" aria-hidden="true" title="" toggle_form="#add_faq_category"></i> | <i class="fa fa-trash-o delete_faq" aria-hidden="true" title=""></i></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>