@extends('adminlte::page')

@section('title', 'Events')

@section('content_header')
    <h1><i class="fa fa-calendar" aria-hidden="true"></i> Events أحداث </h1>
@stop

@section('content')
@php
$lang = 'en';
@endphp
<style>
  .toggleform{ display: block !important; }
</style>
   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active" ><a href="#events_category" data-toggle="tab">Events Category فئة الأحداث
               </a></li>
              <li><a href="#events" data-toggle="tab">Events أحداث
              </a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="events_category">
                <span class="events_category"> </span>
              </div>
              <div class="tab-pane" id="events">
                <span class="events"> </span>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
@stop
@section('adminlte_js')
  <script>
      _blog_cms_page('{{$lang}}')
  </script>
@stop