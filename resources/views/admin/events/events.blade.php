<?php 
$authId = Auth::user()->id;
$rname = Helper::getRolename($authId);
$roleid = '';

foreach ($rname as $key => $value) {
  $roleid = $value->role_id;
}
?>
<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>Events أحداث
      </h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}"  toggle_form="#add_blog-{{$tablename}}">Add إضافة
</button>    </div>
  </div>
</div>
<div id="add_blog-{{$tablename}}" class="hide_form-{{$tablename}}">
      <form id="blog_form-{{$tablename}}" action="{{Helper::admin_prefix('cms_content_store') }}" >
    <div class="box-body">
      <div class="row">
       <div class="col-md-12 nopadding">
        <div class="col-md-5">
         <div class="form-group">
            <label for="category">Category الفئة  <span>*</span></label>
            <select class="form-control select2 select2-hidden-accessible category-{{$tablename}}" style="width: 100%;" tabindex="-1" aria-hidden="true" name="category" >
<!--               <option selected="selected">Category</option>
 -->            </select>
          </div>

       </div>
       <div class="col-md-5">
  <div class="form-group">
            <label for="f_image">Featured Image صورة مميزة</label>
             <input type="file" class="dropify" data-height="100"  data-min-width="400" name="f_image" id="f_image"/ data-show-remove="false">
            <p>Resolution 900X600 900X600 القرار </p>
          </div>
                     
       </div>
       <div class="col-md-5">
       <div class="form-group">
            <label>Event Date</label>
            <input type="text" class="form-control" name="event_date" id="event_date">
            </div>
       </div>
       
       @if($roleid == 1)
       <div class="col-md-2">
         <div class="form-group">
            <label for="status">Status الحالة  </label><br>
            <input type="checkbox" checked data-toggle="toggle" id="status" name="status">
          </div>
       </div>
       @endif
        </div>

      <div class="col-md-6 english_form">
        <div class="inner">
          <div class="form-group">
            <label for="title">Title <span>*</span></label>
            <input type="text" class="form-control" name="en_title" placeholder="Title" id="en_title" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="blog_content">Content <span>*</span></label>
            <textarea name="en_event_content" placeholder="Content" id="en_event_content"></textarea>
          </div>
          <div class="form-group">
            <label for="meta_title">URL</label>
            <input type="text" class="form-control" name="en_slug" placeholder="URL" id="en_slug" autocomplete="off">
          </div>
      
          <input type="hidden" name="id">
          <input type="hidden" name="setdat" id="setdat">
          {{csrf_field()}}
          <div class="form-group">
            <label for="meta_title">Meta Title</label>
            <input type="text" class="form-control" name="en_meta_title" placeholder="Meta Title" id="meta_title" autocomplete="off">
            <input type="hidden" name="get_url" value="cms_content">
           <input type="hidden" name="type" value="{{$page['type']}}">
          </div>
          <div class="form-group">
            <label for="meta_decp">Meta Description</label>
            <textarea name="en_meta_decp" placeholder="Meta Description" class="form-control" id="meta_decp"></textarea>
          </div>
          <div class="form-group">
            <label for="meta_keyword">Meta Keyword</label>
            <input type="text" class="form-control" name="en_meta_keyword" placeholder="Meta Keyword" id="en_meta_keyword" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="meta_keyword">Location</label>
            <input type="text" class="form-control" name="en_location" placeholder="Location" id="en_location" autocomplete="off">
          </div>
          
        </div>
      </div>

      <div class="col-md-6 arabic_form">
          <div class="inner">
            <div class="form-group">
              <label for="title"> عنوان  <span>*</span></label>
              <input type="text" class="form-control" name="ar_title" value="" placeholder=" عنوان  "  id="ar_title" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="content">يحتوى <span>*</span></label>
              <textarea name="ar_event_content" placeholder="Content"  id="ar_event_content" class="textarea"></textarea>
            </div>
             <div class="form-group">
              <label for="meta_title">
                رابط
              </label>
              <input type="text" class="form-control" name="ar_slug" placeholder="رابط  " id="ar_slug" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="meta_title">عنوان الفوقية</label>
              <input type="text" class="form-control" name="ar_meta_title" placeholder="عنوان الفوقية" id="meta_title" autocomplete="off">
              <input type="hidden" name="id">
            </div>
            <div class="form-group">
              <label for="meta_decp">ميتا الوصف</label>
              <textarea name="ar_meta_decp" placeholder="ميتا الوصف" class="form-control" id="meta_decp"></textarea>
            </div>
            <div class="form-group">
              <label for="meta_keyword">الفوقية الكلمة الرئيسية</label>
              <input type="text" class="form-control" name="ar_meta_keyword" placeholder="الفوقية الكلمة الرئيسية" id="meta_keyword" autocomplete="off">
              <input type="hidden" name="get_url" value="cms_content">
              <input type="hidden" name="type" value="{{$page['type']}}">
            </div>
            <div class="form-group">
            <label for="meta_keyword"> موقعك   </label>
            <input type="text" class="form-control" name="ar_location" placeholder=" موقعك   " id="ar_location" autocomplete="off">
           </div>
           
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <button type="submit" class="btn submit-btn btn-flat pull-right" id="submit_blog">Submit  تأكيد  </button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <hr>
</div>

<script type="text/javascript">
//Toggle

$(document).on('change','#ar_title',function(){
      var slug=$('#ar_slug').val();
      var title=$('#ar_title').val();
        $('#ar_slug').val(title);
  });
$(document).on('change','#en_title',function(){
      var slug=$('#en_slug').val();
      var title=$('#en_title').val();
        $('#en_slug').val(title);
  });

/*CKEDITOR.replace('event_content_ar',{
    height: '100px',
    contentsLangDirection: "rtl"
  });*/

  $(document).ready(function(){
    $('.dropify-wrapper').css('height','35px');
  });


</script>


    <section class="content" style="padding: 0px 5px">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="border-top:none;">
            <!-- /.box-header -->
            <div class="box-body">
              <div>
                <table  class="table  table-bordered table-hover" id="table-{{$tablename}}">
                <thead>
                <tr>
                  @if($lang=='ar')
                  <th>عنوان  </th>
                  <th> الفئة  </th> 
                  <th> الحالة  </th>
                  <th> تاريخ الحدث </th>
                  <th> عمل </th>
                  @else
                  <th>Title</th>
                  <th>Category</th>
                  <th>Status</th>
                  <th>Event Date</th>
                  <th>Action</th>
                  @endif

                </tr>
                </thead>
                <tbody>
                </tbody>
                 </table>
               </div>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <script type="text/javascript">
$("#egname").on("change", function(event){
var category = $('select[id="egname"]').val();
$.ajax({
url: APP_URL+"/admin/cms_view?type="+type+"&lang="+category,
dataType : 'html',
success:function(data){
$('.'+type).html(data);
}
})
});  

$( "#event_date" ).datetimepicker({
       format: 'YYYY-MM-DD HH:mm:ss',
    })

</script> 

    <script>
    var tableid =  "{{$tablename}}";
    var type = "{{$page['type']}}";
    FormAction({
      formname:"#blog_form-"+tableid,
      fid:tableid,
      datatable:true,
      type:type,
      images:[{
        name:'f_image',
        url:APP_URL+'/images/events/thumb/',
        delurl:APP_URL+'/delimage'
      }],
      editors:["en_event_content","ar_event_content"],
      selectjs:[{ 
        selector:".category-"+tableid,
        dynamic:true,
        placeholder:"Select Category",
        url:APP_URL+"/admin/cms_category?content="+type+"&category=all",
      }],
      geturl:$("[name=get_url]").val(),
      toggle_name:["status"],
      tableurl:"{{url('/admin')}}/cms_content_table?type="+type+"&lang={{$lang}}",
      tablecol:{!!$table!!}
    });
    $('<div class="lang">' +
    '<select class="form-control" id="egname" name="lang">' +
    '<option value="en">English</option>' +
    '<option value="ar"> عربى  </option>' +
    '</select>' +
    '</div>').appendTo('.dataTables_filter');  

    @if($lang)
        $('select[id="egname"]').val('{{$lang}}')
    @endif
    </script>

    <script type="text/javascript">
$("#egname").on("change", function(event){
var category = $('select[id="egname"]').val();
    $.ajax({
               url: APP_URL+"/admin/cms_view?type="+type+"&lang="+category,
               dataType : 'html',
               success:function(data){
               $('.'+type).html(data);
               }
               })
});
</script>
