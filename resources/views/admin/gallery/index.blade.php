@extends('adminlte::page')

@section('title', 'Gallery')

@section('content_header')
    <h1><i class="fa fa-file-image-o" aria-hidden="true"></i> Gallery
صالة عرض
    </h1>
@stop

@section('content')
@php
$lang = 'en';
@endphp
<style>
  .toggleform{ display: block !important; }
</style>
   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              
              <li class="active"><a href="#gallery_category" data-toggle="tab">Gallery Category فئة معرض
              </a></li>
              <li><a href="#gallery" data-toggle="tab">Image Gallery صالة عرض 
              </a></li>
              <li><a href="#video_gallery" data-toggle="tab">Video Gallery   معرض الفيديو
              </a></li>
            </ul>
            <div class="tab-content">
              
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="gallery_category">
                <span class="gallery_category"> </span>
              </div>

              <div class="tab-pane" id="gallery">
               <span class="gallery"> </span>
              </div>

              <div class="tab-pane" id="video_gallery">
               <span class="video_gallery"> </span>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
@stop
@section('adminlte_js')
  <script>
      _blog_cms_page('{{$lang}}')
  </script>
@stop