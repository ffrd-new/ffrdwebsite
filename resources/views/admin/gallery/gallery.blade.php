<?php 
$authId = Auth::user()->id;
$rname = Helper::getRolename($authId);
$roleid = '';

foreach ($rname as $key => $value) {
  $roleid = $value->role_id;
}
?>
<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>Image Gallery صالة عرض
      </h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}"  toggle_form="#add_blog-{{$tablename}}">Add إضافة
      </button>
    </div>
  </div>
</div>
<div id="add_blog-{{$tablename}}" class="hide_form-{{$tablename}}">
    <form id="blog_form-{{$tablename}}" action="{{Helper::admin_prefix('cms_content_store')}}" enctype="multipart/form-data">
    <div class="box-body">
      <div class="row">
 
      <div class="col-md-4">
        <div class="form-group">
            <label for="category">Category  الفئة  <span>*</span></label>
             <select class="form-control select2 select2-hidden-accessible category-{{$tablename}}" style="width: 100%;" tabindex="-1" aria-hidden="true" name="category" >
<!--               <option selected="selected">Category</option>
 -->        </select>
       </div>
      </div>
      <input type="hidden" name="get_url" value="cms_content">
      <input type="hidden" name="type" value="{{$page['type']}}">
      <div class="col-md-6">
        <div class="form-group">
            <label for="gimage">Featured Image  صورة مميزة    <span>*</span> </label>
            <input type="file" class="form-control" name="gimage[]" value="" multiple="multiple" >
          </div>
          <div class="iimg"></div>
      </div>

      @if($roleid == 1) 
      <div class="col-md-2">
        <div class="form-group">
            <label for="status">Status الحالة  </label><br>
            <input type="checkbox" checked data-toggle="toggle" name="status" id="status-{{$tablename}}" >
          </div>
      </div>
      @endif
      <br>
        <div class="col-md-6 english_form">
          <div class="inner">
          <div class="form-group">
            <label for="title"> Title
             <span>*</span></label>
            <input type="text" class="form-control" name="en_title" placeholder="Title" id="en_title" autocomplete="off">
          </div>
          <input type="hidden" name="id">
          <input type="hidden" name="meta_id">
           <div class="form-group">
            <!-- <label for="meta_title">
            URL
            </label> -->
            <input type="hidden" class="form-control" name="en_slug" placeholder="URL" id="en_slug" autocomplete="off" value="slug">
          </div>
        </div>
        </div>

        <div class="col-md-6 arabic_form">
          <div class="inner">
           <div class="form-group">
            <label for="title"> عنوان
             <span>*</span></label>
            <input type="text" class="form-control" name="ar_title" placeholder=" عنوان  " id="ar_title" autocomplete="off">
          </div>
          <input type="hidden" name="id">
           <div class="form-group">
           <!--  <label for="meta_title"> رابط  </label> -->
            <input type="hidden" class="form-control" name="ar_slug" placeholder=" رابط  " id="ar_slug" autocomplete="off" value=" سبيكة   ">
          </div>
          <input type="hidden" name="content_id" value="">
         
        <!--   <div class="form-group">
            <label for="meta_decp">Meta Description</label>
            <textarea name="meta_decp" placeholder="Meta Description" class="form-control" id="meta_decp"></textarea>
          </div> -->
          {{csrf_field()}}
          <!-- <div class="form-group">
            <label for="meta_title">Meta Title</label>
            <input type="text" class="form-control" name="meta_title" placeholder="Meta Title" id="meta_title">
            

          </div> -->
         <!--  <div class="form-group">
            <label for="meta_keyword">Meta Keyword</label>
            <input type="text" class="form-control" name="meta_keyword" placeholder="Meta Keyword" id="meta_keyword">
          </div> -->
        </div>
      </div>
      </div>
      <div class="row" style="padding-top: 10px;">
      <div class="col-md-6">
        <div class="form-group">
            <label>Publish Date</label>
            <input type="text" class="form-control" placeholder="Publish Date" name="image_date" id="image_date">
            </div>
      </div>
    </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <button type="submit" class="btn submit-btn btn-flat pull-right" id="submit_blog">Submit  تأكيد
            </button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <hr>
</div>

<script type="text/javascript">
//Toggle
  $(document).on('change','#en_title',function(){
      var slug=$('#en_slug').val();
      var title=$('#en_title').val();
        $('#en_slug').val(title);
  });

   $(document).on('change','#ar_title',function(){
      var slug=$('#ar_slug').val();
      var title=$('#ar_title').val();
        $('#ar_slug').val(title);
  });

</script>


     <section class="content" style="padding: 0px 5px">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="border-top:none;">
            <!-- /.box-header -->
            <div class="box-body">
              <div>
                <table  class="table  table-bordered table-hover" id="table-{{$tablename}}">
                <thead>
                <tr>
                  @if($lang=='ar')
                  <th> عنوان  </th>
                  <th> الفئة  </th>
                  <th> الحالة  </th>
                  <th> تاريخ النشر </th>
                  <th> عمل </th>
                  @else
                  <th>Title</th>
                  <th>Category</th>
                  <th>Status</th>
                  <th>Publish Date</th>
                  <th>Action</th>
                  @endif

                </tr>
                </thead>
                <tbody>
                </tbody>
                 </table>
               </div>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

      <script>

        $("#image_date" ).datepicker({
        format: 'yyyy-mm-dd',
        })

      var tableid =  "{{$tablename}}";
      var type = "{{$page['type']}}";
      FormAction({
      formname:"#blog_form-"+tableid,
      fid:tableid,
      datatable:true,
      type:type,
      gallery:'gimage',
      iurl : "{{url('/images/gallery')}}/",
      imgdelurl:"{{url('/admin/gallery/images/del')}}",
      token:"{{csrf_token()}}",  
      selectjs:[{ 
        selector:".category-"+tableid,
        dynamic:true,
        placeholder:"Select Category",
        url:APP_URL+"/admin/cms_category?content="+type+"&category=all",
      }],
      geturl:$("[name=get_url]").val(),
      toggle_name:["status"],
      tableurl:"{{url('/admin')}}/cms_content_table?type="+type+"&lang={{$lang}}",
      tablecol:{!!$table!!}
      });
      $('<div class="lang">' +
    '<select class="form-control" id="egname" name="lang">' +
    '<option value="en">English</option>' +
    '<option value="ar"> عربى  </option>' +
    '</select>' +
    '</div>').appendTo('.dataTables_filter');  

            @if($lang)
                $('select[id="egname"]').val('{{$lang}}')
            @endif
      </script>
<script type="text/javascript">
$(document).ready(function(){
$('.dropify-wrapper').css('height','35px!important');
});

$("#egname").on("change", function(event){
var category = $('select[id="egname"]').val();
$.ajax({
url: APP_URL+"/admin/cms_view?type="+type+"&lang="+category,
dataType : 'html',
success:function(data){
$('.'+type).html(data);
}
})
});            
</script> 