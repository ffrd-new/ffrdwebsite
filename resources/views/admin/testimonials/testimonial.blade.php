<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>Testimonials</h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add"  toggle_form="#add_testimonial">Add Testimonial</button>
    </div>
  </div>
</div>

<div id="add_testimonial" class="hide_form">
  <form id="testimonial_form">
   <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="name">Name <span>*</span></label>
          <input type="text" class="form-control" name="name" placeholder="Name" required id="name">
        </div>
        <div class="form-group">
          <label for="description">Description <span>*</span></label>
          <textarea class="form-control" name="description" placeholder="Description" required id="description"></textarea>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="f_image">Featured Image <span>*</span></label>
          <input type="file" class="dropify" data-min-width="400" name="f_image" id="f_image" required/>
        </div>
        <div class="form-group">
          <label for="status">Status</label><br>
          <input type="checkbox" checked data-toggle="toggle" id="status" name="status">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <button type="submit" class="btn bg-grey btn-flat pull-right">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form>
<hr>
</div>
<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
 <div class="box-body">
  <table id="Table" class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Category</th>
        <th>Status</th>
        <th>Publish on</th>
        <th><i class="fa fa-bars" aria-hidden="true"></i></th>
      </tr>
    </thead>
    <tbody id="testimonial_list">
      <tr>
        <td>1</td>
        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
        <td>Testimonial</td>
        <td>Published</td>
        <td>11/3/19</td>
        <td><i class="fa fa-pencil edit_testimonial edit" aria-hidden="true" title="" toggle_form="#add_testimonial"></i> | <i class="fa fa-trash-o delete_testimonial" aria-hidden="true" title=""></i></td>
      </tr>
    </tbody>
  </table>
</div>
</div>