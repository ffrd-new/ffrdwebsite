@extends('adminlte::page')

@section('title', 'Testimonials')

@section('content_header')
    <h1><i class="fa fa-fw fa-comments-o " aria-hidden="true"></i> Testimonials</h1>
@stop

@section('content')
	 <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#testimonial" data-toggle="tab">Testimonials</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="testimonial">
                @include('admin.testimonials.testimonial')
              </div>
              <!-- /.tab-pane -->
           
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
@stop
@section('adminlte_js')
  <script>
    //
      
    $(document).ready(function(){

      var flag = 0;
      //Toggle
      $('.hide_form').hide();
      $('.add').click(function(){
        var form = $(this).attr('toggle_form');
        $('body').find(form).toggle('slow');
      });

       $('.edit').click(function(){
        var form = $(this).attr('toggle_form');
        $('body').find(form).toggle('slow');
      });

      //Dropify
      $('.dropify').dropify();

      //Datatable
      $('#myTable').DataTable();
    });

  </script>
@stop