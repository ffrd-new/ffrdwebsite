<?php 
$authId = Auth::user()->id;
$rname = Helper::getRolename($authId);
$roleid = '';

foreach ($rname as $key => $value) {
  $roleid = $value->role_id;
}
?>

<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>Ruler الأخبار </h4>
    </div>
    <div class="col-md-2">
     <!--  <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}"  toggle_form="#add_blog-{{$tablename}}"> Add إضافة</button> -->
    </div>
  </div>
</div>

<div id="add_blog-{{$tablename}}" class="hide_form-{{$tablename}}">
  <form id="news_form-{{$tablename}}" action="{{Helper::admin_prefix('cms_content_store') }}" enctype="multipart/form-data">
    <div class="box-body">
      <div class="row">
        <div class="col-md-5" style="display: none;">
          <div class="form-group">       
            <label for="category">Category الفئة  <span>*</span></label>
            <select class="form-control select2 select2-hidden-accessible category-{{$tablename}}" style="width: 100%;" tabindex="-1" aria-hidden="true" name="category" >
<!--          <option selected="selected">Category</option>
 -->        </select>
          </div>
          {{csrf_field()}}
        </div>
        <div class="col-md-8">
          <div class="form-group">
            <label for="r_image">Featured Image صورة مميزة</label>
            <input type="file" class="dropify" data-height="100"  data-min-width="400" name="r_image" id="r_image"/ data-show-remove="false">
            <p>Resolution 900X600 900X600 القرار </p>
          </div>
        </div>
        <div class="col-lg-5" style="display: none;">
           <div class="form-group">
            <label for="bookpdf">PDF File صورة مميزة    <span>*</span> </label>
            <input type="file" class="form-control" name="bookpdf[]" value="" multiple="multiple">
          </div>
          <div class="pdfs col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
        </div>
        @if($roleid == 1)
        <div class="col-md-2">
          <div class="form-group">
            <label for="status">Status الحالة   </label><br>
            <input type="checkbox" checked data-toggle="toggle" id="status" name="status">
          </div>
        </div>
        @endif
       </div>
        <hr>
        <div class="col-md-6 english_form">
          <div class="inner">
            <div class="form-group">
              <label for="title">Name <span>*</span></label>
              <input type="text" class="form-control" name="en_title" value="" placeholder="Name"  id="en_title" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="content">Content <span>*</span></label>
              <textarea name="en_public_content" placeholder="Content" id="en_public_content"></textarea>
            </div>
            <div class="form-group">
              <label for="meta_title">URL</label>
              <input type="text" class="form-control" name="en_slug" placeholder="URL" id="en_slug" autocomplete="off">
            </div>
          
              <input type="hidden" name="id">
              <input type="hidden" name="meta_id">
              <input type="hidden" name="get_url" value="cms_content">
              <input type="hidden" name="type" value="{{$page['type']}}">
          </div>
        </div>
        <div class="col-md-6 arabic_form">
          <div class="inner">
            <div class="form-group">
              <label for="title"> عنوان  <span>*</span></label>
              <input type="text" class="form-control" name="ar_title" value="" placeholder=" عنوان  " id="ar_title" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="content">يحتوى <span>*</span></label>
              <textarea name="ar_public_content" placeholder="Content"  id="ar_public_content" class="textarea"></textarea>
            </div>
            <div class="form-group">
              <label for="meta_title">
                رابط
              </label>
              <input type="text" class="form-control" name="ar_slug" placeholder="رابط   " id="ar_slug" autocomplete="off">
            </div>
            
            <input type="hidden" name="id">
            <input type="hidden" name="get_url" value="cms_content">
            <input type="hidden" name="type" value="{{$page['type']}}">
           
            <div class="form-group">
          <button type="submit" class="btn submit-btn btn-flat pull-right" id="submit_news">
تأكيد   Submit </button>
            </div>
          </div>
        </div>
    </div>
  </form>
  <hr>
</div>

<script type="text/javascript">

  $(document).on('change','#en_title',function(){
      var slug=$('#en_slug').val();
      var title=$('#en_title').val();
        $('#en_slug').val(title);
  });
   $(document).on('change','#ar_title',function(){
      var slug=$('#ar_slug').val();
      var title=$('#ar_title').val();
        $('#ar_slug').val(title);
  });

 /*CKEDITOR.replace('ar_news_content',{
    height: '100px',
    contentsLangDirection: "rtl"
  });*/

  $(document).ready(function(){
    $('.dropify-wrapper').css('height','35px');
  });

</script>
<section class="content" style="padding: 0px 5px">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="border-top:none;">
            <!-- /.box-header -->
            <div class="box-body">
              <div>
                <table  class="table table-bordered table-hover ruler-table" id="table-{{$tablename}}">
                <thead>
                <tr>
                  @if($lang=='ar')
                  <th> عنوان   </th>
                  <th> الحالة </th>
                  <th> عمل </th>
                  @else
                  <th>Title</th>
                  
                  <th>Status</th>
                  <th>Action</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
               </div>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
      <script>
        var tableid =  "{{$tablename}}";
        var type = "{{$page['type']}}";
        
        FormAction({
          formname:"#news_form-"+tableid,
          fid:tableid,
          datatable:true,
          type:type,
          pdf:'bookpdf',
          pdfurl : "{{url('/public/images/publications/media')}}/",
          pdfdelurl:"{{url('/admin/publications/media/del')}}",
          token:"{{csrf_token()}}",  
          images:[{
            name:'r_image',
            url:APP_URL+'/images/ruler/thumb/'
          }],
          editors:["ar_public_content","en_public_content"],
          selectjs:[{
            selector:".category-"+tableid,
            dynamic:true,
            placeholder:"Select Category",
            url:APP_URL+"/admin/cms_category?content="+type+"&category=all",
          }],
          geturl:$("[name=get_url]").val(),
          toggle_name:["status"],
          tableurl:"{{url('/admin')}}/cms_content_table?type="+type+"&lang={{$lang}}",
          tablecol:{!!$table!!},
          });
       $('<div class="lang">' +
    '<select class="form-control" id="egname" name="lang">' +
    '<option value="en">English</option>' +
    '<option value="ar"> عربى  </option>' +
    '</select>' +
    '</div>').appendTo('.dataTables_filter');  

    @if($lang)
        $('select[id="egname"]').val('{{$lang}}')
    @endif
    
</script>

<script type="text/javascript">
$(document).ready(function(){
$('.dropify-wrapper').css('height','35px!important');
});

$("#egname").on("change", function(event){
var category = $('select[id="egname"]').val();
$.ajax({
url: APP_URL+"/admin/cms_view?type="+type+"&lang="+category,
dataType : 'html',
success:function(data){
$('.'+type).html(data);
}
})
});            
</script>   