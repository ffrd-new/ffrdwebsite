@extends('adminlte::page')

@section('title', 'Blog')

@section('content_header')
    <h1><i class="fa fa-rss" aria-hidden="true"></i> Blogs</h1>
@stop

@section('content')
<style>
  .toggleform{ display: block !important; }
</style>
   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#blogs" data-toggle="tab">Blogs</a></li>
              <li><a id="blogcategory" href="#blog_category"  data-toggle="tab">Blog Category</a></li>
              <li><a href="#video_blogs" data-toggle="tab">Video Blogs</a></li>
              <li><a href="#video_category" data-toggle="tab">Video Blog Category</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="blogs">
                <span class="blogs"> </span>
              </div>
    
              <div class="tab-pane" id="blog_category">
              <span class="blog_category"> </span>
              </div>

              <div class="tab-pane" id="video_blogs">          
                  <span class="video_blogs"> </span>
              </div>
          
              <div class="tab-pane" id="video_category">
                  <span class="video_category"> </span>
              </div>
          
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->



@stop
@section('adminlte_js')
  <script>
      _blog_cms_page()
  </script>
@stop