<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>{{$page['title']}}</h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}"  toggle_form="#add_blog-{{$tablename}}">Add {{$page['title']}}</button>
    </div>
  </div>
</div>
<div id="add_blog-{{$tablename}}" class="hide_form-{{$tablename}}">
      <form id="blog_form-{{$tablename}}" action="{{Helper::admin_prefix('cms_content_store') }}" >
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="title">Title <span>*</span></label>
            <input type="text" class="form-control" name="title" placeholder="Title" id="title" required autocomplete="off">
          </div>
          <div class="form-group">
            <label for="blog_content">Content <span>*</span></label>
            <textarea name="blog_content" placeholder="Content" id="blog_content" required></textarea>
          </div>
          <div class="form-group">
            <label for="meta_title">Url</label>
            <input type="text" class="form-control" name="slug" placeholder="url" id="meta_title" required autocomplete="off">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="category">Category <span>*</span></label>
            <select class="form-control select2 select2-hidden-accessible category-{{$tablename}}" style="width: 100%;" tabindex="-1" aria-hidden="true" required name="category" >
<!--               <option selected="selected">Category</option>
 -->            </select>
          </div>
          <input type="hidden" name="id">
          <div class="form-group">
            <label for="f_image">Featured Image</label>
            <input type="file" class="dropify" data-height="100"  data-min-width="400" name="f_image" id="f_image"/ data-show-remove="false" multiple>
            <p>Resolution 900X600</p>
          </div>
          <div class="form-group">
            <label for="meta_decp">Meta Description</label>
            <textarea name="meta_decp" placeholder="Meta Description" class="form-control" id="meta_decp"></textarea>
          </div>
          {{csrf_field()}}
          <div class="form-group">
            <label for="meta_title">Meta Title</label>
            <input type="text" class="form-control" name="meta_title" placeholder="Meta Title" id="meta_title" autocomplete="off">
            <input type="hidden" name="get_url" value="cms_content">
                        <input type="hidden" name="type" value="{{$page['type']}}">

          </div>
          <div class="form-group">
            <label for="meta_keyword">Meta Keyword</label>
            <input type="text" class="form-control" name="meta_keyword" placeholder="Meta Keyword" id="meta_keyword" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Blog Status</label><br>
            <input type="checkbox" checked data-toggle="toggle" name="status" id="status-{{$tablename}}" >
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <button type="submit" class="btn bg-grey btn-flat pull-right" id="submit_blog">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <hr>
</div>

@include('section.table')
<script type="text/javascript">
//Toggle

    var tableid =  "{{$tablename}}";
    var type = "{{$page['type']}}";
    FormAction({
      formname:"#blog_form-"+tableid,
      fid:tableid,
      datatable:true,
      type:type,
      images:[{
        name:'f_image',
        url:APP_URL+'/images/blog/thumb/',
        delurl:APP_URL+'/delimage'
      }],
      editors:["blog_content"],
      selectjs:[{ 
        selector:".category-"+tableid,
        dynamic:true,
        placeholder:"Select Category",
        url:APP_URL+"/admin/cms_category?content="+type+"&category=all",
      }],
      geturl:$("[name=get_url]").val(),
      toggle_name:["status"],
      tableurl:"{{url('/admin')}}/cms_content_table?type="+type,
      tablecol:{!!$table!!}
    });

</script>