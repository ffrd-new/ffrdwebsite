<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>{{$page['title']}}</h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}"  toggle_form="#add_blog-{{$tablename}}">Add {{$page['title']}}</button>
    </div>
  </div>
</div>

<div id="add_blog-{{$tablename}}" class="hide_form-{{$tablename}}">
      <form id="blog_form-{{$tablename}}" action="{{Helper::admin_prefix('cms_content_store') }}" >
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="title">Title <span>*</span></label>
            <input type="text" class="form-control" name="title" value="" placeholder="Title" required id="title">
          </div>
          <div class="form-group">
            <label for="category">Category <span>*</span></label>
            <select class="form-control select2 select2-hidden-accessible video" required style="width: 100%;" tabindex="-1" aria-hidden="true" id="category" required name="category">
              <option selected="selected">Category</option>
            </select>
          </div>
          <input type="hidden" name="id">
          <div class="form-group">
            <label for="video_url">Video URL <span>*</span></label>
            <input type="text" class="form-control" name="video_url" placeholder="Youtube URL" id="video_url" required>
          </div>

              <input type="hidden" name="get_url" value="cms_content">
              <input type="hidden" name="type" value="video_blogs">
              {{csrf_field()}}
          <div class="form-group">
            <label for="thumb_image">Thumb Image</label>
            <input type="file" class="dropify" data-min-width="400" name="f_image" id="thumb_image"/>
          </div>
          <div class="form-group">
            <label for="status">Blog Status</label><br>
            <input type="checkbox" name="status" checked data-toggle="toggle" id="status">
          </div>
        </div>
        
        <div class="col-md-6">
          <div class="form-group">
            <label>Content <span>*</span></label>
            <textarea name="video_blog_content" placeholder="Content" required></textarea>
          </div>
        </div>
        <div class="col-md-12">
         <div class="form-group">
          <button type="submit" class="btn bg-grey btn-flat pull-right" id="submit_video_blog">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form>
<hr>
</div>
@include('section.table')
<script type="text/javascript">
    var tableid =  "{{$tablename}}";
    var type = "{{$page['type']}}";
    FormAction({
      formname:"#blog_form-"+tableid,
      fid:tableid,
      type:type,
      datatable:true,
      images:[{
        name:'f_image',
        url:APP_URL+'/images/video/thumb/'

      }],
      editors:["video_blog_content"],
      selectjs:[{
        selector:".video",
        dynamic:true,
        placeholder:"Select Category",
        url:APP_URL+"/admin/cms_category?content="+type+"&category=all",
      }],
      geturl:$("[name=get_url]").val(),
      toggle_name:["status"],
      tableurl:"{{url('/admin')}}/cms_content_table?type="+type,
      tablecol:{!!$table!!}
    });
</script>