

<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>{{$page['title']}}</h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}" toggle_form="#add_category-{{$tablename}}">Add {{$page['title']}}</button>
    </div>
  </div>
</div>
<style>
  .toggleform{ display: block !important; }
</style>
<div id="add_category-{{$tablename}}" class="hide_form-{{$tablename}}">
  <form id="category_form-{{$tablename}}" action="{{Helper::admin_prefix('cms_category_store') }}">
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="title">Category <span>*</span></label>
            <input type="text" class="form-control" name="name" placeholder="Title" required id="title">
            <input type="hidden" name="type" value="{{$page['type']}}">
            <input type="hidden" name="get_url" value="cms_category">
            <input type="hidden" name="id" value="">
            <input type="hidden" name="form_id" value="{{$tablename}}">
          </div>
         
          <div class="form-group">
            <label>Parent Category</label>
            <select class="form-control select2 select2-hidden-accessible" id="category-{{$tablename}}"  style="width: 100%;" tabindex="-1" aria-hidden="true" name="is_parent">
              <option value="">Category</option>
           
            </select>
          </div>
        </div>
        <div class="col-md-6">
           <div class="form-group">
            <label for="status">Category Status</label><br>
            <input type="checkbox" class="form-control" checked data-toggle="toggle" id="status-{{$tablename}}" value="true" name="status">
              
            {{csrf_field()}}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <button type="submit" class="btn bg-grey btn-flat pull-right" id="submit_blog_category">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form>
<hr>
<span id="console-event"></span>
</div>

@include('section.table')
<script type="text/javascript">
//Toggle
  
    var tableid =  "{{$tablename}}";
    var type = "{{$page['type']}}";
    FormAction({
      formname:"#category_form-"+tableid,
      fid:tableid,
      datatable:true,
      
      selectjs:[{
        selector:"#category-"+tableid,
        dynamic:true,
        placeholder:"Select Category",
        url:APP_URL+"/admin/cms_category?content="+type+"&category=parent",
      }],
      geturl:'cms_category',
 toggle_name:["status"],
      tableurl:"{{url('/admin')}}/cms_category_table?type="+type,
      tablecol:{!!$table!!}
    });
</script>

