<?php
 
$authId = Auth::user()->id;
$rname = Helper::getRolename($authId);
$roleid = '';

foreach ($rname as $key => $value) {
  $roleid = $value->role_id;
}
?>

<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>User المستعمل    </h4>
    </div>
    @if($roleid == 1)
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}"  toggle_form="#add_blog-{{$tablename}}"> Add إضافة</button>
    </div>
    @endif
  </div>
</div>

<div id="add_blog-{{$tablename}}" class="hide_form-{{$tablename}}">
  <form id="news_form" action="{{Helper::admin_prefix('user/store') }}">
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
              <label for="title">Email / البريد الإلكتروني    <span>*</span></label>
              <input type="text" class="form-control" name="email" value="" placeholder="Email"  id="en_title" autocomplete="off">
          </div>
           
          <div class="form-group"> 
          <input type="hidden" name="get_url" value="user_content">

              <input type="hidden" name="type" value="{{$page['type']}}">      
            <label for="category">CMS Role تدحرج   <span>*</span></label>
            <select class="form-control select2 select2-hidden-accessible category-{{$tablename}}" style="width: 100%;" tabindex="-1" aria-hidden="true" name="category" >
          </select>
          </div>
          {{csrf_field()}}
        
        </div>
       <input type="hidden" name="id">  
       <input type="hidden" name="role">
        <div class="col-md-6">
           <div class="form-group">
              <label for="title">Password /كلمه السر    <span>*</span></label>
              <input type="password" class="form-control" name="password" value="" placeholder="Password"  id="en_title" autocomplete="off">
            </div>
            <div class="form-group">
            <label for="status">Status الحالة   </label><br>
            <input type="checkbox" checked data-toggle="toggle" id="status" name="status">
          </div>
        </div>
      
        <div class="col-md-12">
          <div class="form-group">
          <button type="submit" class="btn submit-btn btn-flat pull-right" id="submit_news">
            Submit 
تأكيد   
</button>
            </div>
        </div>
       </div>
        <hr>
    </div>
  </form>
  <hr>
</div>

<script type="text/javascript">
  $(document).on('change','#en_title',function(){
      var slug=$('#en_slug').val();
      var title=$('#en_title').val();
        $('#en_slug').val(title);
  });
   $(document).on('change','#ar_title',function(){
      var slug=$('#ar_slug').val();
      var title=$('#ar_title').val();
      $('#ar_slug').val(title);
  });

  $(document).ready(function(){
    $('.dropify-wrapper').css('height','35px');
  });

</script>
<section class="content" style="padding: 0px 5px">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="border-top:none;">
            <!-- /.box-header -->
            <div class="box-body">
              <div>
                <table  class="table  table-bordered table-hover" id="table-{{$tablename}}">
                <thead>
                <tr>
                  @if($lang=='ar')
                  <th> عنوان   </th>
                  <th> الحالة  </th>
                  <th> عمل </th>
                  <th> عمل </th>
                  @else
                  <th>Email</th>
                  <th>Role</th>
                  <th>Created On</th>
                  <th>Status</th>
                  <th>Action</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
               </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  <style type="text/css">
    a.disabled {
  pointer-events: none;
}
  </style>
<script type="text/javascript">
  
$(document).ready(function(){
$('.dropify-wrapper').css('height','35px!important');
});

$("#egname").on("change", function(event){
var category = $('select[id="egname"]').val();
$.ajax({
url: APP_URL+"/admin/cms_view?type="+type+"&lang="+category,
dataType : 'html',
success:function(data){
$('.'+type).html(data);
}
})
});            
</script>   

<script type="text/javascript">

//Toggle
    var tableid = "{{$tablename}}";
    var type = "{{$page['type']}}";
    FormAction({
      formname:"#news_form",
      fid:tableid,
      datatable:true,
      type:type,
      geturl:$("[name=get_url]").val(),
      selectjs:[{
            selector:".category-"+tableid,
            dynamic:true,
            placeholder:"Select Role",
            url:APP_URL+"/admin/user_role?content="+type+"&category=all",
          }],
      toggle_name:["status"],
      tableurl:"{{url('/admin')}}/usertable",
      tablecol:{!!$table!!}
    });
    $('body').on('click', '[data-act=ajax-del]', function (e) {
            var id = $(this).data('id');
            var form = $(this).attr('toggle_form');
    $('body').find(form).show();
              console.log(id);});

$('select[class=".form-control.select2"]').val(2);
</script>