@extends('adminlte::page')

@section('title', 'User Management')

@section('content_header')
    <h1><i class="fa fa-user" aria-hidden="true"></i>User المستعمل   </h1>
@stop

@section('content')
@php
$lang = 'en';
@endphp
   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#user" data-toggle="tab">User المستعمل   </li>
              <li><a href="#news" data-toggle="tab" style="display: none;">users</a></li>              
            </ul>
            <div class="tab-content" style="padding:0px;">
              <div class="tab-pane active" id="user">
               <span class="user"></span>
              </div>
               <div class="tab-pane" id="news">
               
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
@stop
@section('adminlte_js')
  <script>
     _blog_cms_page('{{$lang}}')
  </script>
@stop