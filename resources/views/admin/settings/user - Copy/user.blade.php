<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>User</h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add-{{$tablename}}"  toggle_form="#add_blog-{{$tablename}}"> Add إضافة</button>
    </div>
  </div>
</div>

<div id="add_blog-{{$tablename}}" class="hide_form-{{$tablename}}">
  <form id="news_form-{{$tablename}}" action="{{Helper::admin_prefix('cms_content_store') }}">
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
              <label for="title">Email / البريد الإلكتروني    <span>*</span></label>
              <input type="text" class="form-control" name="en_title" value="" placeholder="Title"  id="en_title" autocomplete="off">
          </div>
           
          <div class="form-group"> 
          <input type="hidden" name="get_url" value="cms_content">
              <input type="hidden" name="type" value="{{$page['type']}}">      
            <label for="category">Role تدحرج   <span>*</span></label>
            <select class="form-control select2 select2-hidden-accessible category-{{$tablename}}" style="width: 100%;" tabindex="-1" aria-hidden="true" name="category" >
<!--          <option selected="selected">Category</option>
 -->        </select>
          </div>
          {{csrf_field()}}
        
        </div>

        <div class="col-md-6">
           <div class="form-group">
              <label for="title">Password /كلمه السر    <span>*</span></label>
              <input type="text" class="form-control" name="en_title" value="" placeholder="Title"  id="en_title" autocomplete="off">
            </div>
            <div class="form-group">
            <label for="status">Status الحالة   </label><br>
            <input type="checkbox" checked data-toggle="toggle" id="status" name="status">
          </div>
        </div>
      
        <div class="col-md-12">
            <div class="form-group">
          <button type="submit" class="btn submit-btn btn-flat pull-right" id="submit_news">
تأكيد   Submit </button>
            </div>
        </div>
       </div>
        <hr>
    </div>
  </form>
  <hr>
</div>

<script type="text/javascript">

  $(document).on('change','#en_title',function(){
      var slug=$('#en_slug').val();
      var title=$('#en_title').val();
        $('#en_slug').val(title);
  });
   $(document).on('change','#ar_title',function(){
      var slug=$('#ar_slug').val();
      var title=$('#ar_title').val();
        $('#ar_slug').val(title);
    
  });

  $(document).ready(function(){
    $('.dropify-wrapper').css('height','35px');
  });

</script>
<section class="content" style="padding: 0px 5px">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="border-top:none;">
            <!-- /.box-header -->
            <div class="box-body">
              <div>
                <table  class="table  table-bordered table-hover" id="table-{{$tablename}}">
                <thead>
                <tr>
                  @if($lang=='ar')
                  <th> عنوان   </th>
                  <th> الفئة   </th>
                  <th> الحالة  </th>
                  <th> عمل </th>
                  <th> عمل </th>
                  @else
                  <th>Email</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
               </div>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
      <script>
        var tableid =  "{{$tablename}}";
        var type = "{{$page['type']}}";
        
        FormAction({
          formname:"#news_form-"+tableid,
          fid:tableid,
          datatable:true,
          type:type,
          selectjs:[{
            selector:".category-"+tableid,
            dynamic:true,
            placeholder:"Select Role",
            url:APP_URL+"/admin/user_roll?content="+type+"&category=all",
          }],
          geturl:$("[name=get_url]").val(),
          toggle_name:["status"],
          tableurl:"{{url('/admin')}}/cms_content_table?type="+type+"&lang={{$lang}}",
          tablecol:{!!$table!!},
          });
       $('<div class="lang">' +
    '<select class="form-control" id="egname" name="lang">' +
    '<option value="en">English</option>' +
    '<option value="ar"> عربى  </option>' +
    '</select>' +
    '</div>').appendTo('.dataTables_filter');  

    @if($lang)
        $('select[id="egname"]').val('{{$lang}}')
    @endif
    
</script>

<script type="text/javascript">
$(document).ready(function(){
$('.dropify-wrapper').css('height','35px!important');
});

$("#egname").on("change", function(event){
var category = $('select[id="egname"]').val();
$.ajax({
url: APP_URL+"/admin/cms_view?type="+type+"&lang="+category,
dataType : 'html',
success:function(data){
$('.'+type).html(data);
}
})
});            
</script>   