
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">
      <!-- <i class="fa fa-mobile"></i>  -->
      Payments
    </h3>
  </div>
  <!-- /.box-header -->

  <div class="box-body">

    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#paypal" data-toggle="tab">Paypal</a></li>
        <li><a href="#stripe" data-toggle="tab">Stripe</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="paypal">
          <form id="payment-paypalForm">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <div class="col-lg-4 col-md-4">
                    <label for="pusername">Paypal User Name </label>
                  </div>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="paypal_user_name" value="" placeholder="Paypal User Name" required id="pusername">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-4 col-md-4">
                    <label for="ppassword">Paypal Password </label>
                  </div>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="paypal_password" value="" placeholder="Paypal Password" required id="ppassword">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-4 col-md-4">
                    <label for="psignature">Paypal Signature </label>
                  </div>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="paypal_signature" value="" placeholder="Paypal Signature" required id="psignature">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-12 col-md-12 pull-right">
                    <button type="submit" class="btn btn-default bg-btn bg-grey save-btn">Save</button>
                  </div>
                </div>

              </div>
              <div class="col-md-4 text-center">
                <a href="https://www.paypal.com/in/home" target="_blank">
                  <img src="{{ asset('/images/paypal.png')}}"><p>https://www.paypal.com</p>
                </a>
              </div>
            </div>
          </form>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="stripe">
          <form id="payment-stripeForm">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <div class="col-lg-4 col-md-4">
                    <label for="s-apikey">Stripe API Secret Key </label>
                  </div>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="stripe_api_key" value="" placeholder="Stripe API Secret Key " required id="s-apikey">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-4 col-md-4">
                    <label for="s-publishkey">Stripe Publishable Key  </label>
                  </div>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="stripe_publish_key" value="" placeholder="Stripe Publishable key" required id="s-publishkey">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-12 col-md-12 pull-right">
                    <button type="submit" class="btn btn-default bg-btn bg-grey save-btn">Save</button>
                  </div>
                </div>

              </div>
              <div class="col-md-4 text-center">
                <a href="https://stripe.com/" target="_blank">
                  <img src="{{ asset('/images/stripe.png')}}"><p>https://stripe.com</p>
                </a>
              </div>
            </div>
          </form>
        </div>

      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->


  </div>
  <!-- ./box-body -->
</div>

