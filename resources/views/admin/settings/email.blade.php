
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Email</h3>
  </div>
  <!-- /.box-header -->

  <div class="box-body">
    <div class="row">

      <form id="email-form" class="fl w100">
        <div class="col-md-6">
          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="fmail">From Mail Address </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="text" class="form-control" name="fmail" value="" placeholder="From Mail Address" id="fmail">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="fname">From Name </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="text" class="form-control" name="fname" value="" placeholder="From Name" id="fname">
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="email_host">Email Host </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="text" name="email_host" placeholder="Host" class="form-control" >
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="user_name">User Name </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="text" name="user_name" placeholder="User Name" class="form-control" id="user_name">
            </div>
          </div>
        </div>

        <div class="col-md-6">

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="password">Password </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="password" name="password" placeholder="Password" class="form-control" value="">
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="port">Port </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="text" name="port" placeholder="Port" class="form-control" value="">
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="security_type">Security Type </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="text" name="security_type" placeholder="Security Type" class="form-control" value="">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <div class="col-lg-12 col-md-12 pull-right">
              <button type="submit" class="btn btn-default bg-btn bg-grey save-btn">Save</button>
            </div>
          </div>
        </div>

      </form>

  </div>
</div>
<!-- ./box-body -->
</div>