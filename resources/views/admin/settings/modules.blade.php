
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Modules</h3>
  </div>
  <!-- /.box-header -->

  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="" class="table no-margin settings_module">
            <thead>
              <tr>
                <th>Module Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><a href="#">Blogs</a></td>
                <td><label class="switch"><input type="checkbox" id="" class="module_status" checked><span class="slider round"></span></label></td>
              </tr>
              <tr>
                <td><a href="#">Events</a></td>
                <td><label class="switch"><input type="checkbox" id="" class="module_status" checked><span class="slider round"></span></label></td>              
              </tr>
               <tr>
                <td><a href="#">News</a></td>
                <td><label class="switch"><input type="checkbox" id="" class="module_status" checked><span class="slider round"></span></label></td>              
              </tr>
               <tr>
                <td><a href="#">Testimonials</a></td>
                <td><label class="switch"><input type="checkbox" id="" class="module_status" checked><span class="slider round"></span></label></td>              
              </tr>
               <tr>
                <td><a href="#">FAQ</a></td>
                <td><label class="switch"><input type="checkbox" id="" class="module_status" checked><span class="slider round"></span></label></td>              
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- ./box-body -->
</div>