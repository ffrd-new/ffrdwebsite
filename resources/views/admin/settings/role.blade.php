
<div class="box">
  <div class="box-header with-border">
    <div class="row">
      <div class="col-md-10">
        <h3 class="box-title">Role Permissions</h3>
      </div>

    </div>
  </div>
  <!-- /.box-header -->


  <div class="box-body">


    <div class="clearfix"></div>

    <div class="nav-tabs-custom role-tab">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#role_list" data-toggle="tab">Role List</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="role_list">

          <div class="row">
            <div class="col-md-10">
              <h4>Roles</h4>
            </div>
            <div class="col-md-2">
              <button type="button" class="btn bg-grey btn-flat pull-right add"  toggle_form="#add_role">Add Role</button>
            </div>
          </div>

          <div id="add_role" class="hide_form" data_heading="Add Role">

            <form id="news_form">
              <div class="row">
                <div class="form-group">
                  <div class="col-lg-2 col-md-2">
                    <label for="rolename"> Name <span>*</span></label>
                  </div>
                  <div class="col-lg-10 col-md-10">
                    <input type="text" class="form-control" name="rolename" value="" placeholder="Role Name" required id="rolename">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-12 col-md-12">
                    <button type="submit" class="btn bg-grey bg-btn btn-flat pull-right" id="submit_news">Submit</button>
                  </div>
                </div>
              </div>
            </form>
            <hr>
          </div>


          <table id="Table" class="table table-bordered">
            <thead>
              <tr>
                <th>Role</th>
                <th>Type</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="news_list">
              <tr>
                <td>Admin</td>
                <td>System</td>
                <td><a href="{{url('/admin/settings/rolepermission')}}"><i class="fa fa-tag" aria-hidden="true" title="" toggle_form="#"></i></a> | <i class="fa fa-pencil edit_role edit" aria-hidden="true" title="" toggle_form="#add_role"></i> | <i class="fa fa-trash-o delete_role" aria-hidden="true" title=""></i></td>
              </tr>

              <tr>
                <td>Teacher</td>
                <td>System</td>
                <td><a href="{{url('/admin/settings/rolepermission')}}"><i class="fa fa-tag" aria-hidden="true" title="" toggle_form="#"></i></a> | <i class="fa fa-pencil edit_role edit" aria-hidden="true" title="" toggle_form="#add_role"></i> | <i class="fa fa-trash-o delete_role" aria-hidden="true" title=""></i></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.tab-pane -->

        <!-- /.tab-content -->
      </div>
    </div>
    <!-- ./box-body -->
  </div>
</div>