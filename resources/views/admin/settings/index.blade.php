@extends('adminlte::page')

@section('title', 'Settings')

@section('content_header')
<h1><i class="fa fa-cog" aria-hidden="true"></i> Settings</h1>
@stop

@section('content')
<!-- Custom Tabs -->

<div class="nav-tabs-custom settings-tab">
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
    <ul class="nav nav-tabs vertical">
      <li class="active"><a href="#company" data-toggle="tab">Account</a></li>
     <!--  <li><a href="#email" data-toggle="tab">Email</a></li> -->
    </ul>
  </div>

  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
    <div class="tab-content settings-section">
      <div class="tab-pane active" id="company">
          <span class="company"> </span>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="email">
        @include('admin.settings.email')
      </div>

      <div class="tab-pane" id="modules">
        @include('admin.settings.modules')
      </div>

      <div class="tab-pane" id="role">
        @include('admin.settings.role')
      </div>

      <div class="tab-pane" id="payments">
        @include('admin.settings.payments')
      </div>

  </div>
  <!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->
</div>
<!-- /.col -->
@stop
@section('adminlte_js')
<script>
    //

    $(document).ready(function(){

      var flag = 0;

      //Toggle
      $('.hide_form').hide();
      $('.add').click(function(){
        var form = $(this).attr('toggle_form');
        $('body').find(form).toggle('slow');
      });

      $('.edit').click(function(){
        var form = $(this).attr('toggle_form');
        $('body').find(form).toggle('slow');
      });
      
      //Select2
      $('.select2').select2();

      //Dropify
      $('.dropify').dropify();

      //Datatable
      $('#myTable').DataTable();
    });

  </script>
  @stop