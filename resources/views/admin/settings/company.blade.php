
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Company</h3>
  </div>
  <!-- /.box-header -->

  <div class="box-body">
    <div class="row">

      <form id="company-form" class="fl w100">
        <div class="col-md-6">
          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="company">Name <span>*</span></label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="text" class="form-control" name="company" value="" placeholder="Name" required id="company">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="title">Title <span>*</span></label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="text" class="form-control" name="title" value="" placeholder="Title" required id="title">
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="title">Logo <span>*</span></label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="file" class="dropify" data-min-width="400" name="l_image" id="l_image" />
            </div>
          </div>


        </div>
        <div class="col-md-6">

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="address">Address </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <textarea class="form-control" name="address" value="" placeholder="Address" id="address"></textarea>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="contact">Contact </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="tel" name="contact" placeholder="Contact Number" class="form-control" value="">
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-4 col-md-4">
              <label for="contact">Email </label>
            </div>
            <div class="col-lg-8 col-md-8">
              <input type="email" name="email" placeholder="Email" class="form-control" value="">
            </div>
          </div>



        </div>

        <div class="col-md-12">
         <div class="form-group">
          <div class="col-lg-12 col-md-12 pull-right">
            <button type="submit" class="btn btn-default bg-grey bg-btn save-btn">Save</button>
          </div>
        </div>
      </div>
    </form>




  </div>
</div>
<!-- ./box-body -->
</div>

