@extends('adminlte::page')

@section('title', 'Role Permission')

@section('content_header')
<h1><i class="fa fa-cog" aria-hidden="true"></i> Role Permission</h1>
@stop

@section('content')


<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Assign Permission</h3>
  </div>
  <!-- /.box-header -->

  <div class="box-body">
    <table id="Table" class="table no-margin">
      <thead>
        <tr>
          <th>Module</th>
          <th>Feature</th>
          <th>View</th>
          <th>Add</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody id="permission_list">
        <tr>
          <th>Blog</th>
          <td>Blog</td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
        </tr>

        <tr>
          <th></th>
          <td>Blog Category</td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
        </tr>

        <tr>
          <th></th>
          <td>Video Blogs</td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
        </tr>

        <tr>
          <th></th>
          <td>Video Blog Category</td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
        </tr>

        <tr>
          <th>News</th>
          <td>News List</td>
          <td><label class="permission_check">
            <input type="checkbox">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
        </tr>

        <tr>
          <th></th>
          <td>News Category</td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox">
            <span class="checkmark"></span>
          </label></td>
          <td><label class="permission_check">
            <input type="checkbox" checked="checked">
            <span class="checkmark"></span>
          </label></td>
        </tr>

      </tbody>
    </table>
  </div>
  <!-- ./box-body -->
</div>

@endsection