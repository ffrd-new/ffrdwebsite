@extends('adminlte::page')

@section('title', 'Publications')

@section('content_header')
    <h1><i class="fa fa-file-o" aria-hidden="true"></i> Publications الأخبار </h1>
@stop

@section('content')
@php
$lang = 'en';
@endphp
   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#publications_category" data-toggle="tab">Publications Category فئة الأخبار</a></li>
              <li><a href="#publications" data-toggle="tab">Publications الأخبار </a></li>
            </ul>
            <div class="tab-content" style="padding:0px;">
              <div class="tab-pane" id="publications">
               <span class="publications"></span>
              </div>
    
              <div class="tab-pane active" id="publications_category">
                <span class="publications_category"></span>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
@stop
@section('adminlte_js')
  <script>
      _blog_cms_page('{{$lang}}')
  </script>
@stop