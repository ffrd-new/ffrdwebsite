<div class="row">
  <div class="box-body">
    <div class="col-md-12">
      <h4>Resume Listing</h4>
    </div>
  </div>
</div>

<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
  <div class="box-body">
    <table id="Table" class="table table-bordered">
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Job Applied For</th>
          <th><i class="fa fa-bars" aria-hidden="true"></i></th>
        </tr>
      </thead>
      <tbody id="career_list">
        <tr>
          <td>1</td>
          <td>Lorem ipsum</td>
          <td>9876543210</td>
          <td>webdeveloper_02@latlontechnologies.com</td>
          <td>Lorem ipsum</td>
          <td><i class="fa fa-pencil edit_job_application edit" aria-hidden="true" title="" data-toggle="modal" data-target="#viewResume"></i> | <i class="fa fa-trash-o delete_job_application" aria-hidden="true" title=""></i></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<!-- Trigger the modal with a button -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

<!-- Modal -->
<div id="viewResume" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resume</h4>
      </div>
      <div class="modal-body">
        <form class="" method="post" enctype="multipart/form-data" id="">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
             <div class="form-group">
              <label for="fname" class="col-lg-4 col-md-4 col-sm-4">First Name:</label>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <input type="text" name="fname" class="form-control" id="fname">
              </div>
            </div>

            <div class="form-group">
              <label for="email" class="col-lg-4 col-md-4 col-sm-4">Email:</label>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <input type="email" name="email" class="form-control" id="email">
              </div>
            </div>  

            <div class="form-group">
              <label for="experience" class="col-lg-4 col-md-4 col-sm-4">Year of Experience:</label>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <input type="text" name="experience" class="form-control" id="experience">
              </div>
            </div>  
            <div class="form-group">
              <label for="job_applied" class="col-lg-4 col-md-4 col-sm-4">Resume:</label>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <a href="" download>Download</a>
              </div>
            </div>  

          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="lname" class="col-lg-4 col-md-4 col-sm-4">Last Name:</label>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <input type="text" name="lname" class="form-control" id="lname">
              </div>
            </div>

            <div class="form-group">
              <label for="phone" class="col-lg-4 col-md-4 col-sm-4">Phone:</label>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <input type="tel" name="phone" class="form-control" id="phone">
              </div>
            </div>

            <div class="form-group">
              <label for="job_applied" class="col-lg-4 col-md-4 col-sm-4">Job Applied For:</label>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <input type="text" name="job_applied" class="form-control" id="job_applied">
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>