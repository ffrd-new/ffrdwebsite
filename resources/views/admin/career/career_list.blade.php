<div class="row">
  <div class="box-body">
    <div class="col-md-10">
      <h4>Career List</h4>
    </div>
    <div class="col-md-2">
      <button type="button" class="btn bg-grey btn-flat pull-right add"  toggle_form="#add_events">Add List</button>
    </div>
  </div>
</div>
<div id="add_events" class="hide_form">
  <form id="events_form">
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="title">Title <span>*</span></label>
            <input type="text" class="form-control" name="title" placeholder="Title" required id="title">
          </div>
          <div class="form-group">
            <label for="description">Description <span>*</span></label>
            <textarea name="description" class="form-control" placeholder="Description" id="description" required></textarea>
          </div>
        </div>
        <div class="col-md-6">


          <div class="form-group">
            <label for="status">Status</label><br>
            <input type="checkbox" checked data-toggle="toggle" id="status" name="status">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">

          <div class="form-group">
            <button type="submit" class="btn bg-grey btn-flat pull-right">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <hr>
</div>
<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
 <div class="box-body">
  <table id="Table" class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Status</th>
        <th>Publish on</th>
        <th><i class="fa fa-bars" aria-hidden="true"></i></th>
      </tr>
    </thead>
    <tbody id="career_list">
      <tr>
        <td>1</td>
        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
        <td>Published</td>
        <td>11/3/19</td>
        <td><i class="fa fa-pencil edit_job edit" aria-hidden="true" title="" toggle_form="#add_events"></i> | <i class="fa fa-trash-o delete_job" aria-hidden="true" title=""></i></td>
      </tr>
    </tbody>
  </table>
</div>
</div>