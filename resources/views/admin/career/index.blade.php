@extends('adminlte::page')

@section('title', 'Dashboard | Career')

@section('content_header')
    <h1><i class="fa fa-tasks" aria-hidden="true"></i> Career</h1>
@stop

@section('content')
	 <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#career" data-toggle="tab">Career List</a></li>
              <li><a href="#career_types" data-toggle="tab">Career Types</a></li>
              <li><a href="#manage_resume" data-toggle="tab">Manage Resume</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="career">
                @include('admin.career.career_list')
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="career_types">
                @include('admin.career.career_type')
              </div>

              <div class="tab-pane" id="manage_resume">
                @include('admin.career.manage_resume')
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
@stop
@section('adminlte_js')
  <script>
    //
      
    $(document).ready(function(){

      var flag = 0;
      //Toggle
      $('.hide_form').hide();
      $('.add').click(function(){
        var form = $(this).attr('toggle_form');
        $('body').find(form).toggle('slow');
      });

       $('.edit').click(function(){
        var form = $(this).attr('toggle_form');
        $('body').find(form).toggle('slow');
      });

      //CKEditor
      // CKEDITOR.replace( 'content' );
     

      //Select2
      $('.select2').select2();

      //Dropify
      $('.dropify').dropify();

      //Datatable
      $('#myTable').DataTable();
    });

  </script>
@stop