FCMC Request Form Details:

<table width='500' cellspacing='1' bgcolor='#999999' cellpadding='5' border='0'>
	<tr bgcolor='#ffffff'>
		<td align='center' colspan='2'><strong>Name</strong></td>
		<td align='center' colspan='2'>{{$name}}</td>
	</tr>
	<tr bgcolor='#ffffff'>
		<td align='center' colspan='2'><strong>Email</strong></td>
		<td align='center' colspan='2'>{{$email}}</td>
	</tr>
	<tr bgcolor='#ffffff'>
		<td align='center' colspan='2'><strong>Phone</strong></td>
		<td align='center' colspan='2'>{{$phone}}</td>
	</tr>
	<tr bgcolor='#ffffff'>
		<td align='center' colspan='2'><strong>Message</strong></td>
		<td align='center' colspan='2'>{{$bodyMessage}}</td>
	</tr>
</table>