        <!-- Main content -->
    <section class="content" style="padding: 0px 5px">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="border-top:none;">
     
    
            <!-- /.box-header -->
            <div class="box-body">
                  <table  class="table  table-bordered table-hover" id="table-{{$tablename}}">
                <thead>
                <tr>
                  @foreach($heading as $head)
                  <th>{{ucfirst($head)}}</th>
                  @endforeach
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

