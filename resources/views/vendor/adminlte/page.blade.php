@extends('adminlte::master')

@section('adminlte_css')
<link rel="stylesheet"
href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
@stack('css')
@yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
'boxed' => 'layout-boxed',
'fixed' => 'fixed',
'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        @if(config('adminlte.layout') == 'top-nav')
        <nav class="navbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                        <img src="{{asset('/images/logo-white.png')}}" alt="">
                    </a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
                @else
                <!-- Logo -->
                <a href="{{ url('/admin/dashboard') }}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="{{asset('/images/logo-white.png')}}" alt=""></span>
                    <!-- logo for regular state and mobile devices -->
                    <!-- <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span> -->

                   <!--  <span class="logo-lg"><img src="{{asset('/images/menu-logo.png')}}" alt=""></span> -->
                </a>

                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">

                    <!-- Sidebar toggle button-->
                   <!--  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                    </a> -->
                    @endif
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu admin-topmenu">

                        <ul class="nav navbar-nav">

                            <!-- <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                  <i class="fa fa-envelope-o"></i>
                                  <span class="label label-success">4</span>
                                </a>
                                <ul class="dropdown-menu">
                                  <li class="header">You have 4 messages</li>
                                  <li> -->
                                    <!-- inner menu: contains the actual data -->
                                   <!--  <ul class="menu content scroll-drop-menu">
                                        <li> --><!-- start message -->
                                            <!-- <a href="#">
                                                <div class="pull-left">
                                                    <img src="{{asset('images/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li> -->
                                        <!-- end message -->
                                        <!-- <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="{{asset('images/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    AdminLTE Design Team
                                                    <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="{{asset('images/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    Developers
                                                    <small><i class="fa fa-clock-o"></i> Today</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                              <div class="pull-left">
                                                <img src="{{asset('images/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                            </div>
                                            <h4>
                                                Sales Department
                                                <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="{{asset('images/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                            </div>
                                            <h4>
                                                Reviewers
                                                <small><i class="fa fa-clock-o"></i> 2 days</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li> -->

<!-- 
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                          <i class="fa fa-bell-o"></i>
                          <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li> -->
                            <!-- inner menu: contains the actual data -->
                            <!-- <ul class="menu content scroll-drop-menu">
                                <li>
                                    <a href="#">
                                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                    <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                                    page and may cause design problems
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                      <i class="fa fa-users text-red"></i> 5 new members joined
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                      <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
 -->


                <!-- <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="notificationTab"><i class="fa fa-bell-o"></i></a>
                    <div class="dropdown-menu" aria-labelledby="notificationTab">
                        <a class="dropdown-item" href="#">Item</a>
                        <a class="dropdown-item" href="#">Another item</a>
                        <a class="dropdown-item" href="#">One more item</a>
                    </div>
                </li>

                <li>
                    <a href="" class="dropdown-toggle" data-toggle="dropdown" id="messagestab"><i class="fa fa-envelope-o"></i></a>

                    <div class="dropdown-menu" aria-labelledby="messagestab">
                        <a class="dropdown-item" href="#">Item</a>
                        <a class="dropdown-item" href="#">Another item</a>
                        <a class="dropdown-item" href="#">One more item</a>
                    </div>
                </li>
            -->
<li class="mail-content"> <p> <a href=" https://gmail.com" target="_blank"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></p></li>

<li>
    <a href="{{'logot'}}"  >
    <i class="fa fa-fw fa-power-off">
    </i>Logout
</a>
</li>
</ul>
</div>
@if(config('adminlte.layout') == 'top-nav')
</div>
@endif
</nav>
</header>

@if(config('adminlte.layout') != 'top-nav')
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <img src="{{asset('/images/logo-white.png')}}" alt="" width="100%">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
@endif

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    @if(config('adminlte.layout') == 'top-nav')
    <div class="container">
        @endif

        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('content_header')
        </section>

        <!-- Main content -->
        <section class="content">
@include('errors.list')
            @yield('content')

        </section>
        <!-- /.content -->
        @if(config('adminlte.layout') == 'top-nav')
    </div>
    <!-- /.container -->
    @endif
</div>
<!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->
@stop

@section('adminlte_js')
<script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
@stack('js')
@yield('js')


@stop
