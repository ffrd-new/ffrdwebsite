<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Type extends Model
{

	  protected $table = 'type';
	  protected $fillable = [
        'id','name', 'description', 'created_at','updated_at'
    ];
}