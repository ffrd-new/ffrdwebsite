<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    //
    public $timestamps = false;
           protected $table = 'content_meta';

    public function Content()
    {
        return $this->belongsTo('App\Content');
    }

}
