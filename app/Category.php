<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    //


       protected $table = 'content_category';



    public function category()
    {
        return $this->hasMany('App\Content');
    }


       public function meta()
    {
        return $this->belongsTo('App\Meta');
    }

     public static function FCategory($id){
                     $data = DB::table("content_category")
                     ->join('content_detail', 'content_detail.category_id', '=', 'content_category.id')
                    ->select("content_category.id","content_category.en_name","content_category.ar_name",DB::raw('count(content_detail.category_id) as count'))
                    ->where('content_category.type_id',$id)
                    ->where('content_category.status',1)
                    ->where('content_detail.status',1)
                    ->whereRaw('content_detail.category_id = (select max(`category_id`) from content_detail where `category_id` = content_category.id and status = 1)')->GroupBy('content_detail.category_id')
                    ->get();
                    return $data;
    }

    public static function FCate($id){
                     $data = DB::table("content_category")
                     ->join('content_detail', 'content_detail.category_id', '=', 'content_category.id')
                    ->select("content_category.id","content_category.en_name","content_category.ar_name","content_category.type_id",DB::raw('count(content_detail.category_id) as count'))
                   
                    ->where(function($query) use ($id){
                        if($id){
                            $query->where ('content_category.type_id',$id);
                        }
                    })

                    ->where('content_category.status',1)
                    ->where('content_detail.status',1)
                    ->whereRaw('content_detail.category_id = (select max(`category_id`) from content_detail where `category_id` = content_category.id and status = 1)')->GroupBy('content_detail.category_id')
                    ->get();
                    return $data;
    }

    public static function typeName($id){
        $data = DB::table("type")
           
           ->where(function($query) use ($id){
                if($id){
                      $query->where('id',$id);
                }
            })

            ->select('id','name')
            ->get();
             return $data;
    }

    public static function FCatList($id){

        $data = DB::table("content_category as cc")
                ->leftjoin('content_detail as cd', 'cd.category_id', '=', 'cc.id')
                ->leftjoin('content_meta as cm', 'cd.id', '=', 'cm.content_id')
                ->select("cc.id","cc.en_name","cc.ar_name","cc.type_id",DB::raw('count(cd.category_id) as count'))
                ->where(function($query) use ($id) {
                if($id == 10){
                    $query->where('cm.pdf', 'NOT LIKE', '%[]%');
                }
                }
                )
                ->where('cc.type_id',$id)
                ->where('cc.status',1)
                ->where('cd.status',1)
                ->GroupBy('cc.en_name')
                ->get();
        return $data;        
    } 

    public static function FGalleryCatList($id){

        $data = DB::table("content_category as cc")
                ->leftjoin('content_detail as cd', 'cd.category_id', '=', 'cc.id')
                ->leftjoin('content_meta as cm', 'cd.id', '=', 'cm.content_id')
                ->select("cc.id","cc.en_name","cc.ar_name","cc.type_id",DB::raw('count(cd.category_id) as count'))
                ->where('cc.type_id',$id)
                ->where(function($query) use ($id) {
                if($id == 8 && 'cd.gallery_type'== 1){
                    $query->where('cm.gallery', 'NOT LIKE', '%[]%');
                }
                }
                )
                ->where('cc.status',1)
                ->where('cd.status',1)
                ->where('cd.gallery_type',1)
                ->GroupBy('cc.en_name')
                ->get();
        return $data;        
    } 

    public static function FVideoGalleryCatList($id){

        $data = DB::table("content_category as cc")
                ->leftjoin('content_detail as cd', 'cd.category_id', '=', 'cc.id')
                ->select("cc.id","cc.en_name","cc.ar_name","cc.type_id",DB::raw('count(cd.category_id) as count'))
                ->where('cc.type_id',$id)
                ->where('cc.status',1)
                ->where('cd.status',1)
                ->where('cd.gallery_type',2)
                ->GroupBy('cc.en_name')
                ->get();
        return $data;        
    }
    


    public static function gallerycategory($type){
        $data=Category::where('status',1)
        ->where('is_delete',0)
        ->where('type_id',$type)
        ->select('*')
        ->get();

        return $data;
    }

    public static function getDetailNewsGallery($newsgal_id,$id){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->where('content_detail.id',$newsgal_id)
        ->where('m.gallery','NOT LIKE', '%[]%')
        ->where('content_detail.gallery_type',1)
        ->orderBy('content_detail.publish_date', 'DESC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.gallery','m.image_date')
        ->get();
        return $data;
    }

    public static function getDetailNewsVideoGallery($newsgal_id,$id){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->where('content_detail.id',$newsgal_id)
        ->where('content_detail.gallery_type',2)
        ->orderBy('content_detail.publish_date', 'DESC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.gallery','m.video_url','m.video_date')
        ->get();
        return $data;
    }

    
}


