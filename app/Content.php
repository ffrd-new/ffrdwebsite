<?php

namespace App;
use DB;
use App\Content;
use Config;
use Helper;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    //
    protected $table = 'content_detail';
    public function category()
    {
        return $this->hasMany('App\Category');
    }

     public function meta()
    {
        return $this->belongsTo('App\Meta','content_detail.id','content_id');
    }

    public static function getData($id,$lang,$col,$type){
    
        if($lang == 'en'){
            if($type == 'gallery'){
            $data = Content::where('content_detail.category_type_id',$id)
            ->join('content_meta as m','m.content_id','content_detail.id')
            ->join('content_category as c','c.id','content_detail.category_id')
            ->where('content_detail.gallery_type',1)
            ->OrderBy('content_detail.category_id')
            ->get($col);
            }
            elseif($type == 'video_gallery'){
              $data = Content::where('content_detail.category_type_id',$id)
            ->join('content_meta as m','m.content_id','content_detail.id')
            ->join('content_category as c','c.id','content_detail.category_id')
            ->where('content_detail.gallery_type',2)
            ->OrderBy('content_detail.category_id')
            ->get($col);
            }elseif ($type == 'ruler') {
              $data = Content::where('content_detail.category_type_id',$id)
            ->join('content_meta as m','m.content_id','content_detail.id')
            ->join('content_category as c','c.id','content_detail.category_id')
            ->OrderBy('content_detail.category_id')
            ->get($col);   
            }
            else{
            $data = Content::where('content_detail.category_type_id',$id)
            ->join('content_meta as m','m.content_id','content_detail.id')
            ->join('content_category as c','c.id','content_detail.category_id')
            ->OrderBy('content_detail.category_id')
            ->get($col);
            }


        }else
        {
             if($type == 'gallery'){
            $data = Content::where('content_detail.category_type_id',$id)
            ->join('content_meta as m','m.content_id','content_detail.id')
            ->join('content_category as c','c.id','content_detail.category_id')
            ->where('content_detail.gallery_type',1)
            ->OrderBy('content_detail.category_id')
            ->get($col);
            }
            elseif($type == 'video_gallery'){
              $data = Content::where('content_detail.category_type_id',$id)
            ->join('content_meta as m','m.content_id','content_detail.id')
            ->join('content_category as c','c.id','content_detail.category_id')
            ->where('content_detail.gallery_type',2)
            ->OrderBy('content_detail.category_id')
            ->get($col);
            }elseif ($type == 'ruler') {
              $data = Content::where('content_detail.category_type_id',$id)
            ->join('content_meta as m','m.content_id','content_detail.id')
            ->join('content_category as c','c.id','content_detail.category_id')
            ->OrderBy('content_detail.category_id')
            ->get($col);   
            }else{
                 $data = Content::where('content_detail.category_type_id',$id)
            ->join('content_meta as m','m.content_id','content_detail.id')
            ->join('content_category as c','c.id','content_detail.category_id')
            ->OrderBy('content_detail.category_id')
            ->get($col);
            }  
        }
        return $data;
    }


     public static function getContentx($id,$col){
        $data = Content::where('content_detail.id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->first($col);
        return $data;
    }

     public static function getContent($slug){
      
        $data = Content::join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('content_detail.id',$slug)
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.content_id','m.id as meta_id','m.event_date','m.en_location','m.ar_location','m.pdf','m.news_date','m.pubcurl','content_detail.news_gallery_id','content_detail.news_gallery_name','content_detail.news_video_id','content_detail.news_video_name')
        ->first();
        return $data;
    }

    public static function getPublicaContent($slug){
        $data = Content::join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('content_detail.id',$slug)
        // ->where('m.pdf','NOT LIKE', '%[]%')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.content_id','m.id as meta_id','m.event_date','m.en_location','m.ar_location','m.pdf','m.pubcurl','m.news_date','m.flipname','m.publication_date')
        ->first();
        return $data;
    }  

     public static function getallblog($id,$limit){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.event_date','m.en_location','m.ar_location','m.en_meta_keywords','m.ar_meta_keywords','m.pdf','m.pubcurl','m.news_date','m.flipname')
        ->paginate($limit);
        return $data;
    }
    public static function getallblog_evnts($id,$limit){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.event_date','m.en_location','m.ar_location','m.en_meta_keywords','m.ar_meta_keywords','m.pdf','m.pubcurl','m.news_date','m.flipname')->orderBy('m.event_date','DESC')
        ->paginate($limit);
        return $data;
    }
    
    
    public static function getallblog_news($id,$limit){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.event_date','m.en_location','m.ar_location','m.en_meta_keywords','m.ar_meta_keywords','m.pdf','m.pubcurl','m.news_date','m.flipname')->orderBy('m.news_date','DESC')
        ->paginate($limit);
        return $data;
    }

    public static function getpublilist($id,$limit){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        // ->where('m.pdf','NOT LIKE', '%[]%')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.event_date','m.en_location','m.ar_location','m.en_meta_keywords','m.ar_meta_keywords','m.pdf','m.pubcurl','m.news_date','m.flipname','m.publication_date')->orderBy('m.publication_date','DESC')
        ->paginate($limit);
        return $data;
    }

    public static function getgallerylist($id,$limit = 8){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->where('m.gallery','NOT LIKE', '%[]%')
        ->where('content_detail.gallery_type',1)
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.gallery','m.image_date')->orderBy('content_detail.id','DESC')
        ->paginate($limit);
        return $data;
    }
    public static function getgallerylisthome($id,$limit = 6){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->where('m.gallery','NOT LIKE', '%[]%')
        ->where('content_detail.gallery_type',1)
        ->orderBy('content_detail.publish_date', 'DESC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.image_date','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.gallery')
        ->paginate($limit);
        return $data;
    }

    public static function getpublicalist($typeid,$limit = 6){
        $data = Content::where('content_detail.category_type_id',$typeid)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        // ->where('m.pdf','NOT LIKE', '%[]%')
        ->orderBy('content_detail.publish_date', 'DESC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.pdf','m.pubcurl','m.flipname')
        ->paginate($limit);
        return $data;
    } 

    public static function getrulerdetails($typeid,$limit=1){
        $data = Content::where('content_detail.category_type_id',$typeid)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->orderBy('content_detail.publish_date', 'DESC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.pdf','m.pubcurl','m.flipname')
        ->paginate($limit);
        return $data;
    }


    public static function getvideogallerylist($id,$limit = 8){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->where('content_detail.gallery_type',2)
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.gallery','m.video_url','m.video_date')->orderBy('content_detail.id','DESC')
        ->paginate($limit);
        return $data;
    }


     public static function getcategorydetail($id){

        $parentid=DB::table('content_category')
        ->where('id',$id)->select('is_parent')
        ->first();
       if(Config::get('app.locale') == 'en'){
            $data=DB::table('content_category as a')
            ->leftjoin('content_category as b','a.is_parent','b.id')
            ->where('a.is_parent',$parentid->is_parent)
            ->where('a.id',$id)
            ->select('a.id as id','a.en_name as category_name','b.en_name as parent_name')
            ->orderBy('a.created_at','DESC')
            ->first();
       }
       if(Config::get('app.locale') == 'ar'){
         $data=DB::table('content_category as a')
            ->leftjoin('content_category as b','a.is_parent','b.id')
            ->where('a.is_parent',$parentid->is_parent)
            ->where('a.id',$id)
            ->select('a.id as id','a.ar_name as category_name','b.ar_name as parent_name')
            ->orderBy('a.created_at','DESC')
            ->first();
       }
       return $data;
    }


     public static function FListingcategory($id,$cat,$limit=2){
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('content_detail.category_id',$cat)
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->select('content_detail.id','content_detail.en_description','content_detail.image','content_detail.status','content_detail.publish_date as publish on','c.en_name as category','c.id as category_id','content_detail.en_title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc','m.en_meta_keywords')
        ->paginate($limit);
        return $data;
    }

public static function recentNewss($type,$category_id,$id,$col){
    $data = Content::where('content_detail.category_id',$category_id)
                ->join('content_meta as m','m.content_id','content_detail.id')
                ->join('content_category as c','c.id','content_detail.category_id')
                ->where('content_detail.status',1)
                ->where('content_detail.category_type_id',$type)
                ->where('c.status',1)
                ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.news_date')
                ->get();

                if(count($data) >=3){
                $data = Content::where('content_detail.category_id',$category_id)
                ->join('content_meta as m','m.content_id','content_detail.id')
                ->join('content_category as c','c.id','content_detail.category_id')
                ->where('content_detail.status',1)
                ->where('c.status',1)

                ->where('content_detail.category_type_id',$type)
                // ->orderBy('content_detail.created_at', 'DESC')
                ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.news_date')
                ->orderBy('m.news_date','DESC')
                ->get();
                }
                else{
                $data = Content::where('content_detail.status',1)
                ->join('content_meta as m','m.content_id','content_detail.id')
                ->join('content_category as c','c.id','content_detail.category_id')
                ->where('c.status',1)

                 // ->orderBy('content_detail.created_at', 'DESC')
                 ->where('content_detail.category_type_id',$type)
                 ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.news_date')
                  ->orderBy('m.news_date','DESC')
                ->get();
                }
                // var_dump(count($data));
        return $data;
}

    public static function recentblog($type,$category_id,$id,$col){
                $data = Content::where('content_detail.category_id',$category_id)
                ->join('content_meta as m','m.content_id','content_detail.id')
                ->join('content_category as c','c.id','content_detail.category_id')
                ->where('content_detail.status',1)
                ->where('content_detail.category_type_id',$type)
                ->where('c.status',1)
                ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.news_date')
                ->get();

                if(count($data) >=3){
                $data = Content::where('content_detail.category_id',$category_id)
                ->join('content_meta as m','m.content_id','content_detail.id')
                ->join('content_category as c','c.id','content_detail.category_id')
                ->where('content_detail.status',1)
                ->where('c.status',1)

                ->where('content_detail.category_type_id',$type)
                // ->orderBy('content_detail.created_at', 'DESC')
                ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.news_date')
                ->orderBy('content_detail.created_at','DESC')
                ->get();
                }
                else{
                $data = Content::where('content_detail.status',1)
                ->join('content_meta as m','m.content_id','content_detail.id')
                ->join('content_category as c','c.id','content_detail.category_id')
                ->where('c.status',1)

                 // ->orderBy('content_detail.created_at', 'DESC')
                 ->where('content_detail.category_type_id',$type)
                 ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.news_date')
                  ->orderBy('content_detail.created_at','DESC')
                ->get();
                }
                // var_dump(count($data));
        return $data;
    }

    public static function lastevent($category_id){
         $data = Content::where('content_detail.category_type_id',$category_id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('content_detail.status',1)
        ->where('c.status',1)
        ->orderBy('content_detail.publish_date','ASC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.event_date','m.en_location','m.ar_location','m.news_date')
        ->first();
        return $data;
    }

    

    public static function getcontents($cat_typeid){
        $users = DB::table('content_detail')
            ->join('content_meta', 'content_meta.content_id','=','content_detail.id')
            ->join('content_category', 'content_category.id', '=', 'content_detail.category_id')
            ->where('content_detail.status',1)
            ->where('content_category.status',1)
            ->where('content_detail.category_type_id',$cat_typeid)
            ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','content_category.en_name as en_category','content_category.ar_name as ar_category','content_category.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','content_meta.en_meta_title','content_meta.ar_meta_title','content_meta.en_meta_desc','content_meta.ar_meta_desc','content_meta.en_meta_keywords','content_meta.ar_meta_keywords','content_meta.en_location','content_meta.ar_location','content_meta.news_date')
            ->orderBy('content_detail.publish_date','desc')
            ->get();
        return $users; 
    }

    public static function latestNewsHome($cat_typeid){
        $users = DB::table('content_detail')
                ->join('content_meta', 'content_meta.content_id','=','content_detail.id')
                ->join('content_category', 'content_category.id', '=', 'content_detail.category_id')
                ->where('content_detail.status',1)
                ->where('content_category.status',1)
                ->where('content_detail.category_type_id',$cat_typeid)
                //->orderBy('m.event_date','ASC')
                ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','content_category.en_name as en_category','content_category.ar_name as ar_category','content_category.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','content_meta.en_meta_title','content_meta.ar_meta_title','content_meta.en_meta_desc','content_meta.ar_meta_desc','content_meta.en_meta_keywords','content_meta.ar_meta_keywords','content_meta.en_location','content_meta.ar_location','content_meta.news_date')->limit(3)
                ->orderBy('content_meta.news_date','desc')
                ->get();
        return $users; 
    }
    
       public static function latestNewsHomeCaro($cat_typeid){
        $users = DB::table('content_detail')
                ->join('content_meta', 'content_meta.content_id','=','content_detail.id')
                ->join('content_category', 'content_category.id', '=', 'content_detail.category_id')
                ->where('content_detail.status',1)
                ->where('content_category.status',1)
                ->where('content_detail.category_type_id',$cat_typeid)
                //->orderBy('m.event_date','ASC')
                ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','content_category.en_name as en_category','content_category.ar_name as ar_category','content_category.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','content_meta.en_meta_title','content_meta.ar_meta_title','content_meta.en_meta_desc','content_meta.ar_meta_desc','content_meta.en_meta_keywords','content_meta.ar_meta_keywords','content_meta.en_location','content_meta.ar_location','content_meta.news_date')
                ->orderBy('content_meta.news_date','desc')
                ->skip(3)
                ->take(10)
                ->get();
        return $users; 
    }
        public static function getEventByDate($typeid,$date,$lastdate){
       $data = Content::where('content_detail.category_type_id',$typeid)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('content_detail.status',1)
        ->where('c.status',1)
        ->where('m.event_date', '>=', $date)
        //->where('m.event_date', '<=', $lastdate)
        ->orderBy('m.event_date','ASC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description as en_description','content_detail.ar_description as ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.event_date','m.en_location','m.ar_location','m.ar_slug','m.en_slug','m.news_date')
        ->get();
        return $data;
    }

        public static function gcontent($cat_typeid,$limit,$cat_id,$cdate=null){
            if($cat_typeid ==3){
                 $users = DB::table('content_detail')
            ->join('content_meta', 'content_meta.content_id','=','content_detail.id')
            ->join('content_category', 'content_category.id', '=', 'content_detail.category_id')
            ->where('content_detail.status',1)
            ->where('content_category.status',1)
            ->where(function($query) use ($cat_id,$cat_typeid){
              // 
                if($cat_id){
                      $query->where('content_detail.category_id',$cat_id);
                }
                 if($cat_typeid){
                    $query->where('content_detail.category_type_id',$cat_typeid);
                }
            })

            ->orderBy('content_meta.news_date','desc')
            ->orderBy('content_meta.publication_date','desc')
            ->orderBy('content_meta.image_date','desc')
            ->orderBy('content_meta.video_date','desc')
            ->where('content_meta.event_date', '>=', $cdate)

            //->where('content_meta.event_date'>= getdate())
            ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_category.en_name as en_category','content_detail.publish_date as publish_on','content_category.ar_name as ar_category','content_category.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','content_detail.category_type_id','content_detail.gallery_type','content_meta.en_meta_title','content_meta.ar_meta_title','content_meta.event_date as event_on','content_meta.en_location','content_meta.ar_location','content_meta.en_meta_desc','content_meta.ar_meta_desc','content_meta.en_meta_keywords','content_meta.ar_meta_keywords','content_meta.gallery','content_meta.video_url','content_meta.news_date','content_meta.event_date','content_meta.publication_date','content_meta.image_date','content_meta.video_date','content_meta.pubcurl as pdfurl',DB::raw("CONCAT('".url('/pdf')."/', content_meta.flipname) AS flipbook"),DB::raw("CONCAT('".Helper::image_path_base($cat_typeid)."/', content_detail.image) AS base_image"),DB::raw("CONCAT('".Helper::image_path_thumb($cat_typeid)."/', content_detail.image) AS thumb"))->limit($limit)
            ->orderBy('content_meta.event_date','asc')
            ->get();
            }else{


         $users = DB::table('content_detail')
            ->join('content_meta', 'content_meta.content_id','=','content_detail.id')
            ->join('content_category', 'content_category.id', '=', 'content_detail.category_id')
            ->where('content_detail.status',1)
            ->where('content_category.status',1)
            ->where(function($query) use ($cat_id,$cat_typeid){
              // 
                if($cat_id){
                      $query->where('content_detail.category_id',$cat_id);
                }
                 if($cat_typeid){
                    $query->where('content_detail.category_type_id',$cat_typeid);
                }
            })

            ->orderBy('content_meta.news_date','desc')
            ->orderBy('content_meta.publication_date','desc')
            ->orderBy('content_meta.image_date','desc')
            ->orderBy('content_meta.video_date','desc')
            //->where('content_meta.event_date'>= getdate())
            ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_category.en_name as en_category','content_detail.publish_date as publish_on','content_category.ar_name as ar_category','content_category.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','content_detail.category_type_id','content_detail.gallery_type','content_meta.en_meta_title','content_meta.ar_meta_title','content_meta.event_date as event_on','content_meta.en_location','content_meta.ar_location','content_meta.en_meta_desc','content_meta.ar_meta_desc','content_meta.en_meta_keywords','content_meta.ar_meta_keywords','content_meta.gallery','content_meta.video_url','content_meta.news_date','content_meta.event_date','content_meta.publication_date','content_meta.image_date','content_meta.video_date','content_meta.pubcurl as pdfurl',DB::raw("CONCAT('".url('/pdf')."/', content_meta.flipname) AS flipbook"),DB::raw("CONCAT('".Helper::image_path_base($cat_typeid)."/', content_detail.image) AS base_image"),DB::raw("CONCAT('".Helper::image_path_thumb($cat_typeid)."/', content_detail.image) AS thumb"))->limit($limit)
            ->get();
        }
        return $users; 
    }



          public static function gconts($cont_id,$type){
        $users = DB::table('content_detail')
            ->join('content_meta', 'content_meta.content_id','=','content_detail.id')
            ->where('content_detail.status',1)
            ->where('content_detail.id',$cont_id)
            ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.en_title','content_detail.publish_date as publish_on','content_detail.ar_title','content_detail.gallery_type','content_meta.en_meta_title','content_meta.ar_meta_title','content_meta.en_meta_desc','content_meta.ar_meta_desc','content_meta.en_meta_keywords','content_meta.event_date as event_on','content_meta.en_location','content_meta.ar_location','content_meta.ar_meta_keywords','content_meta.gallery','content_meta.pdf','content_meta.en_location','content_meta.ar_location','content_meta.video_url','content_meta.en_slug','content_meta.ar_slug',DB::raw("CONCAT('".Helper::image_path_base($type)."/', content_detail.image) AS base_image"),DB::raw("CONCAT('".Helper::image_path_thumb($type)."/', content_detail.image) AS thumb"))->get();
        return $users; 
    }


     public static function FListing($id)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where('type.id',$id)->where('content_detail.status',1)
        ->select('content_detail.id','content_detail.en_title','content_detail.en_description','content_category.en_name','content_detail.status','content_detail.publish_date','content_detail.category_type_id','content_meta.gallery')->paginate(6);
        return $data;
    }

     public static function FListingpage_en($id,$cat)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where('type.id',$id)
        ->where('content_detail.status',1)
        ->where(function($query) use ($cat) {
        if($cat){
            $query->where('content_detail.category_id', $cat);
        }}
        )
        ->select('content_detail.id','content_detail.image','content_detail.en_title as title','content_detail.en_description as description','content_category.en_name as name','content_detail.status','content_detail.publish_date','content_detail.category_type_id','content_meta.gallery')
        ->get();
        return $data;
    }

     public static function FListingpage_list($id,$cat,$limit)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where('type.id',$id)
        ->where('content_detail.status',1)
        ->where(function($query) use ($cat) {
        if($cat){
            $query->where('content_detail.category_id', $cat);
        }}
        )
        ->select('content_detail.id','content_detail.image','content_detail.ar_title','content_detail.en_title','content_detail.en_description as description','content_category.en_name as name','content_detail.status','content_detail.publish_date as publish_on','content_detail.category_type_id','content_meta.gallery','content_meta.event_date','content_meta.en_location','content_meta.ar_location','content_meta.pdf','content_meta.news_date','content_meta.pubcurl','content_meta.flipname')
        ->orderBy('content_detail.created_at', 'DESC')
        ->paginate($limit);
        return $data;
    }
    public static function FListingpage_list_event($id,$cat,$limit)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where('type.id',$id)
        ->where('content_detail.status',1)
        ->where(function($query) use ($cat) {
        if($cat){
            $query->where('content_detail.category_id', $cat);
        }}
        )
        ->select('content_detail.id','content_detail.image','content_detail.ar_title','content_detail.en_title','content_detail.en_description as description','content_category.en_name as name','content_detail.status','content_detail.publish_date as publish_on','content_detail.category_type_id','content_meta.gallery','content_meta.event_date','content_meta.en_location','content_meta.ar_location','content_meta.pdf','content_meta.news_date','content_meta.pubcurl','content_meta.flipname')
        ->orderBy('content_meta.event_date', 'DESC')
        ->paginate($limit);
        return $data;
    }
    
      public static function FListingpage_publicalist($id,$cat,$limit)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where('type.id',$id)->where('content_detail.status',1)->where(function($query) use ($cat) {
        if($cat){
            $query->where('content_detail.category_id', $cat);
        }}
        )
        ->select('content_detail.id','content_detail.image','content_detail.ar_title','content_detail.en_title','content_detail.en_description','content_detail.ar_description','content_category.en_name as name','content_detail.status','content_detail.publish_date as publish_on','content_detail.category_type_id','content_meta.gallery','content_meta.event_date','content_meta.en_location','content_meta.ar_location','content_meta.pdf','content_meta.pubcurl','content_meta.flipname')
        ->orderBy('content_detail.publish_date', 'DESC')
        ->paginate($limit);
        return $data;
    }

    public static function FListingpage_list_gallery($id,$cat,$limit = 5)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where('type.id',$id)
        ->where('content_detail.status',1)
        ->where('content_meta.gallery','NOT LIKE', '%[]%')
        ->where('content_detail.gallery_type',1)
        ->where(function($query) use ($cat) {
        if($cat){
            $query->where('content_detail.category_id', $cat);
        }}
        )
        ->select('content_detail.id','content_detail.image','content_detail.ar_title','content_detail.en_title','content_detail.en_description as description','content_category.en_name as name','content_detail.status','content_detail.publish_date as publish_on','content_detail.category_type_id','content_meta.gallery')
        ->orderBy('content_detail.id', 'DESC')
        ->paginate($limit);
        return $data;
    }
    public static function FListingpage_list_videogallery($id,$cat,$limit = 8)
    {
        $data = Content::where('content_detail.category_type_id',$id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->where('content_detail.category_id', $cat)
        ->where('content_detail.gallery_type',2)
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.gallery','m.video_url')->orderBy('content_detail.id','desc')
        ->paginate($limit);

        return $data;
    }
   

     public static function FListingpage_ar($id,$cat)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where('type.id',$id)->where('content_detail.status',1)->where(function($query) use ($cat) {
        if($cat){
            $query->where('content_detail.category_id', $cat);
                    //    $query->orwhere('content_detail.category_id', $cat);
        }}
        )
        ->select('content_detail.id','content_detail.ar_title as title','content_detail.ar_description as description','content_category.ar_name as name','content_detail.status','content_detail.publish_date','content_detail.category_type_id','content_meta.gallery')
        ->get();
        return $data;
    }

    public static function getgallery($type,$id)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where('content_detail.id',$id)
        ->where('content_detail.category_type_id',$type)
        ->select('content_detail.id','content_detail.en_title','content_detail.en_description','content_category.en_name','content_detail.status','content_detail.publish_date','content_detail.category_type_id','content_meta.gallery')
        ->first();

        return $data;

    }

    public static function latestevent($category_id){
         $data = Content::where('content_detail.category_type_id',$category_id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('content_detail.status',1)
        ->where('c.status',1)
        ->orderBy('content_detail.publish_date','ASC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description as en_description','content_detail.ar_description as ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords')
        ->get();
        return $data;
    }
     public static function latesteventhome($category_id){
         $data = Content::where('content_detail.category_type_id',$category_id)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('content_detail.status',1)
        ->where('c.status',1)
        ->orderBy('content_detail.publish_date','DESC')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description as en_description','content_detail.ar_description as ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.event_date','m.en_location','m.ar_location','m.ar_slug','m.en_slug','m.news_date','m.event_date_on')
        ->get();
        return $data;
    }


    public static function getfirstimage($type,$category_id){
        $data = Content::where('content_detail.category_type_id',$type)
        ->join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->where('c.id',$category_id)
        ->select('m.gallery')
        ->get();

        return $data;
    }

   

   // *********************************
     public static function FListingpageApi($id,$cat)
    {
        $data = DB::table('content_detail')
        ->join('type', 'content_detail.category_type_id', '=', 'type.id')
        ->join('content_category', 'content_detail.category_id', '=', 'content_category.id')
        ->leftJoin('content_meta', 'content_meta.content_id', '=', 'content_detail.id')
        ->where(function($query) use ($id) {
        if($id){
            $query->where('type.id', $id);
        }})
        ->where('content_detail.status',1)->where(function($query) use ($cat) {
        if($cat){
            $query->where('content_detail.category_id', $cat);
        }}
        )
        ->select('content_detail.id','content_detail.image','content_detail.en_title as title','content_detail.en_description as description','content_category.en_name as name','content_detail.status','content_detail.publish_date','content_detail.category_type_id','content_meta.gallery')
        ->get();
        return $data;
    }


    public static function getSearch($keyword){
        $data = Content::join('content_meta as m','m.content_id','content_detail.id')
        ->join('content_category as c','c.id','content_detail.category_id')
        ->join('type','type.id','c.type_id')
        ->where('c.status',1)
        ->where('content_detail.status',1)
        ->where('content_detail.en_title','like', '%'.$keyword.'%')
        ->orWhere('content_detail.ar_title', 'like', '%' . $keyword . '%')
        ->orWhere('c.en_name', 'like', '%' . $keyword . '%')
        ->orWhere('c.ar_name', 'like', '%' . $keyword . '%')
        ->orWhere('m.en_meta_title', 'like', '%' . $keyword . '%')
        ->orWhere('m.ar_meta_title', 'like', '%' . $keyword . '%')
        ->orWhere('m.en_location', 'like', '%' . $keyword . '%')
        ->orWhere('m.ar_location', 'like', '%' . $keyword . '%')
        ->orWhere('m.en_meta_desc', 'like', '%' . $keyword . '%')
        ->orWhere('m.ar_meta_desc', 'like', '%' . $keyword . '%')
        ->orWhere('m.en_meta_keywords', 'like', '%' . $keyword . '%')
        ->orWhere('m.ar_meta_keywords', 'like', '%' . $keyword . '%')
        ->orWhere('content_detail.en_description', 'like', '%' . $keyword . '%')
        ->orWhere('content_detail.ar_description', 'like', '%' . $keyword . '%')
        ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as en_category','c.ar_name as ar_category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','m.en_meta_title','m.ar_meta_title','m.en_meta_desc','m.ar_meta_desc','m.en_meta_keywords','m.ar_meta_keywords','m.content_id','m.id as meta_id','m.event_date','m.en_location','m.ar_location','m.pdf','c.type_id','type.name','m.gallery','m.news_date','m.pubcurl','m.flipname')
        //->groupBy('type.name')
        ->get();
        return $data;
    }

// 
     public static function getNameGalry($id){
    return $data = Content::select(DB::raw('CONCAT(en_title, " ",ar_title) AS full_name'), 'id')
                        ->where('content_detail.id',$id)
                        ->first();
    }

    public static function getImageforJq($id){
        return $data = Content::select('image')->where('content_detail.id',$id)->first();
    }
         public static function latestNewsBYDate($cat_typeid,$data){
        $users = DB::table('content_detail')
                ->join('content_meta', 'content_meta.content_id','=','content_detail.id')
                ->join('content_category', 'content_category.id', '=', 'content_detail.category_id')
                ->where('content_detail.status',1)
                ->where('content_category.status',1)
                ->where('content_detail.category_type_id',$cat_typeid)
                ->where('content_meta.news_date','=',$data)
                //->orderBy('m.event_date','ASC')
                ->select('content_detail.id','content_detail.en_description','content_detail.ar_description','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','content_category.en_name as en_category','content_category.ar_name as ar_category','content_category.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description','content_detail.ar_description','content_meta.en_meta_title','content_meta.ar_meta_title','content_meta.en_meta_desc','content_meta.ar_meta_desc','content_meta.en_meta_keywords','content_meta.ar_meta_keywords','content_meta.en_location','content_meta.ar_location','content_meta.news_date')->limit(3)
                ->orderBy('content_detail.id','desc')
                ->get();
        return $users; 
    }


    public static function checkNewsGal($id){
         return $data = Content::select('*')->where('content_detail.news_gallery_id',$id)->first();
    }
    
    public static function checkNewsVideoGal($id){
         return $data = Content::select('*')->where('content_detail.news_video_id',$id)->first();
    }


}