<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Helper;
class Usersettings extends Model
{
    //
       protected $table = 'users';
       public static function getUserData($col=['users.id','usr.role_id as role','users.name','users.email','users.created_at as publish on','users.status']){
          $data =DB::table('users')
               ->join('user_has_roles as usr','usr.user_id','users.id')
               //->where('is_delete',0)
               ->get($col);
       return $data;
    }

     public static function getStatus($type)
    {
        if($type == 1){
        return '<span class="label label-warning">Active</span>';
        }
        else
        {
        return '<span class="label label-danger">In active</span>';
        }
    }

    public static function get_career_column_names($type){
		$columns=['users.id','users.email','usr.role_id','users.created_at as publish on','users.status'];
			return $columns;
		}
    
    public static function getCareerContentx($id,$col){
    	$data = Usersettings::where('users.id',$id)
              ->join('user_has_roles as usr','usr.user_id','users.id')
    	        ->join('roles as rol','rol.id','usr.role_id')
    	        ->first(['users.id','users.email','rol.name as category','rol.id as role','users.status']);
    	return $data;
    }

    public static function getRolename($roleid){
      $fs = '';
       $results = DB::select('select * from roles where id = :id', ['id' => $roleid]);
        foreach ($results as $key => $value) {
          $fs = $value->name;
        }
    return $fs;
    } 

    public static function getatn($id)
    {
      $authId = Auth::user()->id;
      $rname = Helper::getRolename($authId);
      $roleid = '';

      foreach ($rname as $key => $value) {
        $roleid = $value->role_id;
      }

      if($roleid == 1){
         return '<a data-act="ajax-modal"  data-id="'.$id.'" class="check btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
     <a data-act="ajax-del" data-id="'.$id.'" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash" ></i></a>';
      }
      else{
        return '<a data-act="ajax-modal"  data-id="'.$id.'" class="disabled check btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
    <a data-act="ajax-del" data-id="'.$id.'" class="disabled btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>';
      }


    }

    public static function getatncont($id){
       $authId = Auth::user()->id;
      $rname = Helper::getRolename($authId);
      $roleid = '';

      foreach ($rname as $key => $value) {
        $roleid = $value->role_id;
      }

      if($roleid == 1){
         return '<a data-act="ajax-modal"  data-id="'.$id.'" class="check btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
     <a data-act="ajax-del" data-id="'.$id.'" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>';
      }
      else{
        return '<a data-act="ajax-modal"  data-id="'.$id.'" class="check btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
    <a data-act="ajax-del" data-id="'.$id.'" class="disabled btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash" ></i></a>';
      }
    }


}