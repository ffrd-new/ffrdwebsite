<?php

namespace App\Http\Middleware;
use App\User;
use Auth;

use Closure;

class APIMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if($request->header('Authtoken') == env('APIKEY'))  
            {
                return $next($request);
            }
                       
            $data=(object)[];
            return response()->json(['status'=>401,'messages' => "UnAuthorized",'data'=> $data]);
        }        
}