<?php

namespace App\Http\Middleware;
use App;
use Closure;
use Session;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;

use Illuminate\Contracts\Routing\Middleware;
class SetLocale
{
       public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Make sure current locale exists.
                // if($request->segment(1)){
                //         Session::put('locate',$locale);
                // }
               
             
                        $locale = $request->segment(1);

             if (array_key_exists($locale, $this->app->config->get('app.locales'))) 
             {
                Session::put('locate',$locale);
             }
        

        if ( ! array_key_exists($locale, $this->app->config->get('app.locales'))) {
            $segments = $request->segments();
            if(Session::has('locate')){
                $segments[0] = Session::get('locate');
            }
            else{

                $segments[0] = $this->app->config->get('app.fallback_locale');

            }
                
               

            return $this->redirector->to(implode('/', $segments));
        }
Session::put('locate',$locale);
        $this->app->setLocale($locale);

        return $next($request);
    }
}
