<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Session; 
class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('locate')) {
        App::setLocale(Session::get('locate'));
        }
       
        return $next($request);
    }
}
