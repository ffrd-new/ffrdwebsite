<?php

Route::get('/', 'FrontEndController@homepage');
Route::get('/news-and-events', function(){
    return view('frontend.news');
});


Route::get('/events','FrontEndController@events_list');
Route::get('/news','FrontEndController@news_list');
Route::get('news/lists','FrontEndController@newslist');
Route::get('events/lists','FrontEndController@eventslist');
Route::get('/gallery','FrontEndController@gallerylist');
Route::get('/getcategorytitle/{id}','FrontendController@getcategorylist');
Route::get('/{id}/news/{title}','FrontendController@FrontendNewsListurl');
Route::get('/{id}/event/{title}','FrontendController@FrontendEvensListurl');
Route::get('/{id}/gallery/{title}','FrontendController@FrontendGalleryListurl');
Route::get('/gallery/{id}/{title}','FrontendController@FrontendvideoGalleryListurl');

Route::get('/contact', function(){
    return view('frontend.contact');
});

Route::post('/postcontact','FrontEndController@contact');
Route::get('/gallery/type/{id}','FrontEndController@gallerycat');
Route::get('/gallery/load/{id}','FrontEndController@LoadGallery');
Route::post('/upload_image','ContentController@uploadimage');
Route::get('/about', function(){
	return view('pages.about');
});

