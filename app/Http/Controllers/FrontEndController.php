<?php

namespace App\Http\Controllers;
use App\Frontend;
use Illuminate\Http\Request;
use Validator;
use Mail;
use Redirect;
use App\Content;
use App\NewsHigh;
use App\Category;
use Helper; 
use App;
use Zipper;
use Config;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Redirector;

class FrontEndController extends Controller
{
    public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    public static function homepage(){
        $lang = App::getLocale();
        //$latestnewds = Content::getcontents(2);
        $latestnews = NewsHigh::getHighlights()->take(4);
        $latestevent = Content::latesteventhome(3)->take(4);
        $levents_cal = Content::latesteventhome(3)->take(10);
        $category = Category::gallerycategory(8)->take(3);  
        $videodata = Content::getvideogallerylist(8)->take(3);
        $imagedata = Content::getgallerylisthome(8)->take(4);

        // update
        $latestNewsHome = Content::latestNewsHome(2)->take(3);
        $latestNewsHomeCaro = Content::latestNewsHomeCaro(2)->take(10);


        $ccdate = date("Y/m/d");
        $lastDayThisMonth = date("Y-m-t");
        $eventsbydate = Content::getEventByDate(3,$ccdate,$lastDayThisMonth)->take(1);
        // 

        $publicadata = Content::getpublicalist(10);
        $rulerdetails = Content::getrulerdetails(11)->take(1);

        return view('frontend.home',
            ['lnews' => $latestnews,
            'levents' => $latestevent,
            'levents_cal' => $levents_cal,
            'category' => $category,
            'limagegallery' => $imagedata,
            'lvideogallery' => $videodata,
            'publicationli' => $publicadata,
            'rulerdetails' => $rulerdetails,

            'newsHome' => $latestNewsHome,
            'newsHomeCaro' => $latestNewsHomeCaro,
            'tes'  => $eventsbydate
        ]);        
        }

   public static function Contact(Request $request) {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|regex:/^\+?\d[0-9-]{9,12}/'
        ],
        [
            'name.required' => 'Name Required',
            'email.required' => 'Email Required',
            'phone.required' => 'Phone Number Required',
        ]);

        if($validator->fails()) 
        {
            return back()->withInput()->withErrors($validator->errors());
        }

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'bodyMessage' => $request->message
        );
        Mail::send('email.contact', $data, function($message) use ($data){
            $message->from($data['email']);
            $message->to('deeba@latlontechnologies.com');
            $message->Subject('Fujairah contact form');
        });
        // check for failures
        if (Mail::failures()) {
        // return response showing failed emails
        return Redirect::to('/contact')->with('message','Mail Not Sent, Please Retry');
        }
        // otherwise everything is okay ...
        return Redirect::to('/contact')->with('message','Mail Sent Successfully');
    }

    function _isCurl(){
        return function_exists('curl_version');
    }

    public function newslist(){      
        $data=Content::getallblog(2);
        $cat = Category::FCategory(2);
        $type='news';
        return view('frontend.event',['news'=>$data,'type'=>$type]);
    }

    public function eventslist(){      
        $data=Content::getallblog(3);
        $cat = Category::FCategory(3);
        $type='events';
        return view('frontend.event',['news'=>$data,'type'=>$type]);
    }

    public function gallerylist(){
         $category = Category::FCategory(8);
         $data=Content::getgallerylist(8);
         $clst = Category::FGalleryCatList(8);
         $videoclst = Category::FVideoGalleryCatList(8);
         $videodata=Content::getvideogallerylist(8);
         return view('frontend.gallery-album',['imagegallery'=>$data,'listdrop'=>$clst,'videolistdrop'=>$videoclst,'category'=>$category,'vidoegallery'=>$videodata]);
    }

    function FrontendGalleryListurl($id,$title){
    $cat = $id;
    $blog = Content::FListingpage_list_gallery(8,$cat);  
    $cat = Category::FCategory(8);  
    $recentevent = Content::lastevent(8);
    $page = ['related'=>"Top gallery"];
    $clst = Category::FGalleryCatList(8);
    $type = 'gallery';
    $videoclst = Category::FVideoGalleryCatList(8);
    $videodata=Content::getvideogallerylist(8);
    return view('frontend.gallery-album',['imagegallery'=>$blog,'type'=>$type,'recentevent'=>$recentevent,'listdrop'=>$clst,'videolistdrop'=>$videoclst,'vidoegallery'=>$videodata,'events'=>$blog,'page'=>$page,'viceversa'=>'events']);
    }

    function FrontendvideoGalleryListurl($id,$title){
    $cat = $id;
    $data=Content::getgallerylist(8); 
    $cat = Category::FCategory(8);  
    $recentevent = Content::lastevent(8);
    $page = ['related'=>"Top gallery"];
    $clst = Category::FGalleryCatList(8);
    $type = 'gallery';
    $videoclst = Category::FVideoGalleryCatList(8);
    $videodata=Content::FListingpage_list_videogallery(8,$id);
    return view('frontend.gallery-album',['imagegallery'=>$data,'type'=>$type,'recentevent'=>$recentevent,'listdrop'=>$clst,'videolistdrop'=>$videoclst,'vidoegallery'=>$videodata,'page'=>$page,'viceversa'=>'events']);
    }

    public function gallerycat($id){
        $images = '';
        $img = '';
        if(App::getLocale() == 'en'){
            $data=Content::FListingpage_en(8,$id);
            foreach ($data as $value) {
                if($value->gallery != null){
                    $images[] =   json_decode($value->gallery); 
                }else{
                    $img = 'Images Not Found';
                }
            } 
        }
        if(App::getLocale() == 'ar'){
            $data=Content::FListingpage_ar(8,$id);
            foreach ($data as $value) {
                $images[] =   json_decode($value->gallery); 
            }

            if($value->gallery != null){
                    $images[] =   json_decode($value->gallery); 
                }else{
                    $img = ' الصور غير موجودة   ';
                }
        }
         return response()->json(['html'=>$data,'images'=>$images,'img' => $img]);
    }

    function LoadGallery($id){
    $data=Content::getgallery(8,$id);
    $url =  json_decode($data->gallery);
    $base = url('/images/gallery/');
    foreach ($url as $key => $value) {
       $dataz[] = ['src'=>$base."/".$value];
    }
    return response()->json(['images'=>$dataz]);  
    }

    function getcategorylist($id){
    $cat = Content::getcategorydetail($id);
    return response()->json($cat);
    }

    public function getNewsbyDatelist($date){
        $collections = Content::latestNewsBYDate(2,$date);
        return response()->json($collections);
    }

    public function getSlugName($id){
        $data =  Helper::generateslug($id);
        return response()->json($data);
    }

    public function getImageNames($fd){

       $cat = Content::getImageforJq($fd);
      
       $data = Helper::imageCheck($cat);
       return response()->json($data);
    }

    public function getDate($date){
        if($date){
          $data =  Helper::ArabicDate(date('M j, Y', strtotime($date)));
        }else
        {
          $data =  Helper::ArabicDate(date('M j, Y', strtotime($date))) ;
        }
        if(App::getLocale() == 'en'){
        $data = date('M j, Y', strtotime($date));
        return response()->json($data);
        }else{
        return response()->json($data);
        }
    }

    public function news_list(){   
        $data = Content::getallblog_news(2,$limit=8);
        $cat = Category::FCategory(2);
        $type = 'news';
        $clst = Category::FCatList(2);
        $recentevent = Content::lastevent(3);
        $test = Content::getcontents(2);
        return view('frontend.news',['news'=>$data,'listdrop'=>$clst,'type'=>$type,'recentevent'=>$test,'viceversa'=>'news']);
    }

    function FrontendNewsListurl($id,$title){

    $cat = $id;
    $blog = Content::FListingpage_list(2,$cat,$limit=6);  
    $cat = Category::FCategory(2);  
    $recentevent = Content::getcontents(2);
    $page = ['related'=>"Top News"];
    $clst = Category::FCatList(2);
    $type = 'news';
    return view('frontend.news',['blog'=>$blog,'type'=>$type,'recentevent'=>$recentevent,'listdrop'=>$clst,'news'=>$blog,'page'=>$page,'viceversa'=>'news']);
    }
    
    public function news_detail($type,$slug){
        $data = Content::getContent($type);
        $recentblog = Content::recentnewss(2,$data->category_id,$slug,'desc')->take(3);
        $recentevent = Content::lastevent(2);
        $getGalleryData = Category::getDetailNewsGallery($data->news_gallery_id,8);
        $getVideoData = Category::getDetailNewsVideoGallery($data->news_video_id,8);

        return view('frontend.news-detail',['news_detail'=>$data,'recentnews'=>$recentblog,'recentevent'=>$recentevent,'type'=>'news','newsgal_data' =>$getGalleryData,'newsvideo_data' => $getVideoData]);
    }

    public function publications_list(){
        $data = Content::getpublilist(10,$limit=6);
        $type = 'publications';
        $clst = Category::FCatList(10);
        $recentevent = Content::lastevent(10);
        $test = Content::getcontents(10);
        return view('frontend.publications',['publist'=>$data,'listdrop'=>$clst,'type'=>$type,'recentpublist'=>$test,'viceversa'=>'publications']);
    }
    public function publica_detail($type,$slug){
    
        $data = Content::getPublicaContent($type);
        $recentblog = Content::recentblog(10,$data->category_id,$slug,'desc')->take(3);
        $recentevent = Content::lastevent(10);
        return view('frontend.publications-detail',['news_detail'=>$data,'recentnews'=>$recentblog,'recentevent'=>$recentevent,'type'=>'publications']);
    }

    public function FrontendPubListurl($id,$title){

    $cat = $id;
    $blog = Content::FListingpage_publicalist(10,$cat,$limit=6);  
    $recentevent = Content::getcontents(10);
    $page = ['related'=>"Top News"];
    $clst = Category::FCatList(10);
    $type = 'publications';
    return view('frontend.publications',['publist'=>$blog,'type'=>$type,'recentpublist'=>$recentevent,'listdrop'=>$clst,'news'=>$blog,'page'=>$page,'viceversa'=>'publications']);
    }
     public function events_list(){      
        $data=Content::getallblog_evnts(3,$limit=8);
        $cat = Category::FCategory(3);
        $type='events';
        $clst = Category::FCatList(3);
        $recentevent=Content::lastevent(3);
        return view('frontend.events',['events'=>$data,'listdrop'=>$clst,'type'=>$type,'recentevent'=>$recentevent,'viceversa'=>'events']);
    }

    function FrontendEvensListurl($id,$title){

    $cat = $id;
    $blog = Content::FListingpage_list_event(3,$cat,$limit=8);  
    $cat = Category::FCategory(3);  
    $recentevent = Content::lastevent(3);
    $page = ['related'=>"Top Events"];
    $clst = Category::FCatList(3);
    $type = 'events';
    return view('frontend.events',['blog'=>$blog,'type'=>$type,'recentevent'=>$recentevent,'listdrop'=>$clst,'events'=>$blog,'page'=>$page,'viceversa'=>'events']);
    }

     public function events_detail($ds,$slug){
        $data = Content::getContent($ds);
        $recentblog=Content::recentblog(3,$data->category_id,$slug,'desc')->take(3);
        $recentevent=Content::lastevent(2);
         $ccdate = date("Y/m/d");
        $lastDayThisMonth = date("Y-m-t");
        $eventsbydate = Content::getEventByDate(3,$ccdate,$lastDayThisMonth)->take(3);
        return view('frontend.events-detail',['news_detail'=>$data,'recentnews'=>$eventsbydate,'recentevent'=>$recentevent,'type'=>'events','viceversa'=>'news']); 
    }


    // ****************************************************************



    public function news_list_json(){
        $va = $_GET['key'];
        if($_GET['typeid']){
         $typeid = $_GET['typeid'];   
        }
        
        $catid = $_GET['catid'];
        $data = Content::getallblog(2,$limit=6);
        $cat = Category::FCategory(2);
        $type = 'news';
        $clst = Category::FCatList(2);
        $recentevent = Content::lastevent(3);
        $test = Content::getcontents(2);
        $content = FListingpageApi($typeid,$catid);
        return response()->json(['listdrop'=>$clst,'type'=>$type,'recentevent'=>$test,'viceversa'=>'news','key'=>$va]);    
    }


     public function ziparchive($id, Request $request)
    {
        if($id) {
            $data = Content::getContent($id);

            $fil = $data->pdf;
            $encfile = json_decode($fil);
            $value = Config::get('app.name');

            if(App::getLocale() == 'en'){
            $one =  str_limit($data->en_title,10);
            $tile = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $one));
            $zipname = $value.time().'.zip';
            }
            if(App::getLocale() == 'ar'){
            $one =  str_limit($data->ar_title,10);    
            $zipname = $value.time().'.zip';
            }
            foreach ($encfile as $key => $value) {
            
               $files = public_path('images/publications/media/'.$value);

               Zipper::make(public_path($zipname))->add($files)->close();
            }  
        return response()->download(public_path($zipname));

        }

        $data = Content::getallblog(10,$limit=6);
        $type = 'publications';
        $clst = Category::FCatList(10);
        $recentevent = Content::lastevent(10);
        $test = Content::getcontents(10);
        return view('frontend.publications',['publist'=>$data,'listdrop'=>$clst,'type'=>$type,'recentpublist'=>$test,'viceversa'=>'publications']);

    }

    public function ziparchivedetail($id)
    {
        if($id) {
            $data = Content::getContent($id);
            $fil = $data->pdf;
            $encfile = json_decode($fil);
            $value = Config::get('app.name');
            
            if(App::getLocale() == 'en'){
            $one =  str_limit($data->en_title,10);
            $tile = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $one));
            $zipname = $value.time().'.zip';
            }
            if(App::getLocale() == 'ar'){
            $one =  str_limit($data->ar_title,10);
            $zipname = $value.time().'.zip';
            }

            foreach ($encfile as $key => $value) {
               $files = public_path('images/publications/media/'.$value);
               Zipper::make(public_path($zipname))->add($files)->close();
            }  
           return response()->download(public_path($zipname));
           }

        $data = Content::getContent($type);
        $recentblog = Content::recentblog(10,$data->category_id,$slug,'desc')->take(3);
        $recentevent = Content::lastevent(10);
        return view('frontend.publications-detail',['news_detail'=>$data,'recentnews'=>$recentblog,'recentevent'=>$recentevent,'type'=>'publications']);
    }


    public function SetLocale(Request $request){


  // return $locale = $request->segment(1);

        $locale = $request->segment(1);
       // return $request->path();
        if (array_key_exists($locale, $this->app->config->get('app.locales'))) {
            $segments = $request->segments();
//           $segments[0] = $this->app->config->get('app.fallback_locale');

            if($locale == 'en'){
                return redirect('/ar');
            }


            if($locale == 'ar'){
                 return redirect('/en');
            }

          //  return $this->redirector->to(implode('/', $segments));
        }
    }

    public static function globalsearch(Request $request){
      $keyword = $request->globsearch;
      if(!empty($keyword)){
         $list = Content::getSearch($keyword);
         $type='events';
         return view('frontend.search',['lists'=>$list,'type'=>$type,'keyw'=>$keyword]);
      }
      return redirect('/');
  }

 public static function requestphoto(Request $request){
    $validator = Validator::make($request->all(),[
        'cname' => 'required',
        'cemail' => 'required',
        'cphone' => 'required|regex:/^\+?\d[0-9-]{9,12}/'
    ]);
    if($validator->fails()){
       return back()->withInput()->withErrors($validator->errors());
    }
    $data = array(
        'name' => $request->cname,
        'email' => $request->cemail,
        'phone' => $request->cphone,
        'bodyMessage' => $request->message
    );
    Mail::send('email.request', $data, function($message) use ($data){
        $message->from('noreply@media.fujairah.ae');
        $message->to('info@media.fujairah.ae');
        // $message->to('webdeveloper_02@latlontechnologies.com');
        $message->Subject('FCMC Request Form');
    });
    if(Mail::failures()){
        return back()->with('message','Mail Not Sent, Please Retry');
    }
    return back()->with('message','Mail Sent Successfully');
  }

  
    public static function upevents(){
        $ccdate = date("Y/m/d");
        $lastDayThisMonth = date("Y-m-t");
        $eventsbydate = Content::getEventByDate(3,$ccdate,$lastDayThisMonth);
        $levents_cal = Content::latesteventhome(3)->take(10);
        return view('frontend.upcoming-events',['evntlis' => $eventsbydate,'levents_cal' => $levents_cal,]);
    }

}