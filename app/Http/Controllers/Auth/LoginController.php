<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Validator;
use App\User;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/news';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){


            $vali = Validator::Make($request->all(),
        [
            
            'email'=>'required|email',
            'password'=>'required|min:6'
        ]);

            if($vali->fails()){
                  return back()->withErrors($vali)
                        ->withInput();
            }


    $user = User::where('email',$request->email)->first();
  
            if(count($user) > 0){
                if($user->status == 1){
                        if(Auth::Attempt(['email'=>$request->email,'password'=>$request->password]))
                        {
                            return Redirect('/admin/news');
                        }
                        else{
                            return back()->with(['danger'=>'Invalid Credentials'])->withInput();
                        }
                }else{
                    return back()->with(['danger'=>'Contact Administrator to login'])->withInput();
                }        
            }
            else{
                $vali =  ['danger'=>'Invalid Credentials'];
            return back()->with($vali)->withInput();
            }

    }
}
