<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Content;
use App\Usersettings;
use App\NewsHigh;
use Helper;
use Yajra\Datatables\Datatables;
use Validator;
use Image;
use DB;
use Auth;
use Carbon\Carbon;
use File;
use App\Meta;


class NewsHighlightController extends Controller
{
    public function __construct( )
    {
    	$this->status = ['success'=>'Record Added Successfully تمت إضافة السجل بنجاح  '];
    	$this->status_update = ['success'=>'Record Updated Successfully تمت إضافة السجل بنجاح  '];
    	$this->delete = ['errors'=>'Record Deleted Successfully تم حذف السجل بنجاح  '];
    	$this->message  = ['errors'=>"Can't Delete. It has either Subcategory or News لا يمكن الحذف.  ديها إما فئة فرعية أو مدونة  "];
    	$this->message_gal  = ['errors'=>"Can't Delete. It has Gallery لا يمكن الحذف.  ديها إما فئة فرعية أو مدونة  "];
    	$this->message_events  = ['errors'=>"Can't Delete. It has Events لا يمكن الحذف.  ديها إما فئة فرعية أو مدونة  "];
    	$this->newsGalTxt = ['errors' => "Can't Delete, It has News  لا يمكن الحذف ، له أخبار  "];
    }

public function newshigh_table(Request $request){
   $data = NewsHigh::getNewsHighData($request->lang);

   return Datatables::of($data)->rawColumns(['status','action'])
      ->addColumn('status',function ($data)
                    {
                  return Helper::cms_category_status($data->status);
                    })
      ->addColumn('action', function ($data)
        {
        return '<a data-act="ajax-modal"  data-id="'.$data->id.'" class="check btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
        <a data-act="ajax-del" data-id="'.$data->id.'" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash" ></i></a>';
        }
        )->make(true);
   }

  public function get_newshigh_content(Request $request,$id){
      
        $type = $request->status;
        $col= NewsHigh::get_member_column_names($type);
        $data = NewsHigh::getNewsHighContentx($id,$col);
        return response()->json($data,200);
   } 


    public function news_store(Request $request){

        if($request->del){
            $store = NewsHigh::find($request->id);
            $store->delete();
            return response()->json(['success'=>'Data Deleted']); 
        }

        if($request->id == ''){
        $validator = Validator::make($request->all(), [
            'en_name' => 'required',
            'ar_name' => 'required',
            'news_high_url' => 'required'
          ],
          [
           'en_name.required' => 'Title Required',
           'ar_name.required' => ' العنوان مطلوب   ',
           'news_high_url.required' => 'Url Required',
           
          ]);
        if($validator->fails()) 
        {
        return response()->json(['errors'=>$validator->errors()->all()]);         
        }
        }

        if($request->id == ''){
        $store = new NewsHigh;
        $store->en_title = $request->en_name;
        $store->ar_title = $request->ar_name;
        $store->news_url = $request->news_high_url;
        if($request->status){
           $store->status = 1;
        }else{
           $store->status = 0;
        }

        $store->save();
          
        }
        else{
        $store = NewsHigh::find($request->id);
        $store->en_title = $request->en_name;
        $store->ar_title = $request->ar_name;
        $store->news_url = $request->news_high_url;

        if($request->status){
        $store->status = 1;
        }else{
        $store->status = 0;
        }
        $store->save();
      }
       return response()->json(['success'=>'Data Recorded']);
  }




// ---------------------
}