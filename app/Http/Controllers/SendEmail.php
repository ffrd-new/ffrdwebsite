<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Validator;
use Session;
use Auth;
use Redirect;

class SendEmail extends Controller
{
	function cntform(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'phone' => 'required|regex:/^\+?\d[0-9-]{9,12}/ ',
			'email' => 'required|email',
			'message' => 'required'
		],
		[
			'name.required' => 'First Name Required',
			'phone.required' => 'Phone Number required',
			'email.required' => 'Email Required',
			'message.required' => 'Message required',
		]);

		if($validator->fails()) 
		{
			return back()->withInput()->withErrors($validator->errors());
		}

		$data = array(
			'name' => $request->name,
			'phone' => $request->phone,
			'email' => $request->email,
			'bodyMessage' => $request->message
		);
		Mail::send('email.getus', $data, function($message) use ($data){
			$message->from($data['email']);
			$message->to('dreamhomz_enquiries@outlook.com');
			$message->Subject('DreamHomz Get In Touch Details');
		});
        // check for failures
		if (Mail::failures()) {
            // return response showing failed emails
			return Redirect::to('/')->with('success', false)->with('message','Mail Not Sent, Please Retry');
		}

        // otherwise everything is okay ...
		return Redirect::to('/')->with('success', true)->with('message','Mail Sent Successfully');
	}

	function contactform(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'phone' => 'required|regex:/^\+?\d[0-9-]{9,12}/ ',
			'email' => 'required|email',
			'message' => 'required'
		],
		[
			'name.required' => 'First Name Required',
			'phone.required' => 'Phone Number required',
			'email.required' => 'Email Required',
			'message.required' => 'Message required',
		]);

		if($validator->fails()) 
		{
			return back()->withInput()->withErrors($validator->errors());
		}

		$data = array(
			'name' => $request->name,
			'phone' => $request->phone,
			'email' => $request->email,
			'bodyMessage' => $request->message
		);
		Mail::send('email.contact', $data, function($message) use ($data){
			$message->from($data['email']);
			$message->to('dreamhomz_enquiries@outlook.com');
			$message->Subject('DreamHomz Contact Details');
		});
        // check for failures
		if (Mail::failures()) {
            // return response showing failed emails
			return Redirect::to('/contact')->with('success', false)->with('message','Mail Not Sent, Please Retry');
		}

        // otherwise everything is okay ...
		return Redirect::to('/contact')->with('success', true)->with('message','Mail Sent Successfully');
	}

	function registerfrm(Request $request){
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'phone' => 'required|regex:/^\+?\d[0-9-]{9,12}/ ',
			'email' => 'required',
			'message' => 'required'
		],
		[
			'name.required' => 'Name Required',
			'phone.required' => 'Phone Number required',
			'email.required' => 'Email Required',
			'message.required' => 'Message required',
		]);

		if($validator->fails()) 
		{
			// return back()->withInput()->withErrors($validator->errors());
			return response()->json(['errors' => $validator->errors()]);
		
		}

		$data = array(
			'name' => $request->name,
			'phone' => $request->phone,
			'email' => $request->email,
			'bodyMessage' => $request->message
		);
		Mail::send('email.register', $data, function($message) use ($data){
			// $message->from('webdeveloper_02@latlontechnologies.com');
			$message->from($data['email']);
			$message->to('dreamhomz_enquiries@outlook.com');
			$message->Subject('DreamHomz Register Your Interest Details');
		});
        // check for failures
		if (Mail::failures()) {
            // return response showing failed emails
			// return Redirect::to('/')->with('success', false)->with('message','Mail Not Sent, Please Retry');
			return response()->json(['fail' => 'Message Failure']);
		}

        // otherwise everything is okay ...
		return response()->json(['success' => 'Your E-mail was sent!.']);

	}
}
