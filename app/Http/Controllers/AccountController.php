<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Content;
use App\Usersettings;
use Helper;
use Yajra\Datatables\Datatables;
use Validator;
use Image;
use DB;
use Auth;
use Carbon\Carbon;
use File;
use App\Meta;


class AccountController extends Controller
{
    public function __construct( )
    {
    	$this->status = ['success'=>'Record Added Successfully تمت إضافة السجل بنجاح  '];
    	$this->status_update = ['success'=>'Record Updated Successfully تمت إضافة السجل بنجاح  '];
    	$this->delete = ['errors'=>'Record Deleted Successfully تم حذف السجل بنجاح  '];
    	$this->message  = ['errors'=>"Can't Delete. It has either Subcategory or News لا يمكن الحذف.  ديها إما فئة فرعية أو مدونة  "];
    }

    // User module
    function user_table(Request $request){
    $data = Usersettings::getUserData();
    return Datatables::of($data)->rawColumns(['action','status'])
    ->addColumn('role',function ($data)
            {
          return Usersettings::getRolename($data->role);
            })
        ->addColumn('status',function ($data)
            {
          return Usersettings::getStatus($data->status);
            })
        ->addColumn('action', function ($data)
        {
        return Usersettings::getatn($data->id);
        }
        )->make(true);
        }

    function user_store(Request $request){

        if($request->del){
            $store = Usersettings::find($request->id);
            $store->delete();
            return response()->json(['success'=>'Data Deleted']); 
        }

        if($request->id == ''){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'category' => 'required',
          ],
          [
               'email.required' => 'Email Required',
               'password.required' => 'Password Required',
               'category.required' => 'Role Required',
          ]);
        if($validator->fails()) 
        {
        return response()->json(['errors'=>$validator->errors()->all()]);         
        }
        }

        if($request->id == ''){
	        $store = new Usersettings;
	        $store->name = '';
	        $store->email = $request->email;
	        $store->password = bcrypt($request->password);
	        if($request->status === 'true'){
	           $store->status = 1;
	        }else{
	           $store->status = 0;
	        }

	        $store->save();
            DB::table('user_has_roles')->insert(
                ['role_id' => $request->category, 'user_id' => $store->id]
            );
          }
          else{
	        $store = Usersettings::find($request->id);
	        $store->email = $request->email;

	        if($request->password){
             $validator = Validator::make($request->all(), [
            'password' => 'min:6'
            ],
              [
               'password.required' => 'Password Required',
              ]);
	        if($validator->fails()) 
	        {
	        return response()->json(['errors'=>$validator->errors()->all()]);         
	        }
	         $store->password = bcrypt($request->password);
	        }
         
          if($request->email == null){
          $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            ],
              [
               'email.required' => 'Email Required',
              ]);
          if($validator->fails()) 
          {
          return response()->json(['errors'=>$validator->errors()->all()]);         
          }
          }

          if($request->email){
          $validator = Validator::make($request->all(), [
             'email' => 'email|unique:users,email,'.$request->id,
             ],
              [
               'email.required' => 'Email Required',
              ]);
          if($validator->fails()) 
          {
          return response()->json(['errors'=>$validator->errors()->all()]);         
          }
          }
            
	        if($request->status === 'true'){
	           $store->status = 1;
	        }else{
	           $store->status = 0;
	        }

          $store->save();
          if($request->category == 1){
             $fss =  DB::table('user_has_roles')
            ->where('user_id', $request->id) 
            ->update(['role_id' => $request->category]);
           }elseif($request->category == 2){
             $fss =  DB::table('user_has_roles')
            ->where('user_id', $request->id) 
            ->update(['role_id' => $request->category]);
           }else{
            $fss =  DB::table('user_has_roles')
            ->where('user_id', $request->id) 
            ->update(['role_id' => $request->role]);
           }
           
          }
          return response()->json(['success'=>'Data Recorded']);
	}

	 public function role_json_get(){
		$users = DB::table('roles')->select('id','name as en_name')->get();
		return response()->json($users,200); 
	}

    public function get_user_content(Request $request,$id){
        $type = $request->status;
        $col= Usersettings::get_career_column_names($type);
        $data = Usersettings::getCareerContentx($id,$col);
        return response()->json($data,200);
    }

public static function logt(Request $request){
  Auth::logout();
  return redirect('/login');
 }

}