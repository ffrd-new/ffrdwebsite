<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Content;
use App\Usersettings;
use Helper;
use Yajra\Datatables\Datatables;
use Validator;
use Image;
use DB;
use Auth;
use Carbon\Carbon;
use File;
use App\Meta;


class ContentController extends Controller
{
    public function __construct( )
    {
    	$this->status = ['success'=>'Record Added Successfully تمت إضافة السجل بنجاح  '];
    	$this->status_update = ['success'=>'Record Updated Successfully تمت إضافة السجل بنجاح  '];
    	$this->delete = ['errors'=>'Record Deleted Successfully تم حذف السجل بنجاح  '];
    	$this->message  = ['errors'=>"Can't Delete. It has either Subcategory or News لا يمكن الحذف.  ديها إما فئة فرعية أو مدونة  "];
    	$this->message_gal  = ['errors'=>"Can't Delete. It has Gallery لا يمكن الحذف.  ديها إما فئة فرعية أو مدونة  "];
    	$this->message_events  = ['errors'=>"Can't Delete. It has Events لا يمكن الحذف.  ديها إما فئة فرعية أو مدونة  "];
    	$this->newsGalTxt = ['errors' => "Can't Delete, It has News  لا يمكن الحذف ، له أخبار  "];
    }

	public function cms_list(Request $request){
         $data =  Helper::cms_page($request->type,$request->lang);
         return view('admin.'.$data["pages"].'',$data);
	}

	public function cms_category_table(Request $request){
	$colums = Helper::cms_category_colums($request->type,$request->lang);
	$data  = Category::where('type_id',Helper::cmstype($request->type))->OrderBy('id','DESC')->get($colums);
	return Datatables::of($data)
                    ->rawColumns(['action','type','status'])->addColumn('type',function ($data)
					{
                  return Helper::cms_category_type($data->is_parent);
					})->addColumn('status',function ($data)
					{
                  return Helper::cms_category_status($data->status);
					})
					->addColumn('action', function ($data) {
                return '<a data-act="ajax-modal"  data-id="'.$data->id.'" class="check btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
     <a data-act="ajax-del" data-id="'.$data->id.'" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>';
            })->orderColumn('type_id','asc')->make(true);
	}

	public function langmember_json_get($id,$type){
    $colums = Helper::cms_category_colums($type);
	if($id=='en'){
    $data  = Category::where('type_id',Helper::cmstype($type))
	         ->get(['id','en_name as title','is_parent','status']);
	}else{
		$data  = Category::where('type_id',Helper::cmstype($type))
	         ->get(['id','ar_name as title','is_parent','status']);
	}       

    return Datatables::of($data)
           ->rawColumns(['action','type','status'])->addColumn('type',function ($data)
					{
                return Helper::cms_category_type($data->is_parent);
					})->addColumn('status',function ($data)
					{
                return Helper::cms_category_status($data->status);
					})
					->addColumn('action', function ($data) {
                return '<a data-act="ajax-modal"  data-id="'.$data->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
                <a data-act="ajax-del" data-id="'.$data->id.'" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-remove" ></i></a>';
            })->make(true);
	}

	public function content_table(Request $request){
				$colums = Helper::cmstype($request->type,$request->lang);
                $columsp = Helper::cms_category_colums($request->type,$request->lang);
				$data = Content::getData($colums,$request->lang,$columsp,$request->type);

			    return Datatables::of($data)
                    ->rawColumns(['action','type','status', 'publish date'])->addColumn('type',function ($data)
					{
                  return Helper::cms_category_type($data->is_parent);
					})
					->addColumn('status',function ($data)
					{
                  return Helper::cms_category_status($data->status);
					})
					->addColumn('publish date',function ($data)
					{
                  return Helper::newsdate($data->news_date,$data->publish_on,$data->publication_date,$data->image_date,$data->video_date);
					})
					->addColumn('action',function($data) 
					{	
					return Helper::access_content($data->id);
                    }
                )->make(true);
		}

	public function cms_category_store(Request $request){
		
 		($request->id) ? $store = Category::find($request->id)  :  $store = new Category ;
 		if($request->del){
           $parent=Category::where('is_parent',$request->id)->whereIn('status',array(0,1))->get();
 		   $blog=Content::where('category_id',$request->id)->whereIn('status',array(0,1))->get();
 			if((count($parent) <= 0) && (count($blog) <= 0)){
			   $store->delete();
		       return response()->json($this->delete,200);
 			}else{
 				if($request->del == 'events_category'){
 			       return response()->json($this->message_events,200);
 				}
 				if($request->del == 'gallery_category'){
 			       return response()->json($this->message_gal,200);
 				}
 				if($request->del == 'news_category'){
 			       return response()->json($this->message,200);	
 				}
 				if($request->del == 'publications_category'){
 			       return response()->json($this->message,200);	
 				}
 			}
 		    }

 		$error = $this->category_validator($request->all());
 		 if($error->fails()) 
          {
          return response()->json(['errors'=>$error->errors()->all()]);           
          }
			$store->en_name = $request->en_name;
			$store->ar_name = $request->ar_name;
			$store->is_parent = ($request->is_parent) ? $request->is_parent : NULL;
			$store->status = Helper::cmsstatus($request->status);
			$store->type_id = Helper::cmstype($request->type);
			$store->save();
			if($request->id){
                return response()->json($this->status_update,200); 
			}else{
				return response()->json($this->status,200);  
			}	
	    }


	public function cms_category_parent($id){
			$data  = Category::find($id);
		if(count($data)>0){
			if($data->is_parent != NULL){
				
			}
		}
	}

	public function cms_category_json_get($id){
		
	$data = Category::where('content_category.id',$id)->leftjoin('content_category as p','p.id','content_category.is_parent')->first(['content_category.id','p.id as parent_id','content_category.en_name as en_name','content_category.ar_name as ar_name','p.en_name as parent_name','content_category.status']);
    return response()->json($data,200); 

	}

	public function get_content(Request $request,$id){
		$type=$request->type;
		$col=Helper::get_column_names($type);
		$data = Content::getContentx($id,$col);
		return response()->json($data,200); 
	}

	public function cms_category_json(Request $request){
		$data = [];
		$arrayName = [];
		$arra = [];
     	
     	if($request->content == 'newsgal'){
     	   $typeGal = 'gallery';	
           $data  = Content::getgallerylisthome(8);
		   foreach($data as $key => $value) {
				if($value->ar_title !=''){
			           $arname = $value->ar_title;
					}else{
					   $arname = '';
					}

			$arrayName[] = array(
				'id' => $value->id, 
				'en_name'=> $value->en_title.' '.$arname);
			}

			$arr[] = array("id"=>"0", "en_name"=>"Select Value  اختار القيمة   ");

			$arra[] = array_merge($arr,$arrayName);
			
			return response()->json($arra[0]);
     	    }
     	    elseif($request->content == 'newsvideogal') {
              $typeGal = 'videogallery';
              $videodata = Content::getvideogallerylist(8);
               foreach($videodata as $key => $value) {
				if($value->ar_title !=''){
			           $arname = $value->ar_title;
					}else{
					   $arname = '';
					}

			$arrayName[] = array('id' => $value->id, 'en_name'=> $value->en_title.' '.$arname);
       
			}

			$arr[] = array("id"=>"0", "en_name"=>"Select Value اختار القيمة    ");
            $arra[] = array_merge($arr,$arrayName);
			return response()->json($arra[0]);

     	    }
     	    else{
            $data  = Category::where('type_id',Helper::cmstype($request->content))->where('status',1)->where(function($query) use ($request){
			if($request->category == 'parent'){
				 $query->whereNull('is_parent');
			}
			if($request->category == 'child'){
				 $query->where('is_parent',$request->id);
			}
			if($request->q){
				 $query->where('en_name','LIKE','%'.$request->q.'%');
			}
			})->get(['id','en_name','ar_name','is_parent']);
			foreach ($data as $key => $value) {
				if($value->ar_name !=''){
			           $arname = $value->ar_name;
					}else{
					   $arname = '';
					}
				if($value->is_parent){
					$arrayName[] = array('id' => $value->id, 'en_name'=> $value->en_name.' '.$arname.'-'.$this->Getparent($value->is_parent));
				}
				else{
					$arrayName[] = array('id' => $value->id, 'en_name'=> $value->en_name.' '.$arname);
				}
			}
			 return response()->json($arrayName);
     	}


		}


		public static function Getparent($id){
			$cat = Category::find($id);
			return $cat->en_name.' '.$cat->ar_name;
		}

// ------- cahnge
		public function cms_content_store(Request $request){
// $re = date("Y-m-d", strtotime($request->event_date));
// return $re;
			if($request->id) {
				$store = Content::find($request->id);
				$store->modified_by = Auth::id();
				$meta = Meta::where('content_id',$request->id)->first();
				$iid=$meta->id;
			}
			else{
				$store = new Content;
				$store->created_by = Auth::id();
				$meta = new Meta;
				$iid = "";
			}

			if($request->del){
				$newsgalId = Content::checkNewsGal($request->id);
				$newsgalVideoId = Content::checkNewsVideoGal($request->id);

                if(count($newsgalId) != 0 || count($newsgalVideoId) != 0){
                   return response()->json($this->newsGalTxt,200); 
                }else{
                   $meta_detail=Meta::where('content_id',$request->id);
				   $store->delete();
				   $meta_detail->delete();
				   return response()->json($this->delete,200); 
                }
 			}
            
 			$error = $this->content_validator($request->all());
 		 	if($error->fails()) 
          	{
        	 return response()->json(['errors'=>$error->errors()->all()]);           
    	  	}
    	  	
			$store->en_title = $request->en_title;
			$store->ar_title = $request->ar_title;
			if($request->type == 'blogs'){
			$store->description	= $request->blog_content;
			}
			if($request->type == 'video_blogs'){
				$store->description	= $request->video_blog_content;
			}
			if($request->type == 'news'){
			$store->en_description	= $request->en_news_content;

			$store->ar_description	= $request->ar_news_content;
			
             if($request->newsgallery && $request->newsgallery != 0){
             	 $re = Content::getNameGalry($request->newsgallery);
             	 if(isset($re)){
             	 	$store->news_gallery_name	= $re->full_name;
			        $store->news_gallery_id	= $request->newsgallery;
             	 }
             }else{
             	$store->news_gallery_name = null;
             	$store->news_gallery_id = null;
             }

             if($request->newsvideogallery && $request->newsvideogallery != 0){
             	 $red = Content::getNameGalry($request->newsvideogallery);

             	 if(isset($red)){
             	 	$store->news_video_name	= $red->full_name;
			        $store->news_video_id	= $request->newsvideogallery;
             	 }
             }else{
             	$store->news_video_name = null;
             	$store->news_video_id = null;
             }
			
			
			}	
			if($request->type == 'events'){
		    $store->en_description	= $request->en_event_content;
			$store->ar_description	= $request->ar_event_content;
			}

			if($request->type == 'publications'){
		    $store->en_description	= $request->en_public_content;
			$store->ar_description	= $request->ar_public_content;
			}

			if($request->type == 'ruler'){
		    $store->en_description	= $request->en_public_content;
			$store->ar_description	= $request->ar_public_content;
			}

			if($request->type == 'gallery'){
			$store->en_description	= 'gallery';
			$store->ar_description	= 'صالة عرض   ';
			$store->gallery_type = 1;

			}

			if($request->type == 'video_gallery'){
			$store->gallery_type = 2;		
			}

			$store->status =  Helper::cmsstatus($request->status);
			$store->publish_date = ($request->status =='true') ? Carbon::now()->toDateTimeString() : NULL;
			if($request->category){
			   $store->category_id = $request->category;
			   $store->category_type_id = helper::Category_Type($request->category);	
			}else{
			   $store->category_id = 88;
			   $store->category_type_id = helper::Category_Type(88);
			}

			if($request->r_image){
               $store->image = ($request->r_image) ? Helper::image(helper::Category_Type(88),$request->r_image,"") : "";
			}
			
			if($request->f_image){
			$store->image = ($request->f_image) ? Helper::image(helper::Category_Type($request->category),$request->f_image,"") : "";
			}

			if($request->vgimage){
			$store->image = ($request->vgimage) ? Helper::image($store->category_type_id,$request->vgimage,"","350,200") : "";	
			}

			if($request->fb_image){
               $store->image = ($request->fb_image) ? Helper::image($store->category_type_id,$request->fb_image,"","200,261") : "";
			}
			$store->save();

			if($request->id){
                if( $request->en_slug == NULL){
                    $meta->en_slug = Helper::pretty_urlwithid($request->en_title,$request->id);
                }
                if($request->ar_slug == NULL){
                	$meta->ar_slug = Helper::make_slug($request->ar_title);
                }

                if($request->en_slug != ''){
	                if($meta->en_slug != $request->en_slug){
					$meta->en_slug = Helper::pretty_urlwithid($request->en_slug,$request->id);
					}else{
						$meta->en_slug = Helper::pretty_urlwithid($request->en_slug,$request->id);
					}
                }
                if($request->ar_slug != ''){
					if($meta->ar_slug != $request->ar_slug){
						$meta->ar_slug = Helper::make_slug($request->ar_slug);
					}else{
						$meta->ar_slug = Helper::make_slug($request->ar_slug);
					}
                }
            if($request->type == 'events'){
			$meta->en_location = $request->en_location;
			$meta->ar_location = $request->ar_location;
			$edate = $request->event_date;
			$meta->event_date = $request->event_date;
			$meta->event_date_on = date("Y-m-d", strtotime($request->event_date));
			//$meta->event_date = date("Y-m-d h:i:s", strtotime($edate));
			}

            if($request->type == 'gallery'){
            $meta->image_date	= $request->image_date;
            }
            if($request->type == 'video_gallery'){
            $meta->video_date	= $request->video_date;
            }

			if($request->type == 'news'){
			$pubdate = $request->news_date;
			$meta->news_date = date("Y-m-d", strtotime($pubdate));	
			}
			if($request->type == 'publications'){
			$meta->pubcurl = $request->pubcurl;	
			$meta->flipname = $request->flipname;
			$meta->publication_date = $request->publication_date;
			}



			}
			else{
				$meta->en_slug = Helper::pretty_url($request->en_slug);
				$meta->ar_slug = Helper::make_slug($request->ar_slug);
			}

            // pdf files
			if($request->bookpdf){
		    $col = "pdf";
			$files = $request->file('bookpdf');
			foreach ($files as $fi_key=> $fi_value) 
             { 
               $extension = $fi_value->getClientOriginalExtension();
               $ofilename = str_replace(' ', '-', $fi_value->getClientOriginalName());
               $ss = explode(".",$ofilename);
		       $uniqueFileName = $ss[0].'-'.rand(10,100).'.'.$ss[1]; 
		       $pdf_file = $fi_value->move(public_path('images/publications/media/'), $uniqueFileName);
		       $pdf[] = $uniqueFileName;
			 }
            
			if(!$iid){
			   $meta->pdf =  json_encode($pdf);
			}
			if($iid){
			    $image =  Meta::where('id',$iid)->first();
			    $a1 = $image->{$col};
			  if(count(json_decode($a1)) > 0){
			      $a2 = json_encode($pdf);
			      $a3 = array_merge(json_decode($a1),json_decode($a2));
			      $meta->pdf = json_encode($a3);
			  }
			  else{
			    $meta->pdf = json_encode($pdf);
			}
			}
			}

            // 

			if($request->gimage){
				$photo = $request->file('gimage');
                $path = public_path('images/gallery');
                $id= $iid;
                $column = "gallery";
                $meta->gallery = Helper::MultipleImages($photo,$path,$id,$column);
			}
			$meta->content_id = $store->id;
			$meta->en_meta_keywords = ($request->en_meta_keyword) ? $request->en_meta_keyword : NULL ;
			$meta->ar_meta_keywords = ($request->ar_meta_keyword) ? $request->ar_meta_keyword : NULL ;

			$meta->en_meta_title = ($request->en_meta_title) ? $request->en_meta_title : $request->en_title;
			$meta->ar_meta_title = ($request->ar_meta_title) ? $request->ar_meta_title : $request->ar_title;

			$meta->en_meta_desc = ($request->en_meta_decp) ? $request->en_meta_decp : str_limit(strip_tags($request->en_description),'240') ;
			$meta->ar_meta_desc = ($request->ar_meta_decp) ? $request->ar_meta_decp : str_limit(strip_tags($request->ar_description),'240') ;

			if($request->type == 'events'){
			$meta->en_location = $request->en_location;
			$meta->ar_location = $request->ar_location;
			
			$edate = $request->event_date;
			$meta->event_date = $request->event_date;
			$meta->event_date_on = date("Y-m-d", strtotime($request->event_date));
			//$meta->event_date = date("Y-m-d", strtotime($edate));
			}

			if($request->type == 'news'){
			$pubdate = $request->news_date;
			$meta->news_date = date("Y-m-d", strtotime($pubdate));	
			}
			if($request->type == 'publications'){
			$meta->pubcurl = $request->pubcurl;	
			$meta->flipname = $request->flipname;
			$meta->publication_date = $request->publication_date;	
			}
            
			if($request->type == 'video_blogs'){
				$meta->video_url = $request->video_url;
			}
			if($request->type == 'video_gallery'){
				$meta->video_url = $request->video_url;
				$meta->video_date	= $request->video_date;
			}

            if($request->type == 'gallery'){
            $meta->image_date	= $request->image_date;
            }
          
			$meta->save();
			if($request->id){
                return response()->json($this->status_update,200); 
			}else{
				return response()->json($this->status,200); 
			}

	        }
	

		public function category_validator(array $data){
			return  Validator::make($data, 
				[
					'en_name' => 'required|string|max:255|unique:content_category,en_name,'.$data['id'],
					'ar_name' => 'required|string|max:255|unique:content_category,ar_name,'.$data['id']
				],[
                 'en_name.required' => 'Please Enter Category',
                 'ar_name.required' => ' يرجى ادخال الفئة  ',
                ]);
		}

		public function content_validator(array $data){
			if($data['type'] == 'blogs'){
				return Validator::make($data, ['title' => 'required|string|max:255','blog_content'=>'required','category'=>'required','f_image'=>'nullable|image|mimes:jpg,jpeg,png|max:2000'],['name.required'=>'Please Enter Title','blog_content.required'=>'Content Required','category'=>'Please Enter Category يرجى ادخال الفئة   ',
                  'f_image.mimes' => 'Only jpeg,png images are allowed',
                  'f_image.image' => 'Featured Image Must be in Image format',
                  'f_image.max' => 'Sorry! Maximum allowed size for an image is 2MB',]);
			}

			if($data['type'] == 'video_blogs'){
				return Validator::make($data, ['title' => 'required|string|max:255','video_blog_content'=>'required','category'=>'required','f_image'=>'nullable|image|mimes:jpg,jpeg,png|max:2000'],['name.required'=>'Please Enter Title','video_blog_content.required'=>'Content Required','category'=>' Please Enter Category يرجى ادخال الفئة   ',
                   'f_image.mimes' => 'Only jpeg,png images are allowed',
                  'f_image.image' => 'Featured Image Must be in Image format',
                  'f_image.max' => 'Sorry! Maximum allowed size for an image is 2MB',]);
			}

			if($data['type'] == 'news'){
				return Validator::make($data, ['en_title' => 'required|string|max:255','ar_title' => 'required|string|max:255','en_news_content'=>'required','ar_news_content'=>'required','category'=>'required','f_image'=>'nullable|image|mimes:jpg,jpeg,png|max:2100','news_date'=>'required'],['en_title.required'=>'Please Enter Title','ar_title.required'=>'يرجى إدخال الموضوع  ', 'en_news_content.required'=>'Content Required','ar_news_content'=>' المحتوى المطلوب   ','category'=>'Please Enter Category يرجى ادخال الفئة  ',
				  'news_date.required' => 'Publish Date Required  تاريخ النشر مطلوب    ',
				  'ar_news_content.required' => ' المحتوى المطلوب  ',
				  'category.required' => 'Category required  الفئة المطلوبة   ',
                  'f_image.mimes' => 'Only jpeg,png images are allowed',
                  'f_image.image' => 'Featured Image Must be in Image format',
                  'f_image.max' => 'Sorry! Maximum allowed size for an image is 2MB',]);
			}

			if($data['type'] == 'events'){
				return Validator::make($data, ['en_title' => 'required|string|max:255','ar_title' => 'required|string|max:255','en_event_content'=>'required','ar_event_content'=>'required','category'=>'required','f_image'=>'nullable|image|mimes:jpg,jpeg,png|max:2100','event_date'=>'required','en_location'=>'required','ar_location'=>'required'],['en_title.required'=>'Please Enter Title','ar_title.required'=>'يرجى إدخال الموضوع  ','en_event_content.required'=>'Content Required','ar_event_content.required'=>'المحتوى مطلوب   ','category.required'=>' Please Enter Category يرجى ادخال الفئة   ',
                  'event_date.required' => 'Event Date Required',
                  'en_location.required' => 'Location Required',
                  'ar_location.required' => ' الموقع مطلوب   ',
                  'f_image.mimes' => 'Only jpeg,png images are allowed',
                  'f_image.mimes' => 'Only jpeg,png images are allowed',
                  'f_image.image' => 'Featured Image Must be in Image format',
                  'f_image.max' => 'Sorry! Maximum allowed size for an image is 2MB',]);
			}

			if($data['type'] == 'ruler'){
				return Validator::make($data, ['en_title' => 'required|string|max:255','ar_title' => 'required|string|max:255','en_public_content'=>'required','ar_public_content'=>'required','r_image'=>'nullable|image|mimes:jpg,jpeg,png|max:2100'],['en_title.required'=>'Please Enter Title','ar_title.required'=>'يرجى إدخال الموضوع  ','en_public_content.required'=>'Content Required','ar_public_content.required'=>'المحتوى مطلوب   ',
                  'r_image.mimes' => 'Only jpeg,png images are allowed',
                  'r_image.mimes' => 'Only jpeg,png images are allowed',
                  'r_image.image' => 'Featured Image Must be in Image format',
                  'r_image.max' => 'Sorry! Maximum allowed size for an image is 2MB',]);
			}

			if($data['type'] == 'gallery'){
				if($data['id']==''){
                   $getdata = Validator::make($data, 
					['en_title' => 'required|string|max:255',
					'ar_title' => 'required|string|max:255',
					'category'=>'required',
					'image_date' => 'required',
					'gimage'=>'required',
					'gimage.*'=>'image|mimes:jpg,jpeg,png|max:2100'
				    ],
					[
					'en_title.required'=>'Please Enter Title',
					'ar_title.required'=>'يرجى إدخال الموضوع  ',
					'event_content.required'=>'Content Required',
					'category'=>' Please Enter Category يرجى ادخال الفئة ',
					'image_date'=>' Publish Date Required يرجى ادخال الفئة ',
					'gimage.required' => 'Featured Image required',
                    'gimage.*.mimes' => 'Only jpeg,png images are allowed',
                    'gimage.*.image' => 'Featured Image Must be in Image format',
                    'gimage.*.max' => 'Sorry! Maximum allowed size for an image is 2MB'
                    ]);
				    }else{
				    	
					$tes = Meta::select('*')
					->where('id','=',$data['meta_id'])
					->where('gallery','LIKE', '%[]%')
					->first();
					
					if(count($tes) > 0){
                    $getdata = Validator::make($data, 
					['en_title' => 'required|string|max:255',
					'ar_title' => 'required|string|max:255',
					'category'=>'required',
					'image_date' => 'required',
					'gimage'=>'required',
					'gimage.*'=>'image|mimes:jpg,jpeg,png|max:2100'
				    ],
					[
					'en_title.required'=>'Please Enter Title',
					'ar_title.required'=>'يرجى إدخال الموضوع  ',
					'event_content.required'=>'Content Required',
					'category'=>' Please Enter Category يرجى ادخال الفئة ',
					'image_date'=>' Publish Date Required يرجى ادخال الفئة ',
					'gimage.required' => 'Featured Image required',
                    'gimage.*.mimes' => 'Only jpeg,png images are allowed',
                    'gimage.*.image' => 'Featured Image Must be in Image format',
                    'gimage.*.max' => 'Sorry! Maximum allowed size for an image is 2MB'
                    ]);
					}else{
					$getdata = Validator::make($data, 
					['en_title' => 'required|string|max:255',
					'ar_title' => 'required|string|max:255',
					'category'=>'required',
					'image_date' => 'required',
					'gimage.*'=>'image|mimes:jpg,jpeg,png|max:2100'
				    ],
					[
					'en_title.required'=>'Please Enter Title',
					'ar_title.required'=>'يرجى إدخال الموضوع  ',
					'event_content.required'=>'Content Required',
					'image_date'=>' Publish Date Required يرجى ادخال الفئة ',
					'category'=>' Please Enter Category يرجى ادخال الفئة ',
                    'gimage.*.mimes' => 'Only jpeg,png images are allowed',
                    'gimage.*.image' => 'Featured Image Must be in Image format',
                    'gimage.*.max' => 'Sorry! Maximum allowed size for an image is 2MB'
                    ]);
					}
                   
				}
				return $getdata;
			    }

			   if($data['type'] == 'video_gallery'){
			   	$videoimg = Content::select('*')
					->where('id','=',$data['id'])
					->whereNotNull('image')
					->first();
				
					if($data['id']==''){
					return Validator::make($data, 
						['en_title' => 'required|string|max:255',
						'ar_title' => 'required|string|max:255',
						'category'=>'required',
						'video_date' => 'required',
						'video_url' => 'required|active_url',
						'vgimage'=>'required|image|mimes:jpg,jpeg,png,bmp|max:2100:'
					    ],
						[
						'en_title.required'=>'Please Enter Title',
						'ar_title.required'=>'يرجى إدخال الموضوع  ',
						'category.required'=>' Please Enter Category يرجى ادخال الفئة ',
						'video_date.required'=>' Publish Date Requiredيرجى ادخال الفئة ',
						'video_url.required' => 'Please Enter Video URL  الرجاء إدخال رابط الفيديو',
						'vgimage.required' => 'Featured Image required صورة مميزة المطلوبة   ',
	                    'vgimage.mimes' => 'Only jpeg,png and bmp images are allowed',
	                    'vgimage.image' => 'Featured Image Must be in Image format',
	                    'vgimage.max' => 'Sorry! Maximum allowed size for an image is 2MB'
	                    ]);
			            }else{
				      	 if(count($videoimg) > 0){
                            return Validator::make($data, 
							['en_title' => 'required|string|max:255',
							'ar_title' => 'required|string|max:255',
							'category'=>'required',
							'video_date' => 'required',
							'video_url' => 'required|active_url',
							'vgimage'=>'image|mimes:jpg,jpeg,png,bmp|max:2100:'
						    ],
							[
							'en_title.required'=>'Please Enter Title',
							'ar_title.required'=>'يرجى إدخال الموضوع  ',
							'category.required'=>' Please Enter Category يرجى ادخال الفئة ',
							'video_date.required'=>' Publish Date Required يرجى ادخال الفئة ',
							'video_url.required' => 'Please Enter Video URL',
		                    'vgimage.mimes' => 'Only jpeg,png and bmp images are allowed',
		                    'vgimage.image' => 'Featured Image Must be in Image format',
		                    'vgimage.max' => 'Sorry! Maximum allowed size for an image is 2MB'
		                    ]);
				      	 }else{
                            return Validator::make($data, 
							['en_title' => 'required|string|max:255',
							'ar_title' => 'required|string|max:255',
							'category'=>'required',
							'video_date' => 'required',
							'video_url' => 'required|active_url',
							'vgimage'=>'required|image|mimes:jpg,jpeg,png,bmp|max:2100:'
						    ],
							[
							'en_title.required'=>'Please Enter Title',
							'ar_title.required'=>'يرجى إدخال الموضوع  ',
							'category.required'=>' Please Enter Category يرجى ادخال الفئة ',
							'video_date.required'=>' Publish Date Required يرجى ادخال الفئة ',
							'video_url.required' => 'Please Enter Video URL',
							'vgimage.required' => 'Featured Image required',
		                    'vgimage.mimes' => 'Only jpeg,png and bmp images are allowed',
		                    'vgimage.image' => 'Featured Image Must be in Image format',
		                    'vgimage.max' => 'Sorry! Maximum allowed size for an image is 2MB'
		                    ]);
				      	 }
			        }
			        }

			    if($data['type'] == 'publications'){
                if($data['id']==''){
				return Validator::make($data, ['en_title' => 'required|string|max:255', 'publication_date' =>'required','ar_title' => 'required|string|max:255','en_public_content'=>'required','ar_public_content'=>'required','category'=>'required','fb_image'=>'required|image|mimes:jpg,jpeg,png|max:2100'],['en_title.required'=>'Please Enter Title','ar_title.required'=>'يرجى إدخال الموضوع  ', 'en_public_content.required'=>'Content Required','ar_public_content.required'=>' المحتوى المطلوب   ','category.required'=>'Please Enter Category يرجى ادخال الفئة  ',
                  'publication_date.required' => 'Publish Date Required',
                  'fb_image.required' => 'Featured Image Required',
                  'fb_image.mimes' => 'Only jpeg,png images are allowed',
                  'fb_image.image' => 'Featured Image Must be in Image format',
                  'fb_image.max' => 'Sorry! Maximum allowed size for an image is 2MB',]);
			   }else{
			   	$tes = Meta::select('*')
					->where('id','=',$data['meta_id'])
					->where('pdf','LIKE', '%[]%')
					->first();
                if(count($tes) > 0){
                   return Validator::make($data, ['en_title' => 'required|string|max:255','publication_date' =>'required','ar_title' => 'required|string|max:255','en_public_content'=>'required','ar_public_content'=>'required','category'=>'required','fb_image'=>'nullable|image|mimes:jpg,jpeg,png|max:2100'],['en_title.required'=>'Please Enter Title','ar_title.required'=>'يرجى إدخال الموضوع  ', 'en_public_content.required'=>'Content Required','ar_public_content.required'=>' المحتوى المطلوب   ','category.required'=>'Please Enter Category يرجى ادخال الفئة  ',
                 
                  'publication_date.required' => 'Publish Date Required',
                 
                  'fb_image.mimes' => 'Only jpeg,png images are allowed',
                  'fb_image.image' => 'Featured Image Must be in Image format',
                  'fb_image.max' => 'Sorry! Maximum allowed size for an image is 2MB',]);
                }else{
                   return Validator::make($data, ['en_title' => 'required|string|max:255','publication_date' =>'required','ar_title' => 'required|string|max:255','en_public_content'=>'required','ar_public_content'=>'required','category'=>'required','fb_image'=>'nullable|image|mimes:jpg,jpeg,png|max:2100','bookpdf.*'=>'mimes:pdf|max:10000'],['en_title.required'=>'Please Enter Title','ar_title.required'=>'يرجى إدخال الموضوع  ', 'en_public_content.required'=>'Content Required','ar_public_content.required'=>' المحتوى المطلوب   ','category.required'=>'Please Enter Category يرجى ادخال الفئة  ',
                  'bookpdf.*.mimes' => 'Only PDF files are allowed',
                  'publication_date.required' => 'Publish Date Required',
                  'fb_image.mimes' => 'Only jpeg,png images are allowed',
                  'fb_image.image' => 'Featured Image Must be in Image format',
                  'fb_image.max' => 'Sorry! Maximum allowed size for an image is 2MB',]);
                }


			   }
			}    

		    }

function uploadimage(Request $request){
    $CKEditor = $request->CKEditor;
    $funcNum = $request->CKEditorFuncNum;
    $message = $url = '';
    if ($request->hasFile('upload')) {
        $file = $request->file('upload');
        if ($file->isValid()) {
            $filename = $file->getClientOriginalName();
            $file->move(public_path().'/images/ckeditorimage/', $filename);
            $url = url('/images/ckeditorimage/'.$filename) ;
        } else {
            $message = 'An error occured while uploading the file.';
        }
    } else {
        $message = 'No file uploaded.';
    }
    return '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';

}

 function imageDel(Request $request){
          $del = Meta::where('content_id',$request->p)->first();
                  $ima =  json_decode($del->gallery);
                  unset($ima[$request->i]);
                  $decode =  array_values($ima);
                  $del->gallery = json_encode($decode);
                  $del->save();   
       return  response()->json(['success'=>'Images Deleted']);     
      }

function pdfDel(Request $request){
	 $del = Meta::where('content_id',$request->p)->first();
                  $ima =  json_decode($del->pdf);
                  unset($ima[$request->i]);
                  $decode =  array_values($ima);
                  $del->pdf = json_encode($decode);
                  $del->save();   
       return  response()->json(['success'=>'PDF File Deleted']);   
}  
		       

}
