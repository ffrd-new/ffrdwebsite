<?php

namespace App\Http\Controllers\Api;
use App\Frontend;
use Illuminate\Http\Request;
use Validator;
use Mail;
use Redirect;
use App\Content;
use App\Category;
use App\Type;
use Helper; 
use App;

class DemoApiController 
{

	private $apitoken;
	

	public function listing(Request $request){
		if($request->is('api/*')){
			$type = $request->type;
			$limit = $request->limit;
			$cat_id = $request->cat_id;

				$data['type'] = Category::typeName($type);
		        $data['categorylist'] = Category::FCate($type);
		        $data['contentlist']= Content::gcontent($type,$limit,$cat_id);
				
				return response()->json(['status'=>200,'messages'=>"listing data",'data'=>$data]);
			
		}
	}

	public function contentdetails(Request $request){
		if($request->is('api/*')){
			$cont_id = $request->cont_id;

				$content=Content::find($cont_id);
				$dataaa= Content::gconts($cont_id,$content->category_type_id);
				$data['contentdetails'] =$dataaa;
				if($content->category_type_id == 8){
					$gall =	$dataaa[0]->gallery;
					foreach (json_decode($gall) as $key => $value) {
						$da[]  = url('/images/gallery/'.$value);
					}
					$data['gallery']  = $da;
				}	
				elseif($content->category_type_id==10){
					$pdf = $dataaa[0]->pdf;
					foreach (json_decode($pdf) as  $key => $value) {
						$daa[] = url('/images/publications/media/'.$value);
					}
					$data['pdf'] = $daa;
				}			

				return response()->json(['status'=>200,'messages'=>"listing details",'data'=>$data]);
		}		 
	}
}
			  