<?php

namespace App\Http\Controllers\Api;
use App\Frontend;
use Illuminate\Http\Request;
use Validator;
use Mail;
use Redirect;
use App\Content;
use App\Category;
use App\Type;
use Helper; 
use App;

class ListtypeController 
{
	public function listings(Request $request){

		if($request->is('api/*')){
			$type = $request->type;
			$limit = $request->limit;
			$cat_id = $request->cat_id;

				$data['type'] = Category::typeName($type);
		        $data['categorylist'] = Category::FCate($type);
		        $ccdate = date("Y/m/d");
		        $da= Content::gcontent($type,$limit,$cat_id,$ccdate);
		        $da->map(function ($value, $key) {
					if($value->gallery){
						$dataz  = json_decode($value->gallery);
						if(count($dataz) > 0){
						    $value->gallery =  url('/images/gallery/'.$dataz[0]);
						    
						}else{
							$value->gallery = null;
						}
						return $value;
					}

					$value->base_image = Helper::image_path_base($value->category_type_id).'/'.$value->image; 

					$value->thumb = Helper::image_path_base($value->category_type_id).'/thumb/'.$value->image; 
				});
				
				$data['contentlist'] = $da;
				return response()->json(['status'=>200,'messages'=>"listing data",'data'=>$data]);
			
		}
	}

	public function details(Request $request){
		if($request->is('api/*')){
		    
		    $daa =[];
			$cont_id = $request->cont_id;

				$content=Content::find($cont_id);
				$dataaa= Content::gconts($cont_id,$content->category_type_id);
				$data['contentdetails'] =$dataaa;
				if($content->category_type_id == 8){
					$gall =	$dataaa[0]->gallery;
					foreach (json_decode($gall) as $key => $value) {
						$path[]  = url('/images/gallery/'.$value);
					}
				$data['gallery']  = $path;
				}
				elseif($content->category_type_id == 10){
					$pdf = $dataaa[0]->pdf;
					foreach (json_decode($pdf) as  $key => $value) {
						$daa[] = url('/images/publications/media/'.$value);
					}
				$data['pdf'] = $daa;
				}	

				return response()->json(['status'=>200,'messages'=>"listing details",'data'=>$data]);
		}	 
	}
}