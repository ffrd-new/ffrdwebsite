<?php
namespace App\Helpers;
use Image;
use DB;
use Session;
use Helper;
use Auth;
use App\Slot;
use Config;
use App\Doctor;
use Carbon\Carbon;
use App\Category;
use APP\Content;
use App\Appoinment;
use App\Usersettings;
use APP\Meta;

class General {
	public static function table_head_lower($str) {
 	   return strtolower(str_replace("_", " ", $str));
	}

     public static function getRolename($id){
     $data = DB::table('users')->where('users.id',$id)
         ->join('user_has_roles as usr','usr.user_id','users.id')
         ->join('roles','roles.id','usr.role_id')
         ->select('users.id','users.email','usr.role_id','roles.name')
         ->where('users.status',1)
         ->groupby('users.id')
         ->get();
      return $data;

     }

	public static function table_head_first($str) {
    	return ucwords(str_replace("_", " ", $str));
	}


	public static function MakeTableView($value){
	
		$tit = $value;
		$value=str_slug($value);
        $tableurl = url('/admin/'.General::table_head_lower($value).'/list');
        $saveurl = url('/admin/'.General::table_head_lower($value).'/save');
        $viewurl = url('/admin/'.General::table_head_lower($value).'/');
        $delurl = url('/admin/'.General::table_head_lower($value).'/drop');
        $pages = ['title'=>$tit,'basemenu'=>'Cars','currenmenu'=>'CarType Listing' , 'subtitle'=>'Listing','content'=>$value,'id'  =>'','type'=>strtolower(str_replace(' ', '_', $tit)),'modal_name'=>'Add'.$value,'table_url'=>$tableurl,'save_url'=>$saveurl,'view_url'=>$viewurl,'delete_url'=>$delurl];
		return $pages;
	}

	public static function TableHeading($heading){

   
    	foreach($heading as $head){
                $table[] = ['data'=>General::table_head_lower($head),'name'=>General::table_head_lower($head)];
        }

        return json_encode($table);
	}


	public static function cms_page($data,$lang = 'en'){

		if($data == 'blogs'){
			$categoryheading = ['Id','Title','Category','Status','Publish on','Action'];
			$page = 'blogs.blogs';
		    $fields = [];
			$datafields = ['id as sno','name as title'];
			$categorypages = General::MakeTableView('Blogs');
       		$categorytable = General::TableHeading($categoryheading);
 		}
		if($data == 'blog_category'){
			$categoryheading = ['id','title','type','status','action'];
			$page = 'blogs.blog_category';
			$fields = ['id','name as title' ,'is_parent','status'];
			$categorypages = General::MakeTableView('Blog category');
       		$categorytable = General::TableHeading($categoryheading);
		}
		if($data == 'video_category'){
			$categoryheading = ['id','title','type','status','action'];
			$page = 'blogs.blog_category';
			$fields = ['id','name as title' ,'is_parent','status'];

			$categorypages = General::MakeTableView('Video category');
       		$categorytable = General::TableHeading($categoryheading);
		}

			if($data == 'video_blogs'){
			$categoryheading = ['id','title','category','status','publish on','action'];
			$page = 'blogs.video_blogs';
			$fields = ['id','name as title' ,'is_parent','status'];
			$categorypages = General::MakeTableView('Video Blogs');
       		$categorytable = General::TableHeading($categoryheading);
		}
		if($data == 'news_category'){
			$categoryheading = ['title','type','status','action'];
			$page = 'news.news_category';
			if($lang == 'ar'){
			    $fields = ['id','ar_name as title','is_parent','status'];
			}else{
				$fields = ['id','en_name as title','is_parent','status'];
			}
			$categorypages = General::MakeTableView('News category');
       		$categorytable = General::TableHeading($categoryheading);
		}
		
		if($data == 'news'){
			$categoryheading = ['Title','Category','Status','Publish Date','Action'];
			$page = 'news.news';
			if($lang == 'ar'){
            $fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.ar_name as category','c.id as category_id','content_detail.ar_title as title','content_detail.ar_description as blog_content','m.ar_meta_title','m.ar_meta_desc','m.ar_meta_keywords','m.news_date'];
			}else{
			$fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as category','c.id as category_id','content_detail.en_title as title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc','m.en_meta_keywords','m.news_date'];	
			}
			
			$categorypages = General::MakeTableView('News');
       		$categorytable = General::TableHeading($categoryheading);
 		}

 		if($data == 'events_category'){
			$categoryheading = ['title','status','action'];
			$page = 'events.events_category';
			if($lang == 'ar'){
				$fields = ['id','ar_name as title' ,'status'];
			}else{
				$fields = ['id','en_name as title' ,'status'];
			}
			$categorypages = General::MakeTableView('Events category');
       		$categorytable = General::TableHeading($categoryheading);
		}
		if($data == 'events'){
			$categoryheading = ['Title','Category','Status','Event Date','Action'];
			$page = 'events.events';
		    //$fields = ['id as sno','arname as title'];
		    if($lang == 'ar'){
            $fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on ','c.ar_name as category','c.id as category_id','content_detail.ar_title as title','content_detail.ar_description as blog_content','m.ar_meta_title','m.ar_meta_desc','m.ar_meta_keywords','m.event_date as event date'];
			}else{
			$fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as category','c.id as category_id','content_detail.en_title as title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc','m.en_meta_keywords','m.event_date as event date'];	
			}
			//$datafields = ['id as sno','name as title'];
			$categorypages = General::MakeTableView('Events');
       		$categorytable = General::TableHeading($categoryheading);
 		}
 		if($data == 'gallery_category'){
			$categoryheading = ['title','status','action'];
			$page = 'gallery.gallery_category';
			if($lang == 'ar'){
				$fields = ['id','ar_name as title' ,'status'];
			}else{
				$fields = ['id','en_name as title' ,'status'];
			}
			
			$categorypages = General::MakeTableView('Gallery category');
       		$categorytable = General::TableHeading($categoryheading);
		}

		if($data == 'gallery'){
			$categoryheading = ['Title','Category','Status','Publish Date','Action'];
			$page = 'gallery.gallery';
		    if($lang == 'ar'){
            $fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.ar_name as category','c.id as category_id','content_detail.ar_title as title','content_detail.ar_description as blog_content','m.ar_meta_title','m.ar_meta_desc','m.ar_meta_keywords','m.gallery','m.image_date'];
			}
			else
			{
			$fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as category','c.id as category_id','content_detail.en_title as title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc','m.en_meta_keywords','m.gallery','m.image_date'];
		    }
			$datafields = ['id as sno','name as title'];
			$categorypages = General::MakeTableView('Gallery');
       		$categorytable = General::TableHeading($categoryheading);
 		}

 		if($data == 'video_gallery'){
			$categoryheading = ['Title','Category','Status','Publish Date','Action'];
			$page = 'gallery.videogallery';
		    if($lang == 'ar'){
            $fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.ar_name as category','c.id as category_id','content_detail.ar_title as title','content_detail.ar_description as blog_content','m.ar_meta_title','m.ar_meta_desc','m.ar_meta_keywords','m.gallery','m.video_url','m.video_date'];
			}else{
			$fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as category','c.id as category_id','content_detail.en_title as title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc','m.en_meta_keywords','m.gallery','m.video_url','m.video_date'];}
			$datafields = ['id as sno','name as title'];
			$categorypages = General::MakeTableView('Video Gallery');
       		$categorytable = General::TableHeading($categoryheading);
 		}

 		if($data == 'publications'){
			$categoryheading = ['Title','Category','Status','Publish Date','Action'];
			$page = 'publications.publications';
			if($lang == 'ar'){
            $fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.ar_name as category','c.id as category_id','content_detail.ar_title as title','content_detail.ar_description as blog_content','m.ar_meta_title','m.ar_meta_desc','m.ar_meta_keywords','m.publication_date'];
			}else{
			$fields = ['content_detail.id','content_detail.image','content_detail.status','content_detail.publish_date as publish_on','c.en_name as category','c.id as category_id','content_detail.en_title as title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc','m.en_meta_keywords','m.publication_date'];	
			}
			$categorypages = General::MakeTableView('Publications');
       		$categorytable = General::TableHeading($categoryheading);
 		}

 		if($data == 'publications_category'){
       		$categoryheading = ['title','status','action'];
			$page = 'publications.publications_category';
			if($lang == 'ar'){
			    $fields = ['id','ar_name as title','is_parent','status'];
			}else{
				$fields = ['id','en_name as title','is_parent','status'];
			}
			$categorypages = General::MakeTableView('Publications Category');
       		$categorytable = General::TableHeading($categoryheading);
 		}


 		if($data == 'user'){
 			$categoryheading = ['email','role','publish on','status','action'];
			$page = 'settings.user.user';
			if($lang == 'ar'){
				$fields = ['id','ar_name as title' ,'status'];
			}else{
				$fields = ['id','en_name as name','role','status'];
			}
			
			$categorypages = General::MakeTableView('User');
       		$categorytable = General::TableHeading($categoryheading);
 		}


 		if($data == 'ruler'){
           $categoryheading = ['title','status','action'];
           $page = 'ruler.ruler';
           if($lang == 'ar'){
           	$fields = ['content_detail.id','content_detail.image','content_detail.publish_date as publish_on','content_detail.status','c.ar_name as category','c.id as category_id','content_detail.ar_title as title','content_detail.ar_description as blog_content','m.ar_meta_title','m.ar_meta_desc','m.ar_meta_keywords'];
           }else{
           	$fields = ['content_detail.id','content_detail.image','content_detail.publish_date as publish_on','content_detail.status','c.en_name as category','c.id as category_id','content_detail.en_title as title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc','m.en_meta_keywords'];
           }
           $categorypages = General::MakeTableView('Ruler');
           $categorytable = General::TableHeading($categoryheading);
 		}

 		if($data == 'news_highlights'){
           $categoryheading = ['title','status','action'];
           $page = 'news.news_highlights';
           if($lang == 'ar'){
           	$fields = ['content_detail.id','content_detail.image','content_detail.publish_date as publish_on','content_detail.status','c.ar_name as category','c.id as category_id','content_detail.ar_title as title','content_detail.ar_description as blog_content','m.ar_meta_title','m.ar_meta_desc','m.ar_meta_keywords'];
           }else{
           	$fields = ['content_detail.id','content_detail.image','content_detail.publish_date as publish_on','content_detail.status','c.en_name as category','c.id as category_id','content_detail.en_title as title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc','m.en_meta_keywords'];
           }

           $categorypages = General::MakeTableView('News Highlights');
           $categorytable = General::TableHeading($categoryheading);
 		}

	    return ['heading'=>$categoryheading,'page'=>$categorypages,'table'=>$categorytable,'tablename'=>rand(10,888),'pages'=>$page,'fields'=>$fields,'lang'=>$lang];
	    }

		public static function cmsstatus($status)
		{	
			if($status == "true"){
				return 1;
			}
			return 0;
		}

		public static function cmstype($type)
		{
			if($type == 'blogs' || $type == 'blog_category'){
				return 1;
			}
			if($type == 'news' || $type == 'news_category'){
				return 2;
			}
			if($type == 'events' || $type == 'events_category'){
				return 3;
			}

			if($type == 'video_category' || $type == 'video_blogs'){
				return 6;
			}

			if($type == 'gallery_category' || $type == 'gallery' || $type == 'video_gallery'){
				return 8;
			}

			if($type == 'publications' || $type == 'publications_category'){
				return 10;
			}

			if($type == 'ruler'){
               return 11;
			}


		}

	    public static function cms_category_type($type)
		{
			if(!$type){
			return '<span class="label label-success">Parent</span>';
			}
			else
			{
			return '<span class="label label-default">Child</span>';
			}
		}

		   public static function cms_category_status($type)
		{
			if($type){
			return '<span class="label label-warning">Active</span>';
			}
			else
			{
			return '<span class="label label-danger">In active</span>';
			}
		}

		public static function newsdate($newsdate,$publishdate,$pub_date,$image_date,$video_date){

          if(isset($pub_date)){
            return $pub_date;
          }

          if(isset($image_date)){
            return $image_date;
          }
          if(isset($video_date)){
            return $video_date;
          }
          if(isset($newsdate)){
              if($newsdate != null){
			   return $newsdate;
			   }else{
			   	  return $publishdate;
			   } 
          }else{
             return $publishdate;
          }
		 
		}

		public static function cms_content_status($type)
		{
			if($type){
			return '<span class="label label-warning">Publish</span>';
			}
			else
			{
			return '<span class="label label-danger">In active</span>';
			}
		}

		public static function cms_category_colums($data,$lang){
				if($data == 'blog_category'){
					$datax = General::cms_page($data);
					return $datax['fields'];
				}

				if($data == 'video_category'){
					$datax = General::cms_page($data);
					return $datax['fields'];
				}
				if($data == 'news_category'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				
				if($data == 'events_category'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}

				if($data == 'gallery_category'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				if($data == 'news'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				if($data == 'events'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				if($data == 'gallery'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				if($data == 'video_gallery'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				if($data == 'publications'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				if($data == 'publications_category'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				if($data == 'ruler'){
					$datax = General::cms_page($data,$lang);
					return $datax['fields'];
				}
				

				
  
		}

		public static function Category_Type($type){
			$data = Category::find($type);
			General::content_path($data->type_id);
			return $data->type_id;
		}

		public static function content_path($type){
			if($type == 1){
				return public_path('images/blog');
			}
			if($type == 6){
				return public_path('images/video');
			}
			if($type == 2){
				return public_path('images/news');
			}
			if($type == 3){
				return public_path('images/events');
			}
			if($type == 10){
				return public_path('images/publications');
			}
			if($type == 11){
                return public_path('images/ruler');
			}
		}

		public static function image($type,$photo,$size,$thumb = '260,150'){
          if($type == 8){
          $destinationPath = public_path('images/videogallery') ;
          }else{
          	$destinationPath = General::content_path($type);
          }
        if($photo)
		{
        $thumb = explode(',',$thumb);
                $imagename = time().rand().'.'.$photo->getClientOriginalExtension(); 
                 $img = Image::make($photo->getRealPath());
                 if($size){
                 	$size = explode(',',$size);
            	    $img->resize($size[0],$size[1]);
                 }
                 $img->save($destinationPath.'/'.$imagename,80);
                 $thumbnail = Image::make($photo->getRealPath());
                 $thumbnail = $thumbnail->resize($thumb[0],$thumb[1]);
  				 $thumbnail->save($destinationPath.'/thumb/'.$imagename,80);
                return $imagename;
			}
		}

		public static function admin_prefix($url){

			return url('/admin/'.$url);

		}

		public static function get_column_names($type){
			if($type== 'blogs'){
				$columns=['content_detail.id as id','content_detail.image as f_image','content_detail.status','c.en_name as category','c.id as category_id','content_detail.title','content_detail.en_description as blog_content','m.en_meta_title','m.en_meta_desc as meta_decp','m.en_meta_keywords as meta_keyword','m.video_url as video_url','content_detail.publish_date','m.slug'];
			}
			if($type== 'video_blogs'){
				$columns=['content_detail.id as id','content_detail.image as f_image','content_detail.status','c.en_name as category','c.id as category_id','content_detail.title','content_detail.en_description as video_blog_content','m.en_meta_title','m.en_meta_desc as meta_decp','m.en_meta_keywords as meta_keyword','m.video_url as video_url','m.slug'];
			}
			if($type== 'news'){
				$columns=['content_detail.id as id','content_detail.image as f_image','content_detail.status','c.en_name as category','c.id as category_id','content_detail.en_title','content_detail.ar_title','content_detail.en_description as en_news_content','content_detail.ar_description as ar_news_content','m.en_meta_title','m.en_meta_desc as en_meta_decp','m.en_meta_keywords as en_meta_keyword','m.en_slug','m.ar_meta_title','m.ar_meta_desc as ar_meta_decp','m.ar_meta_keywords as ar_meta_keyword','m.ar_slug','content_detail.publish_date','m.news_date','content_detail.news_gallery_id','content_detail.news_gallery_name','content_detail.news_video_id','content_detail.news_video_name'];
			}

			if($type== 'events'){
				$columns=['content_detail.id as id','content_detail.image as f_image','content_detail.status','c.en_name as category','c.id as category_id','content_detail.en_title','content_detail.en_description as en_event_content','m.en_meta_title','m.en_meta_desc as en_meta_decp','m.en_meta_keywords as en_meta_keyword','m.en_slug','content_detail.ar_title','content_detail.ar_description as ar_event_content','m.ar_meta_title','m.ar_meta_desc as ar_meta_decp','m.ar_meta_keywords as ar_meta_keyword','m.ar_slug','m.event_date','m.ar_location','m.en_location','m.event_date as setdat'];
			}

			if($type== 'gallery'){
				$columns=['content_detail.id as id','content_detail.image as f_image','content_detail.status','c.en_name as category','c.id as category_id','content_detail.en_title','content_detail.en_description as en_event_content','m.en_meta_title','m.en_meta_desc as en_meta_decp','m.en_meta_keywords as en_meta_keyword','m.en_slug','m.gallery','content_detail.ar_title','content_detail.ar_description as ar_event_content','m.ar_meta_title','m.ar_meta_desc as ar_meta_decp','m.ar_meta_keywords as ar_meta_keyword','m.ar_slug','m.gallery','m.content_id','m.id as meta_id','m.image_date'];
			}

			if($type== 'video_gallery'){
				$columns=['content_detail.id as id','content_detail.image as vgimage','content_detail.status','c.en_name as category','c.id as category_id','content_detail.en_title','content_detail.en_description as en_event_content','m.en_meta_title','m.en_meta_desc as en_meta_decp','m.en_meta_keywords as en_meta_keyword','m.en_slug','m.gallery','content_detail.ar_title','content_detail.ar_description as ar_event_content','m.ar_meta_title','m.ar_meta_desc as ar_meta_decp','m.ar_meta_keywords as ar_meta_keyword','m.ar_slug','m.gallery','m.video_url','m.video_date'];
			}

			if($type== 'publications'){
				$columns=['content_detail.id as id','content_detail.image as fb_image','content_detail.status','c.en_name as category','c.id as category_id','content_detail.en_title','content_detail.en_description as en_public_content','content_detail.ar_description as ar_public_content','m.en_meta_title','m.en_meta_desc as en_meta_decp','m.en_meta_keywords as en_meta_keyword','m.en_slug','m.gallery','content_detail.ar_title','m.ar_meta_title','m.ar_meta_desc as ar_meta_decp','m.ar_meta_keywords as ar_meta_keyword','m.ar_slug','m.pdf','m.content_id','m.id as meta_id','m.pubcurl','m.flipname','m.publication_date'];
			}
			if($type== 'ruler'){
				$columns=['content_detail.id as id','content_detail.image as r_image','content_detail.status','c.en_name as category','c.id as category_id','content_detail.en_title','content_detail.en_description as en_public_content','content_detail.ar_description as ar_public_content','m.en_meta_title','m.en_meta_desc as en_meta_decp','m.en_meta_keywords as en_meta_keyword','m.en_slug','m.gallery','content_detail.ar_title','m.ar_meta_title','m.ar_meta_desc as ar_meta_decp','m.ar_meta_keywords as ar_meta_keyword','m.ar_slug','m.pdf','m.content_id','m.id as meta_id'];
			}
			return $columns;
		}
		public static function imageCheck($image){

	if($image){
		return $image;
	}
	else{
		return "noimage.jpg";
	}
}

public static function seo($title = 0,$meta= 0,$image=0,$meta_title=0,$meta_keyword=0,$type){

    if(isset($title)){
        $title=$title;
    }
    else{
       /* $title = DB::table('settings')->where(array('setting_name' => 'app_title'))->first();*/
       $title="";
    }

    if(isset($meta)){
        $meta=$meta;
    }
    else{
       /* $desc = DB::table('settings')->where(array('setting_name' => 'app_desc'))->first();*/
        $meta="";
    }

    if(isset($image)){
     $image = url('/images/'.$type.'/')."/".$image;
    }

    else{
        /* $image = DB::table('settings')->where(array('setting_name' => 'logo_path'))->first();*/
        $image="latlontech.com/demo/appledental/public/images/logo2.png";
    }

    if(isset($meta_title)){
        $meta_title=$meta_title;
    }
    else{
        $meta_title=$title;
    }

    if(isset($meta_keyword)){
        $meta_keyword=$meta_keyword;
    }
    else{
        $meta_keyword="";
    }

	$data = ['en_title'=>$title,'meta'=>$meta,'image'=>$image,'meta_title'=>$meta_title,'meta_keyword'=>$meta_keyword];

    return $data;
    	}

    public static function generateslug($blogid){

    		$data=DB::table('content_meta')
    			->where('content_id',$blogid)
    			->first();
    			if(Config::get('app.locale') == 'en'){
    				return $data->en_slug;
    			}
    			if(Config::get('app.locale') == 'ar')
    			{
    				return $data->ar_slug;
    			}	
    	   }

   public static function generateslugforpub($blogid){

    		$data=DB::table('content_detail')
    			->where('id',$blogid)
    			->first();
    			if(Config::get('app.locale') == 'en'){
    				return $data->en_title;
    			}
    			if(Config::get('app.locale') == 'ar')
    			{
    				return $data->ar_title;
    			}	
    	   }


    	public static function pretty_url($str){
        $url = General::convert_to_url_friendly($str);
        $turl = $url;
        $i = 0;
        while(1){
            $i++;
            $cnt = Meta::where('en_slug', $turl)->count();
            if($cnt != 0)
                $turl = $url.'-'.$i;
            else break;
        }
        return str_slug($turl);
    }
    
    public static function ar_pretty_url($str){
        //$url = General::ar_convert_to_url_friendly($str);
        $turl = $str;
        $i = 0;
        while(1){
            $i++;
            $cnt = Meta::where('ar_slug', $turl)->count();
            if($cnt != 0)
                $turl = $str.'-'.$i;
            else break;
        }
        return str_slug($turl);
    }

	public static function pretty_urlwithid($str,$id){
        $url = General::convert_to_url_friendly($str);
        $turl = $url;
        $i = 0;
        while(1){
            $i++;
            $cnt = Meta::where('en_slug', $turl)->where('content_id','!=',$id)->count();
            if($cnt != 0)
                $turl = $url.'-'.$i;
            else break;
        }
        return str_slug($turl);
    }
    public static function ar_pretty_urlwithid($str,$id){
        //$url = General::ar_convert_to_url_friendly($str);
        $turl = $str;
        $i = 0;
        while(1){
            $i++;
            $cnt = Meta::where('ar_slug', $turl)->where('content_id','!=',$id)->count();
            if($cnt != 0)
                $turl = $str.'-'.$i;
            else break;
        }
        return str_slug($turl);
    }

    public static function convert_to_url_friendly($str, $replace=array(), $delimiter='-') {
        if( !empty($replace) ) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[_|+ -]+/", $delimiter, $clean);
        return $clean;
    }

  public static function ar_convert_to_url_friendly($str, $replace=array(), $delimiter='-') {
        if( !empty($replace) ) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'utf-8//TRANSLIT', $str);
        $clean = preg_replace('/[^\w\s]+/u','' ,$clean);
        $cleann = preg_replace("/[^a-zA-Z0-9_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        return $clean;
    }


   public static function MultipleImages($img,$path,$id,$column){

   foreach ($img as $key=> $value) 
   { 
      
    $imagename = time().rand().'.'.$value->getClientOriginalExtension();               
    $images[] =   $imagename; 
    //      return  $key;
    $destinationPath = $path;
    $thumb_img = Image::make($value->getRealPath());
    $thumb_img->save($destinationPath.'/'.$imagename,80);

   }
   	if(!$id){
	   return  json_encode($images);
	}
	if($id){
	    $image =  Meta::where('id',$id)->first();
	    $a1 = $image->{$column};
	    if(count(json_decode($a1)) > 0){
	      $a2 = json_encode($images);
	      $a3 = array_merge(json_decode($a1),json_decode($a2));
	      return $images = json_encode($a3);
	  }
	  else{
	    return json_encode($images);
	}
	}
   }


public static function HomeGallery($cat){
        $gallery=Content::getfirstimage(8,$cat);
        if(count($gallery)>0){
        foreach($gallery as $gal){
            $result= $gal->gallery;
            if(count($result) > 0){
            foreach (json_decode($result) as $key => $value) {
				 if($key == 0){
				 $d = $value;
			 }
            }
        }
        }
    	}
    	else{
        	$d ="noimage.jpg";
        }
        return $d;     
    }


    public static function access_content($id){
    	 $authId = Auth::user()->id;
         $rname = Helper::getRolename($authId);
         $roleid = '';

	      foreach ($rname as $key => $value) {
	        $roleid = $value->role_id;
	      }

    	if($roleid == 1){
           return '<a data-act="ajax-modal"  data-id="'.$id.'" class="check btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
     <a data-act="ajax-del" data-id="'.$id.'" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>';
    	}else{
    		return '<a data-act="ajax-modal"  data-id="'.$id.'" class="disabled check btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> 
     <a data-act="ajax-del" data-id="'.$id.'" class="disabled btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>';
    	}
    	
    } 

     public static function BaseUrl($url){
		return url(General::SetLocaleUrl().$url);
    }

    public static function SetLocaleUrl(){
    	if(Session::has('locate')){
    		return Session::get('locate');
    	}
    	else{
    		return 'en';
    	}
    }

    public static function image_path_thumb($id){

        if($id == 2){
            return url('/images/news/thumb/');
        }
         if($id == 3){
            return url('/images/events/thumb/');
        }
         if($id == 8){
            return url('/images/videogallery/thumb/');
        }
         if($id == 10){
            return url('/images/publications/thumb/');
        }
        else{
            return url('/images/');
        }

    }

       public static function image_path_base($id){

        if($id == 2){
            return url('/images/news/');
        }
         if($id == 3){
            return url('/images/events/');
        }
        if($id == 8){
            return url('/images/videogallery/');
        }
         if($id == 10){
            return url('/images/publications/');
        }
        else{
            return url('/images/');
        }

    }

    public static function ArabicDate($date) {
	    $months = array("Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل", "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس", "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر");
	    $your_date = $date; // The Current Date
	    $en_month = date("M", strtotime($your_date));
	    foreach ($months as $en => $ar) {
	        if ($en == $en_month) { $ar_month = $ar; }
	    }

	    $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
	    $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
	    $ar_day_format = date('D',strtotime($your_date)); // The Current Day

	    $ar_day = str_replace($find, $replace, $ar_day_format);

	    header('Content-Type: text/html; charset=utf-8');
	    $standard = array("0","1","2","3","4","5","6","7","8","9");
	    $eastern_arabic_symbols = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
	    // $current_date = $ar_day.' '.date('d').' - '.$ar_month.' - '.date('Y');

	    $current_date = date('Y',strtotime($your_date)).' ,'.date('d',strtotime($your_date)).' '.$ar_month;
	    // $arabic_date = str_replace($standard , $eastern_arabic_symbols , $current_date);

	    // echo '<span dir="ltr">'.$current_date.'</span>';
	    // return;

	     $current_date = $ar_month.' '.date('d',strtotime($your_date)).', '.date('Y',strtotime($your_date));

	    return $current_date;
	}

	public static function ArabicDateMnth($date) {
	    $months = array("Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل", "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس", "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر");
	    $your_date = $date; // The Current Date
	    $en_month = date("M", strtotime($your_date));
	    foreach ($months as $en => $ar) {
	        if ($en == $en_month) { $ar_month = $ar; }
	    }

	    $find = array ("Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri");
	    $replace = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
	    $ar_day_format = date('D',strtotime($your_date)); // The Current Day

	    $ar_day = str_replace($find, $replace, $ar_day_format);

	    header('Content-Type: text/html; charset=utf-8');
	    $standard = array("0","1","2","3","4","5","6","7","8","9");
	    $eastern_arabic_symbols = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
	    // $current_date = $ar_day.' '.date('d').' - '.$ar_month.' - '.date('Y');

	    $current_date = date('Y',strtotime($your_date)).' ,'.date('d',strtotime($your_date)).' '.$ar_month;
	     $current_date = $ar_day.' '.$ar_month.' '.date('d',strtotime($your_date)).', '.date('Y',strtotime($your_date));

	    return $current_date;
	}

	public static function ArabicMonth($month) {
		
		$months = array("Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل", "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس", "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر");

		$en_month = date("M", strtotime($month));

		 foreach ($months as $en => $ar) {
	        if ($en == $en_month) { $ar_month = $ar; }
	    }

	    return $ar_month;
   	}

   	public static function make_slug($string = null, $separator = "-") {
    if (is_null($string)) {
        return "";
    }
    $string = trim($string);
    $string = mb_strtolower($string, "UTF-8");;
    $string = preg_replace("/[^a-z0-9_\s-ءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]/u", "", $string);
    $string = preg_replace("/[\s-]+/", " ", $string);
    $string = preg_replace("/[\s_]/", $separator, $string);
    return $string;
    }


public static function text_cut($text, $length = 100, $dots = true) {
    $text = trim(preg_replace('#[\s\n\r\t]{2,}#', ' ', $text));
    $text_temp = $text;
    while (substr($text, $length, 1) != " ") { $length++; if ($length > strlen($text)) { break; } }
    $text = substr($text, 0, $length);
    return $text . ( ( $dots == true && $text != '' && strlen($text_temp) > $length ) ? '...' : '');
}

}


