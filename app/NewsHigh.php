<?php

namespace App;
use DB;
use App\Content;
use Config;
use Helper;

use Illuminate\Database\Eloquent\Model;

class NewsHigh extends Model
{
//
protected $table = 'news_highlight';

public static function getNewsHighData($lang){
    
    if($lang=='ar'){
    	$col=['news_highlight.id','news_highlight.ar_title as title','news_highlight.ar_title as ar_name','news_highlight.news_url','news_highlight.status','news_highlight.created_at as publish on'];
    }else{
    	$col=['news_highlight.id','news_highlight.en_title as title','news_highlight.ar_title as ar_name','news_highlight.news_url','news_highlight.status','news_highlight.created_at as publish on'];
    }
	

       $data =DB::table('news_highlight')->OrderBy('news_highlight.id','DESC')
               ->get($col);
       return $data;
    }

     public static function get_member_column_names($type){
		$columns=['news_highlight.id','news_highlight.en_title as title','news_highlight.news_url','news_highlight.status','news_highlight.created_at as publish on'];
			return $columns;
		}

		 public static function getNewsHighContentx($id,$col){
        $data = NewsHigh::where('news_highlight.id',$id)
              ->first(['news_highlight.id','news_highlight.en_title as en_name','news_highlight.ar_title as ar_name','news_highlight.news_url as news_high_url','news_highlight.status']);
        return $data;
    }

    public static function getHighlights(){
        return $data = DB::table('news_highlight')->where('news_highlight.status',1)->select('*')->OrderBy('news_highlight.id','DESC')->get();
    }



// ----------
}    